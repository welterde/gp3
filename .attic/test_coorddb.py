import gp3.util.coorddb as cdb
import astropy.units as u

if __name__ == '__main__':
    c=cdb.CoordDB('/home/welterde/tmp/test234')
    c.put((30,30), 'test')
    print c.get((30,30))
    c.update((30.0+0.01*u.arcsec.to(u.deg), 30.0), 'test2')
    print c.get((30,30))
