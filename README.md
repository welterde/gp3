Basic usage
===========

Generic
-------

* Do astrometry using scamp (will create foo*.head with WCS solution)

> gp3-astrometry foo*.fits

GROND
-----

* Reduce one band from one OB

> gp3-redcomb -i grond -o foo.fits -b g /afs/ipp/mpe/gamma/instruments/grond/data/2012-12-10/raw/SMC_XMM0032/OB1_1/*.fits

* Reduce one band from one OB to TDPs

> gp3-redcomb -i grond -t -o "foo-%t.fits" -b g /afs/ipp/mpe/gamma/instruments/grond/data/2012-12-10/raw/SMC_XMM0032/OB1_1/

* Perform photometry (will save as fits table, ascii, etc.). Will use .head files if they exist

> gp3-phot -i grond -o irgendwas.fits -b g foo-3.fits

