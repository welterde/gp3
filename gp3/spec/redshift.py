import numpy as np

def calc_line_redshift_pairs(lines_obs, lines_lab):
    lines_obs = np.sort(lines_obs)
    lines_lab = np.sort(lines_lab)
    r_beta = np.array([], dtype=np.float)
    r_dist = np.array([], dtype=np.float)
    for i1 in range(0, len(lines_obs) - 1):
        for i2 in range(1, len(lines_obs)):
            d_obs = lines_obs[i1] - lines_obs[i2]
            for j in range(0, len(lines_lab) - 1):
                #print j
                d_lab = lines_lab[j] - lines_lab[(j+1):]
                beta = d_obs/d_lab
                idx1 = beta > 0.9
                idx2 = beta < 7
                idx = np.logical_and(idx1, idx2)
                r_beta = np.concatenate([r_beta, beta[idx]])
                r_dist = np.concatenate([r_dist, d_lab[idx]])
    return ((r_beta - 1.0), r_dist)

def calc_line_redshift_dist(lines_obs, lines_lab, line_tol=3.0):
    lines_obs = np.sort(lines_obs)
    lines_lab = np.sort(lines_lab)
    
    # redshift -> set[(weight, line)]
    redshifts = {}
    
    for obs1 in lines_obs[:-1]:
        # each lab line which is bluer than the observed line
        for lab1 in lines_lab[(lines_lab - line_tol) < obs1]:
            beta = obs1/lab1
            z = round(beta - 1, 3)
            if z > 6:
                continue
            if z not in redshifts:
                redshifts[z] = set([])
            redshifts[z].add((1, obs1))
            # each observed line that's redder than the line that we are considering
            for obs2 in lines_obs[lines_obs > obs1]:
                obs2_lab = obs2/beta
                m = np.abs(lines_lab - obs2_lab) < line_tol
                if np.any(m):
                    redshifts[z].add((1, obs2))
                else:
                    redshifts[z].add((-1, obs2))
    return redshifts

def pick_redshift(redshifts):
    weights = np.zeros(len(redshifts), dtype=np.float)
    z = np.empty(len(redshifts), dtype=np.float)
    for i, zi in zip(range(len(redshifts)), sorted(redshifts.keys())):
        for (w,_l) in redshifts[zi]:
            weights[i] += w
        z[i] = zi
    
    sel = np.argmax(weights)
    return z[sel]

def measure_line_redshift(lines_obs, lines_lab, line_tol=3.0):
    redshifts = calc_line_redshift_dist(lines_obs, lines_lab, line_tol)
    #print_redshift_dist(redshifts)
    return pick_redshift(redshifts)

def print_redshift_dist(redshifts):
    for z in sorted(redshifts.keys()):
        x = ''
        for (w,_l) in redshifts[z]:
            if w > 0:
                x += '+'
            else:
                x += '-'
        print 'z=%f\t%s' % (z,x)
