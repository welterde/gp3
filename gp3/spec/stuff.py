import astropy.constants as constants
import astropy.units as u
import math
import numpy as np
import logging

from .helcorr import helcorr

log = logging.getLogger('gp3.spec.helcorr')

def extract_line_velocity(spec, line_wavelength, sky_location, location, time,
                          vel_range=500*(u.kilometer / u.second)):
    wave = spec.wavelength
    
    # perform the velocity correction we have to do
    v = helcorr(lon=location.longitude, lat=location.latitude, alt=location.height,
                ra=sky_location.ra, dec=sky_location.dec, mjd=time)
    beta = v / constants.c
    doppler_factor = math.sqrt((1 + beta)/(1 - beta))
    wave *= doppler_factor
    
    # calculate the doppler shift relative to line
    doppler_factor = wave/line_wavelength
    v = constants.c*((doppler_factor**2 - 1)/(doppler_factor**2 + 1))
    
    # only keep elements inside the range we are interested in
    sel = np.abs(v) < vel_range
    
    return v[sel], spec.data[sel], spec.uncertainty.array[sel], wave[sel]
    
def calc_velocity(line_lab, line_obs):
    doppler_factor = line_obs/line_lab
    v = constants.c*((doppler_factor**2 - 1)/(doppler_factor**2 + 1))
    return v
