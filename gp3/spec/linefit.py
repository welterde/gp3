from astropy.modeling import models, fitting
import astropy.units as u
import numpy as np
from gp3.spec.stuff import calc_velocity

kms = u.kilometer / u.second

def fit_line(spec, line_center, fit_window=40):
    idx = np.abs(spec.wavelength.value - line_center) < fit_window
    wlen = spec.wavelength.value[idx]
    
    # linear baseline + gaussian on top
    g_init = models.Linear1D() + models.Gaussian1D(mean=line_center, stddev=3.0, amplitude=1000)
    fit_t = fitting.LevMarLSQFitter()
    g_fit = fit_t(g_init, wlen, spec.data[idx], weights=1.0/spec.uncertainty.array[idx])
    
    # level of baseline at the center of the line
    baseline = g_fit[0](line_center)
    line_amplitude = g_fit[1].amplitude.value
    line_obs = g_fit[1].mean.value
    line_fwhmG = g_fit[1].stddev.value*2.0*np.sqrt(2*np.log(2))
    
    rel_v = calc_velocity(line_center*u.Angstrom, line_obs*u.Angstrom).to(kms).value
    
    return (line_obs, line_amplitude, line_fwhmG, 0.0, baseline, g_fit, rel_v)

def fit_multiline(spec, lines, fit_window=20*u.Angstrom, block_sep=42*u.Angstrom):
    # TODO: assert that lines are sorted
    # group lines
    def rfunc((prev_groups, cur_group, last), cur):
        if np.abs(cur - last) <= block_sep:
            return (prev_groups, cur_group + (cur,), cur)
        else:
            return (prev_groups + (cur_group,), (cur,), cur)
    (groups,last_group,_) = reduce(rfunc, lines, (tuple(), tuple(), lines[0]))
    groups = map(list, list(groups + (last_group,)))
    
    # now fit each group seperately
    results = []
    for group in groups:
        w_min, w_max = np.min(spec.wavelength), np.max(spec.wavelength)
        idx = np.abs(spec.wavelength - (w_min+w_max)/2.0) < fit_window + (w_max - w_min)
        wlen = spec.wavelength.value[idx]
        # setup model
        m_init = models.Linear1D()
        for line in group:
            stddev = 4.0/2.0/np.sqrt(2*np.log(2))
            m_init += models.Gaussian1D(mean=line.value, stddev=stddev, amplitude=100)
        fit_t = fitting.LevMarLSQFitter()
        m_fit = fit_t(m_init, wlen, spec.data[idx], weights=1.0/spec.uncertainty.array[idx])
        
        g_results = []
        for i, line in enumerate(group):
            baseline = g_fit[0](line_center)
            line_amplitude = g_fit[i+1].amplitude.value
            line_obs = g_fit[i+1].mean.value
            line_fwhm = g_fit[i+1].stddev.value*2.0*np.sqrt(2*np.log(2))
            
            rel_v = calc_velocity(line, line_obs*u.Angstrom).to(kms).value
            
            g_results.append((line, line_obs, line_amplitude, line_fwhm, baseline, rel_v))
        results.append((g_results, m_fit))
    return results
