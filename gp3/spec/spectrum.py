from astropy.nddata import NDData


class Spectrum(NDData):
    
    def __init__(self, data, wavelength, uncertainty, mask=None, meta=None):
        super(Spectrum, self).__init__(flux=data, uncertainty=uncertainty, mask=mask, meta=meta)
        self.wavelength = wavelength
        
    def update(self, data=None, **kwargs):
        existing = {
            'flux': self.data,
            'uncertainty': self.uncertainty,
            'mask': self.mask,
            'meta': self.meta,
            'wavelength': self.wavelength
        }
        if data is not None:
            kwargs['flux'] = data
        existing.update(kwargs)
        return Spectrum(**kwargs)
