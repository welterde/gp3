import numpy as np
import astropy.io.fits as fits
import glob
import os
import specutils
import astropy.units as u
import logging


log = logging.getLogger('mosview.z')


def load_templates(templates_dir):
    log.info("Loading spectral templates from %s" % templates_dir)
    out = dict(map(lambda i: (i, {}), range(0, 9)))
    for fname in glob.iglob(os.path.join(templates_dir, "*.fits")):
        hdu_list = fits.open(fname, mode='readonly')
        spec_class = hdu_list[0].header['SPEC_CLN']
        c0 = hdu_list[0].header['coeff0']
        c1 = hdu_list[0].header['coeff1']
        npix = hdu_list[0].header['naxis1']
        wave = 10.**(c0 + c1 * np.arange(npix))
        # TODO: load uncertainty
        spec = specutils.Spectrum1D.from_array(dispersion=wave*u.Angstrom, flux=hdu_list[0].data[1])
        out[spec_class][os.path.basename(fname)] = spec
    return out

def correlate(specs, template):
    # splice the spectra together
    spliced = np.zeros(len(template.wavelength))
    if np.nanmin(specs[0].wavelength) > 5000*u.Angstrom:
        red = specs[0]
        blue = specs[1]
    else:
        red = specs[1]
        blue = specs[0]
    idx = blue.wavelength < np.nanmin(red.wavelength)
    wave = np.concatenate((blue.wavelength[idx], red.wavelength))
    flux = np.concatenate((blue.data[idx], red.data))
    # interpolate template to template spectrum
    obs_flux = np.interp(template.wavelength, xp=wave, fp=flux, right=0, left=0)
    
    print obs_flux.shape, template.data.shape
    cf = np.correlate(obs_flux, template.data, mode='full')
    print cf
    return cf
    
