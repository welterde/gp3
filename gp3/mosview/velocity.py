import astropy.units as u
import astropy.coordinates as coord
import numpy as np

from gp3.spec.stuff import extract_line_velocity
from gp3.plot.bokeh_plot import errorbar
from bokeh.plotting import figure
from bokeh.embed import components
from bokeh.models import BoxAnnotation


# TODO: update velocity profile when changing velocity range

kms = u.kilometer / u.second

class VelocityView(object):
    
    def __init__(self, spec, line_wavelength, site, time,
                 velocity_range=900*(u.kilometer / u.second)):
        self.spec = spec
        self.line_wavelength = line_wavelength
        self.coord = coord.SkyCoord(ra=spec.meta['RA']*u.deg, dec=spec.meta['DEC']*u.deg)
        self.site = site
        self.time = time
        self.velocity_range = velocity_range
        self.fit_obj = None
        self.update()
    
    def update(self):
        self.velocity, self.flux, self.uncertainty, self.wavelength = extract_line_velocity(
            spec=self.spec,
            line_wavelength=self.line_wavelength, 
            sky_location=self.coord,
            location=self.site,
            time=self.time.mjd,
            vel_range=self.velocity_range)
    
    def plot_bokeh(self):
        fig = figure(title='Velocity', responsive=True, plot_width=600, plot_height=600)
        if self.fit_obj is None:
            fig.line(self.velocity.to(kms).value, self.flux)
        else:
            wlen = np.linspace(np.min(self.wavelength), np.max(self.wavelength), 200)
            fit_val = self.fit_obj(wlen.value)
            # interpolate velocity
            vel = self.velocity.to(kms).value
            vel_i = np.interp(wlen, self.wavelength, vel)
            fig.line(vel_i, fit_val)
            fig.line(self.velocity.to(kms).value, self.fit_obj(self.wavelength.value))
        
        errorbar(fig, self.velocity.to(kms).value, self.flux, yerr=self.uncertainty)
        # TODO: labels
        #uncertainty = float(np.nanmax(self.uncertainty))
        #baseline = float(np.nanmean(self.flux))
        #box = BoxAnnotation(top=baseline+uncertainty, bottom=baseline-uncertainty, fill_alpha=0.2, fill_color='red')
        #fig.add_layout(box)
        return components(fig)
    
        
    
