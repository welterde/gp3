import astropy.io.fits as fits
import ccdproc

def load_multi_hdu_fits(filename, copy_hdr=None):
    hdu_list = fits.open(filename, mode='readonly', mmap=True, do_not_scale_image_data=True)
    
    for hdu in hdu_list:
        
