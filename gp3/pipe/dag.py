
class DAG(object):

    def __init__(self):
        self.stages = []
    
    def register(self, recipe):
        self.stages.append(recipe)
    
    def validate(self):
        for recipe in self.stages:
            recipe.validate()
    
    def run(self):
        runnable = self.stages
        while len(runnable) > 0:
            ready = filter(lambda a: a.ready, runnable)
            if len(ready) == 0:
                raise Exception('Broken DAG or something..')
            for x in ready:
                x()
            runnable = filter(lambda a: not a.done, runnable)
