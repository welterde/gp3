import shlex
import re
import logging
import operator
import collections
import copy



Item = collections.namedtuple('Item', 'tag attrs')

##### Description of the AST

class Node(object):
    
    def __call__(self, item):
        pass


class AndNode(Node):
    
    def __init__(self, nodes):
        self.nodes = nodes
    
    def __str__(self):
        nodes = map(str, self.nodes)
        return '<And: %s>' % list(nodes)
    
    def __call__(self, item):
        for snode in self.nodes:
            if not snode(item):
                return False
        return True

class OrNode(Node):
    
    def __init__(self, nodes):
        self.nodes = nodes
    
    def __str__(self):
        nodes = map(str, self.nodes)
        return '<Or: %s>' % list(nodes)
    
    def __call__(self, item):
        for snode in self.nodes:
            if snode(item):
                return True
        return False

class ConstantNode(Node):
    
    def __init__(self, ret_value=False):
        self.ret_value = ret_value
    
    def __str__(self):
        return '<Constant %s>' % self.ret_value
    
    def __call__(self, item):
        return self.ret_value

CONSTANTS = {
    'yes': ConstantNode(True),
    'no': ConstantNode(False)
}

OB_RE = re.compile(r'^OB(\d+)\_(\d+)$')

MATCH_REL_MAPPING = {
    '=': operator.eq,
    '>': operator.gt,
    '>=': operator.ge,
    '<': operator.lt,
    '<=': operator.le
}

MATCH_RELATIONS = list(MATCH_REL_MAPPING.keys())

class MatchNode(Node):
    
    def __init__(self, prop_name, prop_relation, prop_value):
        self.prop_name = prop_name
        self.prop_relation = prop_relation
        self.prop_value = prop_value
    
    def __str__(self):
        return '<Match: %s%s%s>' % (self.prop_name, self.prop_relation, self.prop_value)
    
    def __call__(self, item):
        attrs = item.attrs
        if self.prop_name not in attrs:
            logging.warn('Trying to match missing attribute %s (has %s)', self.prop_name, list(attrs.keys()))
            return False
        val = attrs[self.prop_name]
        prp = self.prop_value
        if self.prop_relation == '=':
            return attrs[val] == self.prop_value
        ob_m = OB_RE.match(self.prop_value)
        if ob_m:
            val_ob = OB_RE.match(val).groups(0)
            val = (int(val_ob[0]), int(val_ob[1]))
            prp_ob = ob_m.groups(0)
            prp = (int(prp_ob[0]), int(prp_ob[1]))
        return MATCH_REL_MAPPING[self.prop_relation](val, prp)

class ParseError(Exception):
    pass




LOGIC_RELATIONS = ['|', '&']

class Parser(object):
    
    def __init__(self, allowed_properties):
        self.allowed_properties = allowed_properties
    
    def __call__(self, tokens):
        self.tokens = list(map(str, tokens))
        
        nodes, relation = self.parse()
        if len(nodes) > 1 and relation == None:
            raise ParseError('No top-level logical relation given (Use either | or &)')
        elif len(nodes) == 1:
            return nodes[0]
        elif len(nodes) == 0:
            raise ParserError('Nothing there...? (tokens=%s)' % tokens)
        if relation == '&':
            return AndNode(nodes)
        elif relation == '|':
            return OrNode(nodes)
        else:
            raise ParseError('Internal parsing error')
    
    def parse(self, inside_parens=0):
        tokens = self.tokens
        
        if len(tokens) == 0 and inside_parens > 0:
            raise ParseError('Unmatched (')
        nodes = []
        relation = None
        while len(tokens) > 0:
            cur_tok = tokens.pop(0)
            if cur_tok == ')':
                if inside_parens == 0:
                    raise ParseError('Unmatched )')
                return nodes, relation
            if cur_tok == '(':
                sub_nodes, sub_relation = self.parse(inside_parens+1)
                if sub_relation == '&':
                    nodes.append(AndNode(sub_nodes))
                elif sub_relation == '|':
                    nodes.append(OrNode(sub_nodes))
                elif len(sub_nodes) == 1:
                    nodes.append(sub_nodes[0])
                elif len(sub_nodes) == 0:
                    # TODO: log warning about empty parentheses
                    pass
                else:
                    raise ParseError('Got strange sub_relation %s (nodes=%s)' % (sub_relation, sub_nodes))
            elif cur_tok in self.allowed_properties:
                if len(tokens) < 2:
                    raise ParseError('SyntaxEror near match for %s (rest=%s)' % (cur_tok, tokens))
                match_relation = tokens.pop(0)
                if match_relation not in MATCH_RELATIONS:
                    raise ParseError('Invalid match relation %s (allowed: %s; property=%s; rest=%s)' % (match_relation, cur_tok, ','.join(MATCH_RELATIONS), tokens))
                match_value = self.parse_value()
                m_node = MatchNode(prop_name=cur_tok, prop_relation=match_relation, prop_value=match_value)
                nodes.append(m_node)
            elif cur_tok in LOGIC_RELATIONS:
                if relation is not None and cur_tok != relation:
                    raise ParseError('Trying to combine different logical relations (use more parentheses!)')
                relation = cur_tok
            elif cur_tok in CONSTANTS:
                nodes.append(CONSTANTS[cur_tok])
            else:
                raise ParseError('Unknown token encountered: %s' % cur_tok)
        
        # we are out of tokens, but not out of parens..
        if inside_parens > 0:
            raise ParseError('Unmatched (')

        return nodes, relation
            
    
    def parse_value(self):
        tokens = self.tokens
        
        # could be a year YYYY-MM-DD
        if len(tokens) >= 5:
            # TODO: maybe check if it's a sensible year, month or day
            if tokens[0].isdigit() and tokens[1] == '-' and tokens[2].isdigit() and tokens[3] == '-' and tokens[4].isdigit():
                year = tokens.pop(0)
                tokens.pop(0)
                month = tokens.pop(0)
                tokens.pop(0)
                day = tokens.pop(0)
                return '%s-%s-%s' % (year, month, day)
        # could be a float  NNN.NNN
        if len(tokens) >= 3:
            if tokens[0].isdigit() and tokens[1] == '.' and tokens[2].isdigit():
                a = tokens.pop(0)
                tokens.pop(0)
                b = tokens.pop(0)
                return float('%s.%s' % (a,b))
        
        # could still be an int..
        if tokens[0].isdigit():
            return int(tokens.pop(0))
        
        # must be a string..
        return tokens.pop(0)
        # FIXME: could maybe enforce stricter typing by property type..
            

GroupMatch = collections.namedtuple('GroupMatch', 'rule_id matches')


class RuleEngine(object):
    
    def __init__(self, rules, attrs):
        self.raw_rules = rules
        self.file_attrs = attrs
        
        self.log = logging.getLogger('rules')
        
        self.log.debug('Setting up parser..')
        parser = Parser(allowed_properties=attrs)
        self.rules = []
        for rule in rules:
            self.log.debug('Lexing rule "%s"', rule)
            tokens = list(shlex.shlex(rule))
            self.log.debug('Parsing %s', tokens)
            node = parser(tokens)
            self.log.debug('Resulting AST: %s', node)
            self.rules.append(node)
        
    
    def match(self, items):
        """
        Match these (file_name/tag, {attrs}) tuples
        Will yield a GroupMatch for each match
        """
        # we will filter this down.. so better not mess with it in-place
        items = copy.copy(items)
        
        for rule_id, rule in enumerate(self.rules):
            self.log.debug('Matching rule %d (%s) with %d input items', rule_id, rule, len(items))
            # no need to evaluate all this stuff if we are out of items to match anyway
            if len(items) < 1:
                break
            
            new_items = []
            matched = []
            for item in items:
                if rule(item):
                    matched.append(item)
                else:
                    new_items.append(item)
            items = new_items
            self.log.debug('Matched %d items; %d remain', len(matched), len(items))
            yield GroupMatch(rule_id, matched)
            
        
        if len(items) > 0:
            self.log.error('Not all files were matched in the rules (missing default rule?); Unmatched files: %s', list(map(lambda a: a[0], items)))
            
            

