import threading
try:
    import queue
except ImportError:
    import Queue as queue
import logging
from .job import Job, WrappedResultJob


class Recipe(threading.Thread):
    
    def __init__(self):
        threading.Thread.__init__(self, name=self.name)
        self.daemon = True
        self.inq = queue.Queue()
        self.inputs = []
        self.log = logging.getLogger(self.name)
        self.outq = queue.Queue()
    
    def run(self):
        self.log.debug('Started recipe')
        self.setup()
        while True:
            item = self.inq.get()
            if item == None:
                break
            self.inputs.append(item)
            self.ingredients_updated()
        self.prev_stage_finished()
        self.outq.put(None)
        self.log.debug('Finished recipe (inputs=%s)', self.inputs)
    
    def setup(self):
        pass

    def ingredients_updated(self):
        if hasattr(self, 'consume_ingredient'):
            while len(self.inputs) > 0:
                x = self.inputs.pop(0)
                self.consume_ingredient(x)
        else:
            self.log.debug('No per-ingredient handling logic implemented')
        
    def prev_stage_finished(self):
        self.log.debug('prev_stage_finished not implemented')

    def emit_job(self, job):
        assert isinstance(job, Job)
        self.outq.put(job)
    
    def emit_result(self, result):
        self.outq.put(WrappedResultJob(result))


    

