import datetime
import astropy.io.fits as fits
import astropy.time as time

from .recipe import Recipe
from .rules import RuleEngine, Item, GroupMatch
from .types import ObservationBlock, FileTag




def get_night(hdr):
    date = datetime.datetime.strptime(hdr['DATE-OBS'], '%Y-%m-%dT%H:%M:%S.%f')
    if date.hour < 16: #We want the date folder to coorespond to the beginning of the night.
      date -= datetime.timedelta(days=1)
    return date.strftime('%Y-%m-%d')
    
def get_mjd(hdr):
    t = time.Time(hdr['DATE-OBS'])
    return t.mjd

def get_ob(hdr):
    if 'OBSRUNID' in hdr:
        return 'OB%s_%s' % (hdr['OBSRUNID'], hdr['OBSEQNUM'])
    else:
        return 'UNKNOWN'


ATTRS = {
    'night': get_night,
    'mjd': get_mjd,
    'ob': get_ob
}

OPTICAL_BANDS = 'griz'
NIR_BANDS = 'JHK'


def group_all(frames):
    return {'': frames}

def group_ob(frames):
    ret = {}
    for item in frames:
        name = item.attrs['ob']
        if name not in ret:
            ret[name] = []
        ret[name].append(item)
    return ret

def group_night(frames):
    ret = {}
    for item in frames:
        name = item.attrs['night']
        if name not in ret:
            ret[name] = []
        ret[name].append(item)
    return ret

# TODO: both essentially the same..


GROUP_ALGOS = {
    'per_ob': group_ob,
    'per_night': group_night,
    'all': group_all
}


class SorterRecipe(Recipe):
    name = 'sort'
    
    def __init__(self, rules, raw_file_fun, bands='grizJHK', default_grouping='per_ob'):
        self.rules = rules
        self.bands = bands
        self.raw_file_fun = raw_file_fun
        self.default_grouping = default_grouping
        
        match_rules = list(map(lambda a: a['match'], rules))
        self.engine = RuleEngine(match_rules, list(ATTRS.keys()))
        Recipe.__init__(self)
    
    def create_items(self, fnames, fname_filter):
        items = []
        for fname in fnames:
            # apply fname filter
            if fname_filter not in fname:
                continue
            
            self.log.debug('Loading header for %s', fname)
            hdr = fits.getheader(self.raw_file_fun(fname))
            
            attrs = dict([(k, v(hdr)) for k,v in ATTRS.items()])
            for band in self.bands:
                if band in OPTICAL_BANDS and 'OIMG' not in fname:
                    continue
                if band in NIR_BANDS and 'IRIM' not in fname:
                    continue
                attrs_band = attrs.copy()
                attrs_band['band'] = band
                tag = FileTag(fname, band)
                item = Item(tag, attrs_band)
                items.append(item)
        return items
    
    def prev_stage_finished(self):
        assert len(self.inputs) == 1
        file_names = self.inputs[0]
        
        self.log.info('Sorting optical frames..')
        items = self.create_items(file_names, 'OIMG')
        self.process_items(items, 'opt')
        
        self.log.info('Sorting NIR frames..')
        items = self.create_items(file_names, 'IRIM')
        self.process_items(items, 'nir')
        
    def process_items(self, items, supergrp_name):
        self.log.debug('Running ruleset matching..')
        for grp in self.engine.match(items):
            matches = grp.matches
            self.log.info('Matched rule %d with %d items', grp.rule_id, len(grp.matches))
            if 'bands' in self.rules[grp.rule_id]:
                bands = self.rules[grp.rule_id]['bands']
                matches = list(filter(lambda a: a.attrs['band'] in bands, matches))
                self.log.debug('Further filtered by band %s to %d items', bands, len(matches))
            grouping = self.rules[grp.rule_id].get('group', self.default_grouping)
            if grouping not in GROUP_ALGOS.keys():
                # TODO: maybe check this in the setup...
                self.log.error('Encountered unknown grouping algorithm "%s" in rule %d', grouping, grp.rule_id)
                continue
            if self.rules[grp.rule_id].get('drop', False):
                self.log.debug('Dropping this group according to rule %d', grp.rule_id)
                continue
            if len(matches) < 1:
                self.log.debug('All matches in this rule filtered away..')
                continue
            
            # pre-group by band
            for band in self.bands:
                m_band = list(filter(lambda a: a.attrs['band'] == band, matches))
                if len(m_band) < 1:
                    continue
                
                groups = GROUP_ALGOS[grouping](matches)
                for subgrpname, group in groups.items():
                    if len(subgrpname) > 0:
                        subgrpname = '_%s' % subgrpname
                    ob = ObservationBlock(name='r%02d%s_%s' % (grp.rule_id, subgrpname, band))
                    ob.raw_frames = group
                    ob.band = band
                    self.log.info('Created OB %s with %d frames', ob.name, len(ob.raw_frames))
                    self.emit_result([ob])
                
                
