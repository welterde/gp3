import h5py
import collections
import hashlib
import numpy as np
try:
    from cStringIO import StringIO
except ImportError:
    from io import StringIO
from astropy.table import Table
import astropy.units as u
from astropy.coordinates import SkyCoord

from .recipe import Recipe
from .job import Job
from .types import RedFile, RedProducts, PHOT_CATALOG, HDF5_CATALOG
from .tools import filter_products

from gp3.lc.simple import SimpleLC
from gp3.util.hash import hash_file


Observation = collections.namedtuple('Observations', 'cat band mjd cal_offset')

def load_observation(fname, band):
    f = h5py.File(fname, 'r')
    grp = f['GROND/%s' % band]
    mjd = grp.attrs['mid_time_mjd']
    
    if '/GROND/%s/phot/psf' % band not in f:
        return None

    cal_offset = np.nan
    if '/GROND/%s/phot/calib' % band in f:
        cal_offset = f['/GROND/%s/phot/calib' % band].attrs['offset']
        
    cat = Table.read(fname, format='hdf5', path='/GROND/%s/phot/psf' % band)
    
    return Observation(cat, band, mjd, cal_offset)

def compute_utag(in_hashes, target_coord):
    h = hashlib.new('sha1')
    data = StringIO()
    data.write('Coords: %s\n' % target_coord.to_string('hmsdms'))
    for input_hash in in_hashes:
        data.write(input_hash)
    h.update(data.getvalue().encode('utf-8'))
    data.close()
    return h.hexdigest()



class SimpleLCRecipe(Recipe):
    name = 'simplelc'
    
    def __init__(self, proj, target_name, target_coord, xmatch_radius):
        self.proj = proj
        
        self.target_name = target_name
        self.target_coord = target_coord
        
        self.xmatch_radius = xmatch_radius
        
        Recipe.__init__(self)
        
    def prev_stage_finished(self):
        ### Collect all the input hdf5 catalogs
        catalog_rfiles = []
        bands = []
        catalog_hashes = []
        for ob in self.inputs:
            # get the last reduction in the chain
            ob_products = ob.red_stack[-1]
            matched_prods = filter_products(ob_products, [PHOT_CATALOG, HDF5_CATALOG])
            if len(matched_prods) != 1:
                self.log.warning('OB has wrong number of hdf5 catalogs (num=%d)', len(matched_prods))
                continue
            catalog_rfiles.extend(matched_prods)
            bands.append(ob.band)
        
        # TODO: verify they exist..
        catalog_fnames = map(self.proj.red_file, catalog_rfiles)
        catalog_hashes = map(hash_file, catalog_fnames)
        
        utag = compute_utag(catalog_hashes, self.target_coord)
        
        ### Load catalogs
        obs = []
    
        for cat_fname, band in zip(catalog_fnames, bands):
            ob = load_observation(cat_fname, band)
            if ob is not None:
                obs.append(ob)
        
        ### Construct light curves for each band
        lcs = {}
        
        for ob in obs:
            reference_epoch = False
            if ob.band not in lcs:
                lcs[ob.band] = SimpleLC(self.xmatch_radius)
                reference_epoch = True
            # TODO: reformat cat to match the wanted format..
            lcs[ob.band].add_observation(ob.mjd, ob.cat, calib_offset=ob.cal_offset, reference_epoch=reference_epoch)
        
        lcs_prods = []
        lcs_bands = list(lcs.keys())
        for band in lcs_bands:
            if len(lcs[band].raw_obs) < 2:
                self.log.info('Skipping band %s - too few observations' % band)
                continue
            self.log.debug('Processing band %s' % band)
            lcs[band].process()
            
            prod = RedFile(stage='simplelc', utag=utag, fname='%s_%s' % (self.target_name, band), tags=[])
            
            fname = self.proj.red_file(prod)
            
            # find the right lightcurve..
            ras = np.array(map(lambda a: a[0], lcs[band].lc_coords))
            decs = np.array(map(lambda a: a[1], lcs[band].lc_coords))
            lc_coords = SkyCoord(ras*u.deg, decs*u.deg)
            sep = self.target_coord.separation(lc_coords)
            sel = sep < self.xmatch_radius
            nmatch = np.count_nonzero(sel)
            self.log.debug('Search for LC found %d matches', nmatch)
            if nmatch != 1:
                self.log.warning('Invalid number of matches')
                continue
            idx = sel.nonzero()[0][0]
            times_mjd = lcs[band].mjds
            mag, mag_err,times_mjd,valid,raw_mag,cal_offset = lcs[band].lc[idx]
            
            t = Table([times_mjd, mag, mag_err, raw_mag, cal_offset], names=('MJD', 'MAG', 'MAG_ERR', 'MAG_RAW', 'CAL_OFFSET'))
            t_valid = t[valid]
            t_valid.write(fname, format='ascii')
            lcs_prods.append(prod)
        products = RedProducts(stage='lc', utag=utag, products=lcs_prods)
        self.proj.register_products(self.inputs[0], products)
