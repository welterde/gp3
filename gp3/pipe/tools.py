


def filter_products(prods, tags):
    prods = prods.products
    
    for tag in tags:
        prods = filter(lambda a: tag in a.tags, prods)
    return list(prods)
