import os
import logging
import time

from .recipe import Recipe
from .job import Job
from .types import RedFile, RedProducts

from gp3.redcomb.gp1 import redcomb
from gp3.wrappers.gp1 import hash_redcomb
from gp3.util.hash import hash_file




# TODO: support TDP processing
class RedcombJob(Job):
    
    def __init__(self, ob, raw_imgs, dest_fname):
        self.ob = ob
        self.raw_imgs = raw_imgs
        self.dest_fname = dest_fname
        log = logging.getLogger('redcomb')
        self.log = logging.LoggerAdapter(log, {'obname': ob.name})

    def __call__(self):
        # TODO: use own tmpdir?
        self.log.info('Running gp1/redcomb with %d input files', len(self.raw_imgs))
        start_time = time.time()
        redcomb(self.raw_imgs, outfile=self.dest_fname, bands=self.ob.band, quiet=True, log=self.log)
        done_time = time.time()
        if os.path.exists(self.dest_fname):
            self.log.info('Success! Took %f min', (done_time-start_time)/60.0)
            yield self.ob
        else:
            self.log.error('redcomb failed')
    


class GP1Redcomb(Recipe):
    name = 'redcomb'
    
    def __init__(self, proj, red_dir, raw_file_fun, red_file_fun):
        self.proj = proj
        self.red_dir = red_dir
        self.raw_frame_fun = raw_file_fun
        self.red_frame_fun = red_file_fun
        Recipe.__init__(self)

    def consume_ingredient(self, ob):
        log = logging.LoggerAdapter(self.log, {'obname': ob.name})
        log.debug('Looking at %s', ob)
        #self.log.debug('Raw frames: %s', ob.raw_frames)
        raw_frame_files = list(map(lambda a: self.raw_frame_fun(a.tag.arcfile), ob.raw_frames))
        #self.log.debug('Raw frame files: %s', raw_frame_files)
        # construct hash!
        ob_hash = hash_redcomb(raw_frame_files, band=ob.band, hash_image_func=hash_file)
        log.debug('Hashed to %s', ob_hash)
        
        redfile = RedFile(stage='redcomb', utag=ob_hash, fname='%s.fits' % ob.name, tags=[])
        products = RedProducts(stage='redcomb', utag=ob_hash, products=[redfile])
        self.proj.register_products(ob, products)
        red_fname = self.red_frame_fun(redfile)
        newob = ob.clone()
        newob.push_reduction(products)
        
        if os.path.isfile(red_fname):
            log.info('Found existing reduction for input hash %s', ob_hash)
            self.emit_result([newob])
        else:
            log.info('No cached reduction found for input hash %s', ob_hash)
            job = RedcombJob(newob, raw_imgs=raw_frame_files, dest_fname=red_fname)
            self.emit_job(job)
