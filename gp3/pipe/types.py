import collections
import copy

class ObservationBlock(object):
    
    def __init__(self, name, band=None, raw_frames=None, red_stack=None, attrs=None):
        self.name = name
        self.band = band
        self.raw_frames = raw_frames
        if red_stack is None:
            red_stack = []
        self.red_stack = red_stack
        self.attrs = attrs
    
    def __str__(self):
        # TODO: stuff about attributes, raw and red frames
        return '[OB name=%s band=%s]' % (self.name, self.band)
    
    def clone(self):
        return copy.deepcopy(self)
    
    def push_reduction(self, products):
        self.red_stack.append(products)


FileTag = collections.namedtuple('FileTag', 'arcfile band')

RedProducts = collections.namedtuple('RedProducts', 'stage utag products')

PHOT_CATALOG = 'phot_cat'

HDF5_CATALOG = 'hdf5_cat'
ASCII_CATALOG = 'ascii_cat'

RedFile = collections.namedtuple('RedFile', 'fname stage utag tags')
