import os
import sys
import glob
import shutil
import datetime
import logging
import itertools

from .recipe import Recipe

# File collecting module


NEXT_DAY = datetime.timedelta(days=1)


class SimpleFileFinder(object):
    
    def __init__(self, target_names, search_directories, time_range, target_header_key='OBJECT'):
        self.target_names = target_names
        self.search_directories = search_directories
        self.target_header_key = target_header_key
        self.time_range_start = time_range[0]
        self.time_range_end = time_range[1]
        
        self.log = logging.getLogger('collector')
    
    def search(self):
        cur_date = self.time_range_start
        
        # iterate over the days
        iters = []
        while cur_date < self.time_range_end:
            x = self.search_day(cur_date)
            if x is not None:
                iters.append(x)
            cur_date += NEXT_DAY
        return itertools.chain(*iters)

        
        
    def search_day(self, date):
        seen_files = set()
        self.log.debug('Searching day %s', date)
        for search_dir in self.search_directories:
            cd = os.path.join(search_dir, date.isoformat(), 'raw')
            self.log.debug('Looking at %s', cd)
            if not os.path.isdir(cd):
                self.log.debug('Missing directory %s', cd)
                continue
            targets = os.listdir(cd)
            self.log.debug('Contains: %s', targets)
            for tgt_name in self.target_names:
                if tgt_name in targets:
                    self.log.debug('Found targets %s', tgt_name)
                    return self.process_target(cd, tgt_name, seen_files)
            
    
    def process_target(self, day_dir, target_name, seen_files):
        glob_s = os.path.join(day_dir, target_name, '*', '*.fits')
        self.log.debug('Searching for files (%s)..', glob_s)
        files = glob.iglob(glob_s)
        found = 0
        skipped = 0
        for f in files:
            if os.path.basename(f) not in seen_files:
                seen_files.add(f)
                found += 1
                yield f
            else:
                skipped += 1
        self.log.debug('Found %d files and ignored %d dups', found, skipped)




class SimpleCollectorRecipe(Recipe):
    name = 'collector'
    
    def __init__(self, finder, cache_raw=False, raw_dir=False):
        self.finder = finder
        self.cache_raw = cache_raw
        self.raw_dir = raw_dir
        Recipe.__init__(self)
    
    def prev_stage_finished(self):
        files = self.finder.search()
        
        if files is None:
            self.log.critical('Could not find any file in any archive!')
            sys.exit(1)
        
        raw_files = []
        for f in files:
            tgt_path = os.path.join(self.raw_dir, os.path.basename(f))
            raw_files.append(os.path.basename(f))
            if os.path.exists(tgt_path):
                continue
            # that link is dead.. lets handle it like it wasn't there
            if os.path.islink(tgt_path):
                self.log.info('Deleting dead symlink %s', tgt_path)
                os.remove(tgt_path)
            if self.cache_raw:
                self.log.info('Copying raw file to %s (from %s)', tgt_path, f)
                shutil.copy(f, tgt_path)
            else:
                self.log.info('Symlink raw file to %s (from %s)', tgt_path, f)
                os.link(f, tgt_path)
        self.log.info('Collected %d raw files', len(raw_files))
        self.emit_result([raw_files])
