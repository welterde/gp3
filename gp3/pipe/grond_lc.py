import logging
import datetime

import astropy.units as u

from .runner import Runner

from .collect import SimpleCollectorRecipe, SimpleFileFinder
from .sort import SorterRecipe
from .redcomb import GP1Redcomb
from .astrphot import GP1AstrPhot
from .lc import SimpleLCRecipe

from gp3.util.coords import parse_coords

def from_isoformat(s):
    year, month, day = s.split('-')
    return datetime.date(year=int(year), month=int(month), day=int(day))

def parse_time_range(cfg):
    if 'start' not in cfg:
        logging.error('Missing start of time range')
        sys.exit(1)
    start = cfg['start']
    end = cfg['end']
    if 'end' not in cfg:
        end = 'today'
    if end == 'today':
        end = datetime.date.today()
    else:
        end = end
    start = start
    return (start, end)
    
    


class GRONDLCPipeline(Runner):
    
    def setup(self, proj):
        cfg = proj.raw_cfg
        # where to get the files
        #ensure_cfg_section(cfg, ['collect'])
        finder = SimpleFileFinder(
            target_names=cfg['collect']['target_names'],
            search_directories=cfg['collect']['search_directories'],
            time_range=parse_time_range(cfg['collect']['time_range'])
        )
        self.add_stage(SimpleCollectorRecipe(
            finder,
            cache_raw=cfg['collect'].get('cache_raw', True),
            raw_dir=proj.raw_directory
        ))
        self.add_stage(SorterRecipe(
            rules=cfg['sort']['rules'],
            bands=cfg['sort'].get('bands', 'grizJHK'),
            raw_file_fun=proj.raw_file,
            default_grouping=cfg['sort'].get('default_group', 'per_ob')
        ))
        self.add_stage(GP1Redcomb(proj,
            red_dir=proj.red_directory,
            raw_file_fun=proj.raw_file,
            red_file_fun=proj.red_file
        ))
        self.add_stage(GP1AstrPhot(proj,
            red_file_fun=proj.red_file
        ))
        self.add_stage(SimpleLCRecipe(proj,
                                      target_name='default',
                                      target_coord=parse_coords(cfg['lc']['coords']),
                                      xmatch_radius=1*u.arcsec))
        # TODO: continue here
        #self.add_stage(ReduceGP1Redcomb)



Pipe = GRONDLCPipeline
