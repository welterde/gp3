from .exceptions import ValidationError
from .types import DataType

class Node(object):
    def __new__(cls, *args, **kwargs):
        if '_bind' in kwargs:
            return super(Node, cls).__new__(cls)
        else:
            return UnboundNode(cls, *args, **kwargs)
    
    def __init__(self, name, _bind, typeclass):
        self.name = name
        self._parent = None
        # TODO: implement proper typeclass handling
        self.typeclass = typeclass
    
    def validate(self):
        raise NotImplementedError('The %s type is missing the validate function' % repr(self.__class__))
    
    @property
    def parent(self):
        return self._parent
    
    @parent.setter
    def parent(self, new):
        self.update_parent(new)
    
    def update_parent(self, new):
        raise NotImplementedError()



class UnboundNode(object):
    def __init__(self, node_class, *args, **kwargs):
        self.node_class = node_class
        self.args = args
        self.kwargs = kwargs

    def bind(self, name):
        kw = dict(
            self.kwargs,
            _bind=True,
            name=name
        )
        return self.node_class(*self.args, **kw)

    def __repr__(self):
        return '<UnboundNode(%s, %r, %r)>' % (self.node_class.__name__, self.args, self.kwargs)

class Parameter(Node):
    # TODO: add support for validators
    # TODO: better keep track of default value?
    def __init__(self, typeclass, default=None, optional=False, **kwargs):
        super(Parameter, self).__init__(typeclass=typeclass, **kwargs)
        self.value = default
        self.optional = optional
    
    def validate(self):
        if self.value is None and not self.optional:
            raise ValidationError('Value of property %s is None' % self.name)
    
    def update_parent(self, new):
        raise AttributeError("parent node for parameter doesn't make sense")
    
class Requirement(Node):
    
    def __init__(self, typeclass, optional=False, **kwargs):
        super(Requirement, self).__init__(typeclass=typeclass, **kwargs)
        self.optional = optional
        self._parent = None
        self._value = None
    
    def update_parent(self, new):
        # FIXME: or some sort of alternate source..?
        if not isinstance(new, Product):
            raise ValueError('You have to attach requirement to a product')
        self._parent = new
    
    def validate(self):
        if self.optional:
            return
        if self._value is not None:
            return
        if self._parent is None:
            raise ValidationError('%s is missing a source providing it' % repr(self))
    
    def __repr__(self):
        # TODO: maybe show Product node as well if there
        return '<Requirement(%s, %s)>' % (self.__class__.__name__, self.typeclass.__name__)
    
    @property
    def present(self):
        if self._value is not None:
            return True
        return self._parent.value is not None
        
    @property
    def fulfilled(self):
        if self._value is not None:
            return True
        if self._parent is not None:
            return self.present
    
    @property
    def value(self):
        if self._value is not None:
            return self._value
        return self._parent.value
    
    @value.setter
    def value(self, new_val):
        self._value = new_val

class Product(Node):
    
    def __init__(self, typeclass, **kwargs):
        super(Product, self).__init__(typeclass=typeclass, **kwargs)
        self._children = set()
        self._data = None
    
    def update_parent(self, new):
        raise AttributeError("parent node for a product doesn't make sense")
    
    def add_child(self, new):
        self._children.add(new)
        
    def validate(self):
        pass
    
    @property
    def value(self):
        return self._data
    
    @value.setter
    def value(self, val):
        # TODO: perform some checking
        self._data = val
