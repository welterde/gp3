import os
import logging
import shutil
import time

from .recipe import Recipe
from .job import Job
from .types import RedFile, RedProducts, PHOT_CATALOG, HDF5_CATALOG, ASCII_CATALOG

from gp3.wrappers.gp1 import astrphot, hash_astrphot
from gp3.util.hash import hash_file


OUTPUT_PREFIX = 'ana.'



class AstrPhotJob(Job):
    
    def __init__(self, ob, input_img, products, productmap, red_file_fun):
        self.ob = ob
        self.input_img = input_img
        self.products = products
        self.productmap = productmap
        self.red_file_fun = red_file_fun
        log = logging.getLogger('astrphot')
        self.log = logging.LoggerAdapter(log, {'obname': ob.name})
        
    def __call__(self):
        # TODO: use own tmpdir?
        start_time = time.time()
        self.log.info('Running gp1/astrphot')
        astrphot(self.input_img, self.ob.band, self.callback, quiet=True)
        done_time = time.time()
        self.log.info('Success! Took %f min', (done_time-start_time)/60.0)
        yield self.ob
    
    def callback(self, reddir):
        self.log.debug('Copying files from work directory %s', reddir)
        for product, infile in zip(self.products, self.productmap):
            src_fname = os.path.join(reddir, infile)
            if not os.path.isfile(src_fname):
                raise ValueError('Product file %s not found', src_fname)
            dst_fname = self.red_file_fun(product)
            shutil.copy(src_fname, dst_fname)
            

OUTPUT_MAP = [
    ('h5', 'input_ana.h5', [PHOT_CATALOG, HDF5_CATALOG]),
    ('result', 'input_ana.result', [PHOT_CATALOG, ASCII_CATALOG]),
    ('fits', 'input_ana.fits', [])
]


class GP1AstrPhot(Recipe):
    name = 'astrphot'
    
    def __init__(self, proj, red_file_fun):
        self.proj = proj
        self.red_file_fun = red_file_fun
        Recipe.__init__(self)
    
    def create_product_mapping(self, ob, in_hash):
        ret = []
        fmap = []
        for ext, infile, tags in OUTPUT_MAP:
            prod = RedFile(stage='astrphot', utag=in_hash, fname='%s_%s%s' % (ob.name, OUTPUT_PREFIX, ext), tags=tags)
            ret.append(prod)
            fmap.append(infile)
        return ret, fmap
        
    def products_exist(self, products):
        for product in products:
            fname = self.red_file_fun(product)
            if not os.path.isfile(fname):
                return False
        return True
    
    def consume_ingredient(self, ob):
        log = logging.LoggerAdapter(self.log, {'obname': ob.name})
        log.debug('Looking at %s', ob)
        
        # TODO: search for correctly tagged file
        input_fname = self.red_file_fun(ob.red_stack[-1].products[0])
        
        input_hash = hash_astrphot(input_fname, band=ob.band, hash_image_func=hash_file)
        log.debug('Hashed to %s', input_hash)
        
        products, productmap = self.create_product_mapping(ob, input_hash)
        job_products = RedProducts(stage='astrphot', utag=input_hash, products=products)
        self.proj.register_products(ob, job_products)
        
        newob = ob.clone()
        newob.push_reduction(job_products)
        
        if self.products_exist(products):
            log.info('Found existing reduction for input hash %s', input_hash)
            self.emit_result([newob])
        else:
            log.info('No cached reduction found for input hash %s', input_hash)
            job = AstrPhotJob(newob, input_fname, products, productmap, self.red_file_fun)
            self.emit_job(job)
