import astropy.nddata as nddata
import xxhash

class BaseDataModel(object):
    
    def to_hash(self):
        """
        Convert all reproducible accessible attributes to a unique hash value
        """
        raise NotImplementedError()



class CCDFrame(nddata.CCDData, BaseDataModel):
    
    def to_hash(self):
        # TODO: also take wcs and other headers into account
        # TODO: maybe also compute it once and do some dirty bit tracking?
        h = xxhash.xxh64()
        h.update(self.data)
        return h.digest()
