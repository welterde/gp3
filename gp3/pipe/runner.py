from collections import namedtuple
import logging
import threading
import time
from .job import Job



Stage = namedtuple('Stage', 'recipe ignore_failed')

class DummyFuture(object):
    
    def __init__(self, result):
        self.out = result
    
    def result(self):
        return self.result
    
    def exception(self):
        return None


class RecipeHandlerThread(threading.Thread):
    
    def __init__(self, name, prev_recipe, job_engine, failure_evt, ignore_failed=False):
        threading.Thread.__init__(self, name=name)
        self.daemon = True
        self.log = logging.getLogger('%s_hand' % name)
        self.ignore_failed = ignore_failed
        self.failure_evt = failure_evt
        self.complete_evt = threading.Event()
        self.job_engine = job_engine
        self.prev_recipe = prev_recipe
        
        self.next_recipe = None
        self.failures = 0
        self.outputs = []

    def run(self):
        while True:
            item = self.prev_recipe.outq.get()
            if item == None:
                self.prev_recipe.outq.task_done()
                break
            if isinstance(item, Job):
                item = self.job_engine.submit(item)
                item.add_done_callback(self.handle_job_complete)
            else:
                item = DummyFuture(item)
                self.handle_job_complete(item)
        
        # wait for all jobs emitted by the previous stage to finish
        self.prev_recipe.outq.join()
        if self.failures > 0 and not self.ignore_failed:
            self.failure_evt.set()
        elif self.next_recipe is not None:
            self.next_recipe.inq.put(None)
        self.complete_evt.set()

    
    def handle_job_complete(self, item):
        self.log.debug('Handling item (%s)', item)
        if item.exception() is not None:
            self.log.error('Previous recipe failed')
            self.failures += 1
        elif self.next_recipe is not None and (not self.failure_evt.is_set()):
            for x in item.result():
                self.next_recipe.inq.put(x)
        else:
            for x in item.result():
                self.outputs.append(x)
        self.prev_recipe.outq.task_done()
        




class Runner(object):
    
    def __init__(self, job_engine):
        self.stages = []
        self.job_engine = job_engine
        self.log = logging.getLogger('run')
    
    def add_stage(self, recipe, ignore_failed=False):
        self.log.debug('Adding recipe %s (ignore_failed=%s)', recipe.name, ignore_failed)
        s = Stage(recipe, ignore_failed)
        self.stages.append(s)
    
    def run(self):
        self.log.debug('Starting run')
        
        # prime first recipe
        self.stages[0].recipe.inq.put('0')
        self.stages[0].recipe.inq.put(None)
        
        # setup failure callback
        failure_evt = threading.Event()
        complete_evt = None
        
        # now setup the chain
        while len(self.stages) > 0:
            stage = self.stages.pop(0)
            
            # start the current recipe
            cur_recipe = stage.recipe
            cur_recipe.start()
            
            # and start the companion handler
            handler = RecipeHandlerThread(
                name=cur_recipe.name,
                prev_recipe=cur_recipe,
                job_engine=self.job_engine,
                failure_evt=failure_evt,
                ignore_failed=stage.ignore_failed
            )
            if len(self.stages) > 0:
                handler.next_recipe = self.stages[0].recipe
            else:
                complete_evt = handler.complete_evt
            handler.start()
        
        # TODO: maybe inspect job engine about remaining jobs, etc.
        while True:
            if complete_evt.is_set():
                return True
            if failure_evt.is_set():
                return False
            time.sleep(1)

            
