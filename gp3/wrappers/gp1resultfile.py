import astropy.io.ascii.core as core


NAME_MAPPING = [
    
]


class GP1ResultHeader(core.BaseHeader):
    
    def __init__(self):
        core.BaseHeader.__init__(self)
    
    def get_cols(self, lines):
        
        columns = {}
        
        icolumns = False
        for line, idx in zip(lines, range(len(lines))):
            if not line.startswith('#'):
                break # End of header lines
            if line.startswith('# Columns:'):
                icolumns = True
                line = line[11:]
            elif icolumns:
                line = line[2:]
            
            if icolumns:
                cols = filter(lambda s: len(s) == 0, line.strip().split(','))
