import logging
import tempfile
import subprocess
import shutil
import os

import gp3.conf as conf

log = logging.getLogger('gp3.wrappers.astrometrynet')


def astrometry(image, callback, copy_image=True,
               workdir=None, preserve_workdir=False, tmpdir=conf.pipeline.tmpdir):
    # 1. create temp dir
    if workdir:
        reddir = workdir
    else:
        reddir = tempfile.mkdtemp(prefix='astrometrynet', dir=os.path.expanduser(tmpdir))
        log.debug('Created tempdir %s', reddir)

    # 2. copy over input images
    if copy_images:
        image_loc = 'input.fits'
        log.debug('Copying file over')
        shutil.copy(image, image_loc)
    else:
        image_loc = image

    # 3. Run astrometry.net for this image
    args = ['solve-field', '--guess-scale', image_loc]
    proc = subprocess.Popen(args, cwd=reddir,
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE)
    while True:
        line = proc.stdout.readline()
        if not line:
            break
        log.debug(line.strip())
    proc.wait()
    log.debug("Got returncode: %d", proc.returncode)

    # 5. callback
    ret = callback(reddir, 'input.fits')
    
    if not preserve_workdir:
        log.debug('Deleteing %s..', reddir)
        #shutil.rmtree(reddir)

    return ret
