import logging
import tempfile
import shutil
import pkgutil
import configobj
import os
import shlex
import subprocess

import astropy.table as table

import gp3.conf as conf
import gp3.wrappers.utils as utils


log = logging.getLogger('gp3.wrappers.sex')




def sextractor(measure_image, detect_image=None, callback=None, ini={},
               params=[], template='default.sex', parse_cat=True,
               workdir=None, preserve_workdir=False, catalog_name='out.cat',
               filtername='default.conv', tmpdir=conf.pipeline.tmpdir):
    # 1. create temp dir
    if workdir:
        reddir = workdir
    else:
        reddir = tempfile.mkdtemp(prefix='sextractor', dir=os.path.expanduser(tmpdir))
        log.debug('Created tempdir %s', reddir)
    
    
    # 2. generate configuration files
    # 2.1 sextractor.cfg
    cfg = utils.parse_sex(pkgutil.get_data('gp3.wrappers', 'sextractor/%s' % template).decode('utf8'))
    cfg['FILTER_NAME'] = os.path.join(os.environ.get('SEXTRACTOR_DIR', '/usr/share/sextractor'), filtername)
    cfg['PARAMETERS_NAME'] = 'sextractor.params'
    cfg['CATALOG_NAME'] = os.path.join(reddir, catalog_name)
    # TODO: read some parameters from input file
    cfg.update(ini)
    utils.write_sex(cfg, os.path.join(reddir, 'sextractor.cfg'))
    # 2.2 sextractor.params
    with open(os.path.join(reddir, cfg['PARAMETERS_NAME']), 'w') as f:
        f.write('\n'.join(params))
    
    # 3. run sextractor
    measure_image = os.path.abspath(measure_image)
    if detect_image:
        detect_image = os.path.abspath(detect_image)
        args = ['sextractor', '-c', 'sextractor.cfg', measure_image, detect_image]
    else:
        args = ['sextractor', '-c', 'sextractor.cfg', measure_image]
    proc = subprocess.Popen(args, cwd=reddir,
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE)
    while True:
        line = proc.stdout.readline()
        if not line:
            break
        log.debug(line.strip())
    proc.wait()
    log.debug("Got returncode: %d", proc.returncode)
    
    # 4. callback (copies relevant files)
    if callback:
        ret = callback(reddir)
    else:
        ret = None
    
    if parse_cat:
        ret = table.Table.read(cfg['CATALOG_NAME'], format='ascii.sextractor')
    
    # 5. delete temp dir
    if (not workdir) and (not preserve_workdir):
        log.debug('Deleting %s..', reddir)
        shutil.rmtree(reddir)
    
    return ret
