import logging
import tempfile
import shutil
import pkgutil
try:
    import ConfigParser as cfgparser
except ImportError:
    import configparser as cfgparser
import io
import subprocess
import os
import sys
try:
    from cStringIO import StringIO
except ImportError:
    from io import StringIO
import hashlib

import gp3.conf as conf


log = logging.getLogger('gp3.wrappers.gp1')


# TODO: refactor.. redcomb and astrphot tasks are almost the same
# 1. split out tmpdir code
# 2. split out ini generation code

def get_version():
    log.debug('Getting gp1 software version')
    
    proc = subprocess.Popen(['gr_version.py'],
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)
    v = proc.stdout.readline()
    return v.strip()

def redcomb_gencfg(reddir, band, ini_override):
    log.debug('Generating config with reddir=%s band=%s ini_override=%s', reddir, band, ini_override)

    cp = cfgparser.ConfigParser()
    # read template config file from gp1 package
    if sys.version_info < (3, 0):
        cp.readfp(io.BytesIO(pkgutil.get_data('tom', 'etc/%s.ini' % band)))
    else:
        cfg_data = pkgutil.get_data('tom', 'etc/%s.ini' % band).decode('utf-8')
        cp.read_string(cfg_data)
    for ckey in ini_override:
        group = 'task'
        key = ckey
        if len(ckey.split(':')) > 1:
            group, key = ckey.split(':', 1)
        cp.set(group, key, ini_override[ckey])
    cp.set('task', 'images', os.path.join(reddir, 'input-*.fits'))
    cp.set('task', 'workdir', os.path.join(reddir, ''))
    return cp

def redcomb(images, band, callback, ini_override={}, quiet=False,
            tmpdir=conf.pipeline.tmpdir):
    log.debug('Processing data with gp1 in band %s' % band)
    
    # 1. create temp dir
    reddir = tempfile.mkdtemp(prefix='gp1redcomb', dir=os.path.expanduser(tmpdir))
    log.debug('Created tempdir %s', reddir)
    # 2. copy images over
    log.debug('Copying %d files over', len(images))
    for img, i in zip(images, range(len(images))):
        shutil.copy(img, os.path.join(reddir, 'input-%02d.fits' % i))
    

    # 3. generate ini file
    inif = os.path.join(reddir, 'redcomb-%s.ini' % band)
    cp = redcomb_gencfg(reddir, band, ini_override)
    with open(inif, 'w') as f:
        cp.write(f)
    
    # 4. run pipeline
    proc = subprocess.Popen(['gr_redcomb.py', inif],
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)
    while True:
        line = proc.stderr.readline()
        if not line:
            break
        if not quiet:
            log.debug(line.strip())
    proc.wait()
    log.debug("Got returncode: %d", proc.returncode)
    # TODO: check if 0
    
    # 5. callback (copies relevant files)
    ret = callback(reddir)
    
    # 6. delete temp dir
    log.debug('Deleting %s..', reddir)
    shutil.rmtree(reddir)
    
    return ret


def hash_redcomb(images, band, hash_image_func, ini_override={}):
    # Get version of the gp1 pipeline
    gp_version = get_version()
    
    # generate config
    cp = redcomb_gencfg('', band, ini_override)
    # delete references to data reduction directory and image filenames
    cp.remove_option('task', 'images')
    cp.remove_option('task', 'workdir')
    
    # hash input images
    log.debug('Hashing input images')
    image_hashes = sorted(map(hash_image_func, images))
    
    data = StringIO()
    data.write('GP version: %s\nImage Hashes: ' % gp_version)
    # TODO: collect versions of more software components..?
    for img_hash in image_hashes:
        data.write(img_hash)
    data.write('\nConfig:\n')
    cp.write(data)
    
    h = hashlib.new('sha1')
    h.update(data.getvalue().encode('utf-8'))
    data.close()
    
    # TODO: log this structure? 
    
    return h.hexdigest()


def astrphot_gencfg(reddir, band, ini_override):
    log.debug('Generating astrphot config with reddir=%s band=%s ini_override=%s', reddir, band, ini_override)

    cp = cfgparser.ConfigParser()
    # read template config file from gp1 package
    if sys.version_info < (3, 0):
        cp.readfp(io.BytesIO(pkgutil.get_data('tom', 'etc/%sana.ini' % band)))
    else:
        cfg_data = pkgutil.get_data('tom', 'etc/%sana.ini' % band).decode('utf-8')
        cp.read_string(cfg_data)
    for ckey in ini_override:
        group = 'task'
        key = ckey
        if len(ckey.split(':')) > 1:
            group, key = ckey.split(':', 1)
        cp.set(group, key, ini_override[ckey])
    cp.set('task', 'images', os.path.join(reddir, 'input.fits'))
    cp.set('task', 'workdir', os.path.join(reddir, ''))
    return cp

def astrphot(image, band, callback, ini_override={}, copy_hook=None,
             tmpdir=conf.pipeline.tmpdir, quiet=False):
    # 1. create temp dir
    reddir = tempfile.mkdtemp(prefix='gp1astrphot', dir=os.path.expanduser(tmpdir))
    log.debug('Created tempdir %s', reddir)
    
    # 2. copy image over
    log.debug('Copying image over')
    if copy_hook:
        copy_hook(image, os.path.join(reddir, 'input.fits'))
    else:
        shutil.copy(image, os.path.join(reddir, 'input.fits'))
    
    # 3. generate ini file
    inif = os.path.join(reddir, 'astrphot-%s.ini' % band)
    cp = astrphot_gencfg(reddir, band, ini_override)
    with open(inif, 'w') as f:
        cp.write(f)
    
    # 4. run pipeline
    proc = subprocess.Popen(['gr_astrphot.py', inif],
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)
    while True:
        line = proc.stderr.readline()
        if not line:
            break
        log.debug(line.strip())
    proc.wait()
    log.debug("Got returncode: %d", proc.returncode)
    # TODO: check if 0
    
    # 5. callback (copies relevant files)
    ret = callback(reddir)
    
    # 6. delete temp dir
    log.debug('Deleting %s..', reddir)
    #shutil.rmtree(reddir)
    
    return ret

def hash_astrphot(image, band, hash_image_func, ini_override={}):
    # Get version of the gp1 pipeline
    gp_version = get_version()
    
    # generate config
    cp = astrphot_gencfg('', band, ini_override)
    # delete references to data reduction directory and image filenames
    cp.remove_option('task', 'images')
    cp.remove_option('task', 'workdir')
    
    # hash input images
    log.debug('Hashing input image')
    image_hash = hash_image_func(image)
    
    data = StringIO()
    data.write('GP version: %s\nImage Hash: %s' % (gp_version, image_hash))
    
    data.write('\nConfig:\n')
    cp.write(data)
    
    h = hashlib.new('sha1')
    h.update(data.getvalue().encode('utf-8'))
    data.close()
    
    # TODO: log this structure? 
    
    return h.hexdigest()
