import shlex


def parse_sex(cfg):
    tokens = shlex.split(cfg, comments=True)
    assert len(tokens) % 2 == 0
    return dict(zip(tokens[0::2], tokens[1::2]))

def write_sex(cfg, dest):
    with open(dest, 'w') as f:
        for k in cfg:
            f.write('%s\t%s\n' % (k, cfg[k]))
