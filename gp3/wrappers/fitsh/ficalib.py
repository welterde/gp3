import subprocess
import logging

from gp3.pipe import Recipe, Requirement, Product, Parameter

class Ficalib(Recipe):
    
    img_in = Requirement(Image)
    img_out = Product(Image)
    
    master_bias = Requirement(Image, optional=True)
    master_dark = Requirement(Image, optional=True)
    master_flat = Requirement(Image, optional=True)
    
    post_scale = Parameter(float, default=None, optional=True)
    
    def run(self):
        in_fname = self.img_in.data.to_file(readonly=True)
        datamodel = self.img_in.data.datamodel
        out_fname, out_finalize = Image.from_newfile(dag=self.dag, create=False, datamodel=datamodel)
        
        args = ['ficalib', '-o', out_fname, '-i', in_fname]
        
        if self.master_bias.present:
            args.append('--input-master-bias')
            args.append(self.master_bias.data.to_file(readonly=True))
        
        if self.master_dark.present:
            args.append('--input-master-dark')
            args.append(self.master_dark.data.to_file(readonly=True))
            # setup exptime correction from DataModel
            
        
        if self.master_flat.present:
            args.append('--input-master-flat')
            args.append(self.master_flat.date.to_file(readonly=True))
        
        # TODO: setup mosaic, overscan, gain, trim, etc. from DataModel
        
        if self.post_scale.data is not None:
            args.append('--post-scale')
            args.append('%f' % self.post_scale.data)
        
        proc = subprocess.Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        while True:
            line = proc.stdout.readline()
            if not line:
                break
            #log.debug(line.strip())
        proc.wait()
        # TODO: do the logging and take returncode into consideration
        img_out = out_finalize()
        # FIXME: change headers at this stage..?
        self.img_out.data = img_out
        
