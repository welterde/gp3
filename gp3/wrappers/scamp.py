import logging
import tempfile
import subprocess
import shutil
import pkgutil
import os



import gp3.conf as conf
import gp3.wrappers.utils as utils


log = logging.getLogger('gp3.wrappers.scamp')



def scamp(catalogs, callback, ini={}, template='default.scamp', copy_cats=True,
          workdir=None, preserve_workdir=False, tmpdir=conf.pipeline.tmpdir):
    # 1. create temp dir
    if workdir:
        reddir = workdir
    else:
        reddir = tempfile.mkdtemp(prefix='scamp', dir=os.path.expanduser(tmpdir))
        log.debug('Created tempdir %s', reddir)
    
    # 2. copy catalogs
    if copy_cats:
        log.debug('Copying %d files over', len(catalogs))
        ncatalogs = ['input-%03d.cat' % i for i in range(len(catalogs))]
        for cat, ncat in zip(catalogs, ncatalogs):
            shutil.copy(cat, os.path.join(reddir, ncat))
    else:
        ncatalogs = catalogs
    
    # 3. generate configuration files
    cfg = utils.parse_sex(pkgutil.get_data('gp3.wrappers', 'scamp/%s' % template).decode('utf8'))
    # TODO: read some parameters from input file
    cfg.update(ini)
    utils.write_sex(cfg, os.path.join(reddir, 'scamp.cfg'))
    
    # 4. run scamp
    args = ['scamp', '-c', 'scamp.cfg']
    args.extend(ncatalogs)
    proc = subprocess.Popen(args, cwd=reddir,
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE)
    while True:
        line = proc.stdout.readline()
        if not line:
            break
        log.debug(line.strip())
    proc.wait()
    log.debug("Got returncode: %d", proc.returncode)
    
    # 5. callback
    ret = callback(reddir, zip(catalogs, ncatalogs))
    
    if not preserve_workdir:
        log.debug('Deleteing %s..', reddir)
        #shutil.rmtree(reddir)
    
    return ret
