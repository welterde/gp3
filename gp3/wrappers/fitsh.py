import subprocess
import logging


log = logging.getLogger('gp3.wrappers.fitsh')

def ficombine(destfile, input_files, mode='median'):
    args = ['ficombine', '-o', destfile, '-m', mode]
    args.extend(input_files)
    log.info('Combining %d files using "%s" mode to %s', len(input_files), mode, destfile)
    log.debug('Will run %s', repr(args))
    proc = subprocess.Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    while True:
        line = proc.stdout.readline()
        if not line:
            break
        log.debug(line.strip())
    proc.wait()
    log.debug("Got returncode: %d", proc.returncode)
    # TODO: refactor this piece of code to utility function
    # TODO: handle non-zero return code (exception)

def ficalib(out_file, in_file, master_bias=None, master_dark=None,
            master_flat=None, saturation=20000, exptime_correction=True,
            post_scale=None, mosaic=None, overscan=None):
    args = ['ficalib', '-s', str(saturation), '-o']
    # TODO: check that number of input and output files match
    if isinstance(out_file, list):
        log.info('Calibrating %d files', len(out_file))
        args.extend(out_file)
    else:
        log.info('Calibrating one file')
        args.append(out_file)
    args.append('-i')
    if isinstance(in_file, list):
        args.extend(in_file)
    else:
        args.append(in_file)
    if master_bias is not None:
        log.debug('Using master bias: %s', master_bias)
        args.append('-B')
        args.append(master_bias)
    if master_dark is not None:
        log.debug('Using master dark: %s', master_dark)
        args.append('-D')
        args.append(master_dark)
    if master_flat is not None:
        log.debug('Using master flat: %s', master_flat)
        args.append('-F')
        args.append(master_flat)
    if exptime_correction:
        log.debug('Will do exposure time correction')
        args.append('--exptime-correction')
    if isinstance(mosaic, list):
        log.debug('Reducing a mosaic image:')
        for element in mosaic:
            log.debug('\t%s', element)
            args.append('--mosaic')
            args.append(element)
    if overscan:
        log.debug('Overscan: %s', overscan)
        args.append('--overscan')
        args.append(overscan)
    if post_scale:
        log.debug('Rescale mean to %f', post_scale)
        args.append('--post-scale')
        args.append('%f' % post_scale)

    log.debug('Will run %s', repr(args))
    proc = subprocess.Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    while True:
        line = proc.stdout.readline()
        if not line:
            break
        log.debug(line.strip())
    proc.wait()
    log.debug("Got returncode: %d", proc.returncode)
