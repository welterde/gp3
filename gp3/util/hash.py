import xxhash



def hash_file(fname):
    x = xxhash.xxh64()
    with open(fname, 'rb') as f:
        x.update(f.read())
    return x.hexdigest()
