import struct
import functools

try:
    import pylru
except ImportError:
    pylru = False
import leveldb
import numpy as np
import msgpack
import msgpack_numpy

import astropy.units as u
import astropy.coordinates as coordinates
import healpy
import astropy.coordinates.angle_utilities as angle_utils


# Key: [prefix][block-id][coord]

# Special keys:
# .config
# 


DEFAULT_TOLERANCE=0.1*u.arcsec.to(u.deg)

HEALPY_NSIDES = 8192

def get_block_num(ra, dec):
    return healpy.ang2pix(HEALPY_NSIDES, ra, dec, lonlat=True)

def to_key(coord):
    if isinstance(coord, tuple):
        assert np.abs(coord[0]) < 360
        assert np.abs(coord[1]) < 360
        block = get_block_num(coord[0], coord[1])
        return struct.pack('>ldd', block, coord[0], coord[1])
    elif isinstance(coord, coordinates.SkyCoord):
        return to_key((coord.ra.deg, coord.dec.deg))
    else:
        raise NotImplemented('no other coordinate format supported yet')

def from_key(key):
    _block,ra,dec = struct.unpack('>ldd', key)
    return [ra,dec]

# def to_key(coord):
#     if isinstance(coord, tuple):
#         return struct.pack('dd', coord[0], coord[1])
#     else:
#         raise NotImplemented('no other coordinate format supported yet')

# def from_key(key):
#     nums = np.fromstring(key, dtype=np.double)
#     return nums.reshape((len(nums)/2, 2))

def dbg(key):
    return repr(to_key(key).encode('hex'))


class CoordDB(object):
    def __init__(self, dbdir, tolerance=DEFAULT_TOLERANCE, coding='msgpack'):
        self.db = leveldb.LevelDB(dbdir)
        self.tolerance = tolerance
        if coding == 'msgpack':
            self.encode = functools.partial(msgpack.packb, default=msgpack_numpy.encode)
            self.decode = functools.partial(msgpack.unpackb, object_hook=msgpack_numpy.decode, use_list=False)
        else:
            self.encode = None
            self.decode = None
        
        if pylru:
            self.find = pylru.FunctionCacheManager(self._find, 128)
        else:
            self.find = self._find
    
    def get_block(self, block_num):
        return list(self.db.RangeIter(key_from=struct.pack('>l', block_num), key_to=struct.pack('>l', block_num+1), include_value=False))
    
    def _find(self, coord, tolerance=None, closest=False):
        if tolerance == None:
            tolerance = self.tolerance
        # FIXME: this breaks when tolerance larger than block size
        blocks = set()
        blocks.add(get_block_num(coord[0], coord[1]))
        blocks.add(get_block_num(coord[0]-tolerance, coord[1]))
        blocks.add(get_block_num(coord[0]+tolerance, coord[1]))
        blocks.add(get_block_num(coord[0], coord[1]-tolerance))
        blocks.add(get_block_num(coord[0], coord[1]+tolerance))
        blocks.add(get_block_num(coord[0]-tolerance, coord[1]-tolerance))
        blocks.add(get_block_num(coord[0]-tolerance, coord[1]+tolerance))
        blocks.add(get_block_num(coord[0]+tolerance, coord[1]-tolerance))
        blocks.add(get_block_num(coord[0]+tolerance, coord[1]+tolerance))
        
        keys = []
        for block_num in blocks:
            keys.extend(self.get_block(block_num))
        
        #print 'found ', repr(keys)
        #print 'all ', repr(list(self.db.RangeIter()))
        #keys = "".join(keys)
        if len(keys) < 1:
            return (False, None)
        # hmm... best/fastest way to do this?
        #coords = np.fromstring("".join(keys), dtype=np.double)
        #coords = coords.reshape((len(coords)/2,2))
        coords = np.array(map(from_key, keys))
        distance = angle_utils.angular_separation(coord[0], coord[1], coords[:,0], coords[:,1])
        #centerc = coordinates.SkyCoord(ra=coord[0], dec=coord[1], unit=(u.deg, u.deg))
        #distance = coordinates.SkyCoord(coords, unit=(u.deg, u.deg)).separation(centerc).deg
        if closest:
            idx = np.argmin(distance)
            if distance[idx] < tolerance:
                return (True, coords[idx])
            return (False, None)
        else:
            #print repr(distance), centerc, coords
            matches = coords[distance < tolerance]
            if len(matches) < 1:
                return (False, None)
            else:
                return (True, matches)
        
    
    def put(self, coord, data, force=False):
        key = to_key(coord)
        if force:
            #self.db.Put(key, data)
            self._put(key, data)
        else:
            if self.find(coord)[0]:
                raise ValueError('Key already exists.. should use update!')
            self._put(key, data)
    
    def update(self, coord, data):
        ncoord = self.find(coord, closest=True)[1]
        key = to_key((ncoord[0], ncoord[1]))
        self._put(key, data)
    
    def get(self, coord, find=True):
        if find:
            coord = self.find(coord, closest=True)[1]
            #print coord, repr(list(self.db.RangeIter()))
            #print coord
            coord = (coord[0], coord[1])
        key = to_key(coord)
        #print repr(key)
        return self._get(key)
    
    def _put(self, key, data):
        # clear LRU cache, so we don't cache non-existing entries that now exist
        if pylru:
            self.find.clear()
        if self.encode:
            data = self.encode(data)
        self.db.Put(key, data)
    
    def _get(self, key):
        data = self.db.Get(key)
        if self.decode:
            return self.decode(data)
        return data
    
    def __iter__(self):
        for k,v in self.db.RangeIter():
            k_decode = from_key(k)
            if self.decode:
                v_decode = self.decode(v)
            else:
                v_decode = v
            yield k,v
