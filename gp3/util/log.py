import logging


class AddMissingFields(object):
    
    def __init__(self, fields):
        self.fields = fields
    
    def filter(self, record):
        for f in self.fields:
            if not hasattr(record, f):
                setattr(record, f, '')
        return True
