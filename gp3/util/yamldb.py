import yaml
import os

class YAMLDatabase(object):
    """
    Simple database that stores each entry as yaml file in a directory
    """

    def __init__(self, db_dir):
        if not os.path.isdir(db_dir):
            os.makedirs(db_dir)
        self.db_dir = db_dir

    def __getitem__(self, key):
        f = os.path.join(self.db_dir, '%s.yaml' % key)
        if not os.path.isfile(f):
            raise KeyError('%s not found' % key)
        with open(f, 'r') as fd:
            return yaml.load(fd)

    def __setitem__(self, key, val):
        f = os.path.join(self.db_dir, '%s.yaml' % key)
        with open(f, 'w') as fd:
            yaml.dump(val, stream=fd)
    
    def __contains__(self, key):
        f = os.path.join(self.db_dir, '%s.yaml' % key)
        return os.path.exists(f)
