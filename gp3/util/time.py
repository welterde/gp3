import datetime
import sys
import click

def from_isoformat(fragment):
    if sys.version_info >= (3,7):
        return datetime.date.fromisoformat(fragment)
    else:
        dt = datetime.datetime.strptime(fragment, '%Y-%m-%d')
        return datetime.date(dt.year, dt.month, dt.day)


def verify_date(ctx, param, value):
    try:
        return from_isoformat(value)
    except ValueError:
        raise click.BadParameter('%s needs to be in ISO format YYYY-MM-DD (e.g. 2019-03-23)' % param)
