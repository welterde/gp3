import numpy as np
cimport numpy as np

DTYPE = np.float32
ctypedef np.float32_t DTYPE_t

def naive_convolve(np.ndarray[DTYPE_t, ndim=2] input_buf,
                   np.ndarray[DTYPE_t] output_buf):
