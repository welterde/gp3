from astropy.coordinates import SkyCoord
import astropy.units as u



def verify_coords(ctx, param, value):
    # TODO: support more formats..
    try:
        ra, dec = value.split(',')
        return float(ra), float(dec)
    except ValueError:
        raise click.BadParameter('%s needs to be in degree floating point format separated by comma (e.g. 3.14,-32.3)')

def parse_coords(value):
    ra, dec = value.split(',')
    return SkyCoord(float(ra)*u.deg, float(dec)*u.deg)
