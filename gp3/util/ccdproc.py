import logging

import numexpr
import numpy
import time

from astropy.modeling import models, fitting


# Ideas:
# * Arrays with transformations attached to them
#   like for bscale & bzero, throw around the mmaped array
#   with a string 'bzero + bscale*data' attached to it
#   or even better in symbolic form
# * then later transform to LLVM and run


log = logging.getLogger('gp3.ccdproc')

def fit_overscan(model=None):
    if model == None:
        model = models.Legendre1D(1)
    def doit(data):
        of = fitting.LinearLSQFitter()
        yarr = numpy.arange(len(data))
        oscan = of(model, yarr, data)
        return oscan(yarr)
    return doit

def process_image(output, inp, scaling=None, overscan=None, bias=None, darkscale=None, dark=None, flat=None, bzero=None, bscale=None, init=False):
    # construct string to eval
    if init:
        fun = ''
    else:
        fun = 'p + '
    if bzero is not None:
        fun += '(%f + ' % bzero
    else:
        fun += '('
    if bscale is not None:
        fun += '%f*i' % bscale
    else:
        fun += 'i'
    if overscan is not None:
        fun += ' - o'
    if bias is not None:
        fun += ' - b'
    if dark is not None:
        if darkscale is not None:
            fun += ' - ds*d'
        else:
            fun += ' - d'
    if scaling is not None:
        fun += ')/%f' % scaling
    else:
        fun += ')'
    if flat is not None:
        fun += '/f'
    
    log.info('Running "%s"', fun)
    start_t = time.time()
    numexpr.evaluate(fun, out=output, local_dict={
        'p': output,
        'i': inp,
        'o': overscan,
        'b': bias,
        'd': dark,
        'ds': darkscale,
        'f': flat})
    log.debug('Took %f s', time.time() - start_t)
            

def process_avg_images(output, inputs, regions, overscan_fitfun=None, save_fits=False, bias=None, dark=None, darkscale=None, scales=None):
    oscan_fits = []
    for regname, out_reg, osc_reg, dat_reg, osc_axis in regions:
        log.debug('Processing region %s (out=%s; osc=%s; data=%s)', regname, repr(out_reg),
                  repr(osc_reg), repr(dat_reg))
        # slice out the region we want to write to
        out_xs, out_xe, out_ys, out_ye = out_reg
        out = output[out_xs:out_xe, out_ys:out_ye]

        # source regions
        dat_xs, dat_xe, dat_ys, dat_ye = dat_reg

        # FIXME: FutureWarning: comparison to `None` will result in an elementwise object comparison in the future.
        if bias != None:
            bias_cut = bias[out_xs:out_xe,out_ys:out_ye]
        else:
            bias_cut = None

        if dark != None:
            dark_cut = dark[out_xs:out_xe,out_ys:out_ye]
        else:
            dark_cut = None

        first = True
        for hdu, i in zip(inputs, range(len(inputs))):
            log.debug('Processing image %d', i)
            if 'BZERO' in hdu.header:
                bzero = hdu.header['BZERO']
                bscale = hdu.header['BSCALE']
            else:
                bzero = None
                bscale = None
            if darkscale != None and len(darkscale) > 0:
                darkscale_cur = darkscale[i]
            else:
                darkscale_cur = darkscale
            if scales != None and len(scales) > 0:
                scaling_cur = scales[i]
            else:
                scaling_cur = scales
            if scaling_cur == None:
                scaling_cur = len(inputs)
            else:
                scaling_cur *= len(inputs)
            # calculate overscan
            if osc_reg != None:
                # source region
                osc_xs, osc_xe, osc_ys, osc_ye = osc_reg
                oscan = numpy.mean(hdu.data[osc_xs:osc_xe, osc_ys:osc_ye], axis=osc_axis)
                numexpr.evaluate('%f+%f*a' % (bzero, bscale), out=oscan, local_dict={'a': oscan})
                if overscan_fitfun != None:
                    if not save_fits:
                        oscan = overscan_fitfun(oscan)
                    else:
                        unfit = oscan
                        oscan = overscan_fitfun(oscan)
                        oscan_fits.append((hdu, unfit, oscan))
                if osc_axis == 1:
                    oscan = numpy.reshape(oscan, (oscan.size, 1))
                else:
                    oscan = numpy.reshape(oscan, (1, oscan.size))
            else:
                oscan = None
            process_image(out, hdu.data[dat_xs:dat_xe,dat_ys:dat_ye], overscan=oscan, bzero=bzero, bscale=bscale, scaling=scaling_cur, init=first, bias=bias_cut, dark=dark_cut, darkscale=darkscale_cur)
            first = False
    return oscan_fits
