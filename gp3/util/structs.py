import collections

# from http://stackoverflow.com/questions/11351032/named-tuple-and-optional-keyword-arguments
# Author: Mark Lodato 
def namedtuple_with_defaults(typename, field_names, default_values=()):
    """
    Allow namedtuple to have default arguments
    
    Example:
        >>> Node = namedtuple_with_defaults('Node', 'val left right', {'right':7})
        >>> Node()
        Node(val=None, left=None, right=7)
    """
    T = collections.namedtuple(typename, field_names)
    # this requires at least python2.6 I think
    T.__new__.__defaults__ = (None,) * len(T._fields)
    if isinstance(default_values, collections.Mapping):
        prototype = T(**default_values)
    else:
        prototype = T(*default_values)
    T.__new__.__defaults__ = tuple(prototype)
    return T
