import msumastro.header_processing.feder as feder

from astropy.coordinates import EarthLocation
import astropy.units as u


class COGSite(EarthLocation):
    """
    The Campus Observatory Garching site.
    An astropy location with the observatory location pre-set to:
        + `lat` = 46.86678 degrees North
        + `long` = -96.453278 degrees East
        + `height` = 480 meters
        + `name` = COG
    """

    def __new__(cls):
        return EarthLocation.__new__(COGSite,
                                     lat=48.25*u.degree,
                                     lon=11.65*u.degree,
                                     height=490*u.m)

    def __init__(self):
        self._name = 'Campus Observatory Garching'

    @property
    def name(self):
        return self._name

class COGSite(feder.Feder):
    """
    The Campus Observatory Garching site.
    An astropysics site with the observatory location and name pre-set to:
    
    """
    def __init__(self):
        feder.Feder.__init__(self)
        self.site = COGSite()
    
