import os
import logging
import shutil
import glob
import itertools
import datetime
import collections

import astropy.io.fits as fits
import astropy.time as time

import gp3.redcomb.utils as utils
import gp3.wrappers.gp1 as gp1



default_log = logging.getLogger('gp3.redcomb.gp1')

OPTICAL_BANDS = ['g', 'r', 'i', 'z']
NIR_BANDS = ['J', 'H', 'K']
BANDS = OPTICAL_BANDS + NIR_BANDS
CALIB_DIR = os.path.expanduser('~/data/calib')


def filter_files(prefix, fnames):
    return list(filter(lambda f: os.path.basename(f).startswith(prefix), fnames))

def parse_date(date):
    try:
        return time.Time([date])
    except:
        return None

DirsDeltaFlat = collections.namedtuple('DirsDateFlat', 'dirs delta flats')


def findCalibration(band, date, fast=False):
    dirs = filter(os.path.isdir,
                  filter(lambda d: 'bpm' not in d,
                         glob.iglob(os.path.join(CALIB_DIR, '*-*-*'))))

    def flatglob(d):
        if band in 'griz' and fast:
            return set(map(os.path.basename,
                           glob.glob(os.path.join(d, 'flat_*_fast.fits'))))
        else:
            return set(map(os.path.basename,
                           glob.glob(os.path.join(d, 'flat_*.fits'))))

    def get_delta(d):
        day = parse_date(os.path.basename(d))
        if day != None:
            return abs((day-date)[0].sec)

    dirs_delta_flat = filter(lambda a: a.delta != None,
                            map(lambda d: DirsDeltaFlat(d,
                                                       get_delta(d),
                                                       flatglob(d)), dirs))
    if fast:
        matches = filter(lambda a: 'flat_%s_fast.fits' % band in a.flats, dirs_delta_flat)
    else:
        matches = filter(lambda a: 'flat_%s.fits' % band in a.flats, dirs_delta_flat)
    msorted = sorted(matches, key=lambda a: a.delta)
    return msorted[0][0]


def redcomb(src_list, outfile, calibdir=None, bands='all', overwrite=True,
            tdp_stack=True, quiet=False, log=default_log, extra_args={}):
    bands = utils.parse_bands(BANDS, bands)
    if len(bands) > 1:
        if '%b' not in outfile:
            raise ValueError('Multiple bands specified but no %%b in outfile: "%s"' % outfile)
    if not tdp_stack:
        if '%t' not in outfile:
            raise ValueError('No TDP stacking specified but no %%t in outfile: "%s"' % outfile)
    
    for band in bands:
        calib_dir = calibdir
        destf = outfile.replace('%b', band)
        if os.path.exists(destf) and not overwrite:
            raise IOError('%s exists (and no overwrite)' % destf)
        
        if os.path.isdir(src_list[0]):
            if band in OPTICAL_BANDS:
                images = glob.glob(os.path.join(src_list[0], 'GROND_O*.???_????.fits'))
            else:
                assert band in NIR_BANDS
                images = glob.glob(os.path.join(src_list[0], 'GROND_I*.???_????.fits'))
        else:
            if band in OPTICAL_BANDS:
                images = filter_files('GROND_O', src_list)
            else:
                assert band in NIR_BANDS
                images = filter_files('GROND_I', src_list)
        
        def cb(reddir):
            if tdp_stack:
                shutil.move(os.path.join(reddir, 'GROND_%s_OB.fits' % band), destf)
            else:
                tdps = glob.glob(os.path.join(reddir, 'GROND_%s_TDP*_cor.fits' % band))
                for tdpf, ti in zip(tdps, range(len(tdps))):
                    dest = destf.replace('%t', str(ti))
                    shutil.move(tdpf, dest)
        calib_file_suffix = ''
        if band in OPTICAL_BANDS:
            hdr = fits.getheader(images[0])
            fast = hdr['HIERARCH ESO DET READ CLOCK'] == 'CCD 0/1/2/3 225kHz'
            if fast:
                calib_file_suffix = '_fast'
        else:
            fast = None
        
        log.info('No calib directory specified. Searching..')
        if calib_dir == None:
            hdul = fits.open(images[0])
            day = time.Time([hdul[0].header['DATE-OBS']])
            calib_dir = findCalibration(band, day, fast=fast)
            log.info('Will use %s', calibdir)
        
        ini = {
            'masterflat': os.path.join(calib_dir, 'flat_%s%s.fits' % (band, calib_file_suffix)),
            'masterbias': os.path.join(calib_dir, 'bias_%s%s.fits' % (band, calib_file_suffix)),
            'masterdark': os.path.join(calib_dir, 'dark_%s%s.fits' % (band, calib_file_suffix)),
            'masterbpm': os.path.expanduser('~/data/calib/bpm/bpm_%s.fits' % band)
        }
        if not tdp_stack:
            ini['cleanup'] = 'MDP'

        for k in extra_args.keys():
            ini[k] = extra_args[k]
        
        gp1.redcomb(images, band, cb, quiet=quiet, ini_override=ini)
