


def parse_bands(valid_bands, args):
    if args == 'all':
        return valid_bands
    bands = args.split(',')
    invalid_bands = list(filter(lambda a: a not in valid_bands, bands))
    if len(invalid_bands) > 0:
        raise ValueError('Invalid band(s) %s (Valid bands:  %s)' % (repr(invalid_bands), repr(valid_bands)))
    
    return set(bands)
