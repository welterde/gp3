import gp3.wrappers.fitsh as fitsh

MOSAIC_SETUP = {
    # FIXME: flat field in g shows rather low flux towards the top.. so probably should cut away even more?
    # or just rely on the weightmap while stacking
    'g': [
        'size=[2048,2048]',
        '[name=CCDg,image=[49:49:1072:2095],overscan=[area=0:49:49:2095,spline,order=3,iterations=3,sigma=3],offset=[0,0]]',
        '[name=CCDg,image=[1127:49:2150:2095],overscan=[area=2151:49:2200:2095,spline,order=3,iterations=3,sigma=3],offset=[1024,0]]'
    ],
    'r': [
        'size=[2048,2025]',
        '[name=CCDr,image=[49:23:1072:2095],offset=[0,0]]',
        '[name=CCDr,image=[1127:23:2150:2095],offset=[1024,0]]',
    ]
}


def process_image(out_file, in_file, band=None, master_bias=None, master_dark=None, master_flat=None, post_scale=None):
    mosaic = MOSAIC_SETUP[band]

    fitsh.ficalib(out_file, in_file, master_bias=master_bias, master_dark=master_dark,
                  master_flat=master_flat, saturation=90000, exptime_correction=False,
                  post_scale=post_scale, mosaic=mosaic)
            
