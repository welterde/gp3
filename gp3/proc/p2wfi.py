import gp3.wrappers.fitsh as fitsh

# TODO: lookup somewhere actual saturation level
SATURATION_LEVEL = 90000


# taken from THELI WFI.ini:
## The chip geometry:
## The first two numbers give the chips in each row/column,
## the second two numbers the chip dimensions.
# CHIPGEOMETRY="4 2 2032 4092"
## overscan region
## the values are the X-range; in general, we need separate regions
## for all the chips
# OVSCANX1=([5]=10 [6]=10 [7]=10 [8]=10 [4]=2105 [3]=2105 [2]=2105 [1]=10)
# OVSCANX2=([5]=40 [6]=40 [7]=40 [8]=40 [4]=2135 [3]=2135 [2]=2135 [1]=40)
## sections for exposure cutting:
# CUTX=([5]=51 [6]=51 [7]=51 [8]=51 [4]=51 [3]=51 [2]=51 [1]=51)
# CUTY=([5]=31 [6]=31 [7]=31 [8]=31 [4]=2  [3]=2  [2]=2  [1]=2)
# SIZEX=([5]=2032 [6]=2032 [7]=2032 [8]=2032 [4]=2032 [3]=2032 [2]=2032 [1]=2032)
# SIZEY=([5]=4092 [6]=4092 [7]=4092 [8]=4092 [4]=4092 [3]=4092 [2]=4092 [1]=4092)


# (x0, x1)
overscan_region_x = {
    5: (10, 40),
    6: (10, 40),
    7: (10, 40),
    8: (10, 40),
    4: (2105, 2135),
    3: (2105, 2135),
    2: (2105, 2135),
    1: (10, 40)
}
# FIXME: chip1 seems odd in that respect..?

# (x0, y0, dx, dy)
trim_region = {
    5: (51, 31, 2032, 4092),
    6: (51, 31, 2032, 4092),
    7: (51, 31, 2032, 4092),
    8: (51, 31, 2031, 4092),
    4: (51, 2, 2032, 4092),
    3: (51, 2, 2032, 4092),
    2: (51, 2, 2032, 4092),
    1: (51, 2, 2032, 4092)
}

def chip2extname(chip_num):
    # WHY???
    if chip_num == 5:
        return 'WIN1.CHIP5.OUT2'
    else:
        return 'WIN1.CHIP%d.OUT1' % chip_num

ext_name = map(chip2extname, range(1, 9))


def process_image(out_file, in_file, chip=None, master_bias=None, master_dark=None, master_flat=None, post_scale=None):
    oscan_x0, oscan_x1 = overscan_region_x[chip]
    (x0, y0, size_x, size_y) = trim_region[chip]

    # FIXME: or is it -1...?
    max_x = x0 + size_x - 1
    max_y = y0 + size_y - 1

    mosaic = [
        'size=[%d,%d]' % (size_x, size_y),
        '[name=%s,image=[%d:%d:%d:%d],overscan=[area=%d:%d:%d:%d,spline,order=3,iterations=3,sigma=3],offset=[0,0]]' % (ext_name[chip],
                                                                                                                        x0, y0, max_x, max_y,
                                                                                                                        oscan_x0, y0, oscan_x1, max_y)
    ]

    fitsh.ficalib(out_file, in_file, master_bias=master_bias, master_dark=master_dark,
                  master_flat=master_flat, saturation=SATURATION_LEVEL, exptime_correction=False,
                  post_scale=post_scale, mosaic=mosaic)
    
    
