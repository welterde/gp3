import ccdproc
import logging
import numpy as np
import os
import astropy.time as time
import collections

import gp3.calibred.tools as tools



log = logging.getLogger('gp3.calibred')

def create_master_bias(src_list):
    # TODO: do overscan subtraction + trimming here?
    log.info("Combining %d files" % len(src_list))
    biases = ccdproc.Combiner(src_list)
    log.debug("Computing average..")
    mbias= biases.average_combine()
    mbias.meta['DATE-OBS'] = time.Time(np.median(time.Time([ob.meta['DATE-OBS'] for ob in src_list]).mjd), format='mjd', scale='utc').isot
    return mbias

def img_stats(bias_frame):
    log.debug("Computing statistics...")
    bins, bin_edges = np.histogram(bias_frame.data, bins=100)
    # TODO: consider masks
    return {
        'low': bias_frame.data.min(),
        'high': bias_frame.data.max(),
        'avg': bias_frame.data.mean(),
        'std': bias_frame.data.std(),
        'hist': {
            'bins': bins,
            'edges': bin_edges
        },
        'mjd': time.Time(bias_frame.meta['DATE-OBS']).mjd
    }
    
def create_master_dark(src_list):
    # TODO: do overscan subtraction + trimming here?
    log.info("Combining %d files" % len(src_list))
    darks = ccdproc.Combiner(src_list)
    log.debug("Computing median..")
    mdark = darks.median_combine(median_func=tools.bn_median)
    mdark.meta['DATE-OBS'] = time.Time(np.median(time.Time([ob.meta['DATE-OBS'] for ob in src_list]).mjd), format='mjd', scale='utc').isot
    return mdark

RON_CUTOFF=60

RONVal = collections.namedtuple('RONVal', 'time dt low high avg std')

def ron_stats(bias_list):
    times = sorted(
        map(lambda bf: (bf, time.Time(bf.meta['DATE-OBS'])), bias_list),
        key=lambda el: el[1])
    dts = map(lambda i: times[i+1][1] - times[i][1], range(len(bias_list) - 1))
    
    ret={}
    
    vals=[]
    for i,dt in zip(range(len(dts)), dts):
        if dt.sec > RON_CUTOFF:
            continue
        # calculate difference image (should be the same => diff is read-out noise)
        diff = times[i][0].subtract(times[i+1][0])
        
        vals.append(RONVal(time=times[i][1].mjd,
                           dt=dt.sec,
                           low=float(times[i][0].data.min()),
                           high=float(times[i][0].data.max()),
                           avg=float(times[i][0].data.mean()),
                           std=float(times[i][0].data.std()),
                       ))
    ret['vals'] = map(lambda a: a._asdict(), vals)
    for kw in ['low', 'high', 'avg', 'std']:
        ret[kw] = float(np.median(map(lambda v: v._asdict()[kw], vals)))
    ret['mjd'] = times[0][1].mjd
    return ret
    


def autocalib(src_list, dest_dir, overwrite):
    meta={}
    typed = zip(src_list, map(tools.imagetyp, src_list))
    exptimed = zip(typed, map(tools.extract_header('EXPTIME'), src_list))
    
    log.info("--> Creating master bias")
    bias_files = map(lambda (f,t): f, filter(lambda (f,t): t == 'zero', typed))
    if len(bias_files) > 3:
        bias_files = map(tools.load_file, bias_files)
        master_bias = create_master_bias(bias_files)
        tools.write_file(master_bias, os.path.join(dest_dir, 'BIAS.fits'), mef=False, overwrite=overwrite)
        #write_file(master_bias, os.path.join(dest_dir, 'BIAS_mef.fits'), mef=True)
        meta['bias:master'] = img_stats(master_bias)
        
        meta['ron'] = ron_stats(bias_files)
    else:
        log.warn("Not creating master bias.. not enough bias files: %s" % len(bias_files))
    
    # collect the exptimes needed for the flatfields
    flat_exptimed = filter(lambda ((f,it),et): it == 'flat', exptimed)
    dark4flats = sorted(set(map(lambda (a,et): int(et), flat_exptimed)))
    mdarks4flats = {}
    
    log.info("--> Creating per-exptime master darks")
    dark_exptimed = filter(lambda ((f,it),et): it == 'dark', exptimed)
    # TODO: rename
    def foobar(acc, tup):
        f,e = tup
        e=int(e)
        acc.setdefault(e,0)
        acc[e]+=1
        return acc
    exptimes = reduce(foobar, dark_exptimed, {})
    for exptime in exptimes.keys():
        if exptimes[exptime] < 3:
            log.warn("Skipping %d s(<3 frames)" % exptime)
            continue
        log.debug("=> Exptime=%d" % exptime)
        mdark = create_master_dark(
            map(tools.load_file,
                map(lambda ((f,i),e): f,
                    filter(lambda (a,e): int(e)==exptime,
                           dark_exptimed))))
        tools.write_file(mdark, os.path.join(dest_dir, 'DARK-%03ds.fits' % exptime), mef=False)
        meta['dark:%ds' % exptime] = img_stats(mdark)
        if exptime in dark4flats:
            mdarks4flats[exptime] = mdark
    
    
    # TODO: create scalable master dark?
    
    tools.write_meta(meta, os.path.join(dest_dir, "meta.msgpack"))
