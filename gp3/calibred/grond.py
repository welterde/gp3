import fnmatch
import functools

import gp3.calibred.generic as calred

def filter_filename(filename, args):
    return filter(lambda (f, _): fnmatch.fnmatch(f, filename), args)


class GRONDCalRed(calred.MultiCalRed):
    def __init__(self, files, opts, target_dir):
        super(GRONDCalRed, self).__init__(files, opts, target_dir, {
            'nir': GRONDNIRCalRed,
            'opt': GRONDOptCalRed
            })
    
    def group_inputs(self, file_hdu):
        ret = []
        # pre-filter by filename
        # TODO: simplify this?
        bias_opt = filter_filename('GROND_BIAS*.fits', file_hdu)
        dark_opt = filter_filename('GROND_OPTDARK.*.fits', file_hdu)
        dark_nir = filter_filename('GROND_IRDARK.*.fits', file_hdu)
        flat_opt = filter_filename('GROND_OIMG.*.fits', file_hdu)
        flat_nir = filter_filename('GROND_IRIM.*.fits', file_hdu)

        return {
 #           'nir': dark_nir + flat_nir,
            'opt': bias_opt + dark_opt + flat_opt
            }    

class GRONDNIRCalRed(calred.MosaicCalRed):
    def __init__(self, files, opt, target_dir):
        super(GRONDNIRCalRed, self).__init__(files, opt, target_dir)
    


def get_grond_regions(band):
    if band == 'g':
        return [
            ('A',
             (0, 2052, 0, 1024),     # target region
             (48, 2100, 0, 49),      # overscan region
             (48, 2100, 49, 1073),   # data region
             1                       # axis
            ),
            ('B',
             (0, 2052, 1024, 2048),  # target region
             (48, 2100, 2151, 2200), # overscan region
             (48, 2100, 1127, 2151), # data region
             1                       # axis
             )]
    elif band == 'r':
        return [
            ('A',
             (0, 2052, 0, 1024),     # target region
             (0, 2052, 0, 49),       # overscan region
             (0, 2052, 49, 1073),    # data region
             1
             ),
             ('B',
              (0, 2052, 1024, 2048),  # target region
              (0, 2052, 2151, 2200),  # overscan region
              (0, 2052, 1127, 2151),  # data region
              1                       # axis
              )]
    elif band == 'i':
        return [
            ('A',
             (0, 2052, 0, 1024),     # target region
             (0, 2052, 0, 49),       # overscan region
             (0, 2052, 50, 1074),    # data region
             1
             ),
             ('B',
              (0, 2052, 1024, 2048),  # target region
              (0, 2052, 2151, 2200),  # overscan region
              (0, 2052, 1127, 2151),  # data region
              1                       # axis
              )]
    elif band == 'z':
        return [
            ('A',
             (0, 2052, 0, 1024),     # target region
             (0, 2052, 0, 49),       # overscan region
             (0, 2052, 50, 1074),    # data region
             1
             ),
             ('B',
              (0, 2052, 1024, 2048),  # target region
              (0, 2052, 2151, 2200),  # overscan region
              (0, 2052, 1127, 2151),  # data region
              1                       # axis
              )]
    elif band == 'J':
        return [
            ('',
             (0, 1024, 0, 1024),      # target region
             None, # no overscan
             (0, 1024, 0, 1024),      # source region
             1)
             ]
    elif band == 'H':
        return [
            ('',
             (0, 1024, 0, 1024),      # target region
             None, # no overscan
             (0, 1024, 1024, 2048),   # source region
             1)
             ]
    elif band == 'K':
        return [
            ('',
             (0, 1024, 0, 1024),      # target region
             None, # no overscan
             (0, 1024, 2048, 3072),   # source region
             1)
             ]
    else:
        raise ValueError('Unknown GROND band %s' % repr(band))

def get_grond_dims(band):
    if band in 'griz':
        return (2048, 2052)
    elif band in 'JHK':
        return (1024, 1024)
    else:
        raise ValueError('Unknown GROND band %s' % repr(band))
        
class GRONDOptCalRed(calred.MosaicCalRed):
    def __init__(self, files, opt, target_dir):
        super(GRONDOptCalRed, self).__init__(files, opt, target_dir)

    def setup_processing(self, file_hdu):
        bias_slow = filter_filename('GROND_BIAS.*.fits', file_hdu)
        bias_fast = filter_filename('GROND_BIASF.*.fits', file_hdu)
        dark = filter_filename('GROND_OPTDARK.*.fits', file_hdu)
        flat = filter_filename('GROND_OIMG.*.fits', file_hdu)

        ret = []
        for band in 'griz':
            regions = get_grond_regions(band)
            dims = get_grond_dims(band)
            bias_fn = functools.partial(self.bias, regions=regions, dims=dims)
            ret.append((bias_fn, '%s_slow' % band,
                        map(lambda hdul: hdul[1]['CCD%s' % band], bias_slow)))
            ret.append((bias_fn, '%s_fast' % band,
                        map(lambda hdul: hdul[1]['CCD%s' % band], bias_fast)))
            ret.append((functools.partial(self.dark, use_bias='%s_slow' % band, regions=regions,
                                          dims=dims),
                        band, map(lambda hdul: hdul[1]['CCD%s' % band], dark)))
            ret.append((functools.partial(self.flat, use_bias='%s_slow' % band,
                                          use_dark=band, regions=regions, dims=dims,
                                          countlimits=(5000, 55000)), band,
                                          map(lambda hdul: hdul[1]['CCD%s' % band], flat)))
        return ret



class GRONDOptBias(pipe.Recipe):
    bias_frames = pipe.Requirement(pipe.ListOfType(CCDFrame))
    master_frame = pipe.Output(CCDFrame)
    # TODO: support for enums
    band = pipe.Parameter(str)
    
    def run(self):
        regions = get_grond_regions(self.band.value)
        
    
