import collections
import os
import logging

import numpy
import numexpr
import astropy.io.fits as fits

import gp3.util.ccdproc as ccdproc

log = logging.getLogger('gp3.calibred')

# TODO: add emit_qc_info function?
# gets thrown away if not running with --output-qc foo.msgpack

def create_fits(fname, dims, header_len, copy_header=None):
    # based on http://astropy.readthedocs.org/en/latest/io/fits/appendix/faq.html#id13
    data = numpy.empty((100, 100), dtype=numpy.float64)
    hdu = fits.PrimaryHDU(data=data, header=copy_header)
    header = hdu.header
    while len(header) < header_len:
        # append blank cards until enough memory for header allocated
        header.append()
    header['NAXIS1'], header['NAXIS2'] = dims
    
    # write just the header
    header.tofile(fname)

    # now increase file size to be large enough
    # should be a sparse allocation
    with open(fname, 'rb+') as fobj:
        fobj.seek(len(header.tostring()) + (dims[0] * dims[1] * 8) - 1)
        fobj.write('\0')

def handle_create_fits(calibtype):
    def deco(f):
        def wrapper(*args, **kwargs):
            # (self, name, hdus, ... dims=...)
            name = args[1]
            hdus = args[2]
            if 'dims' not in kwargs:
                dims = hdus[0].shape
            else:
                dims = kwargs['dims']
            tgt_fname = self.get_filename(calibtype, name)
            if os.path.isfile(tgt_fname):
                create_fits(tgt_fname, dims, len(hdus[0].header)+50, hdus[0].header)
            hdu_list = fits.open(tgt_name, mode='update', memmap=True)
            kwargs['dims'] = dims
            return f(args[0], hdu_list[0], *args[2:], **kwargs)
        return f
    return deco
            

class CalibReduction(object):

    def __init__(self, files, opts, target_dir):
        log.debug('init CalibRed')
        self.files = files
        self.opts = opts
        self.target_dir = target_dir
    
    def execute(self):
        # load all image files
        log.info('Loading input files..')
        def load_file(fname):
            log.debug('Loading %s', fname)
            return (fname, fits.open(fname, memmap=True, do_not_scale_image_data=True))
        fname_hdul = map(load_file, self.files)

        # find out what we have to do with what HDUs
        steps = self.setup_processing(fname_hdul)

        # now do those things
        for step in steps:
            func, args = step[0], step[1:]
            func(*args)

    def setup_processing(self, file_hdul):
        """
        :returns: (func, *args)
        """
        raise NotImplemented()

    def get_filename(self, ftype, fname):
        if len(fname) > 0:
            return os.path.join(self.target_dir, '%s_%s.fits' % (ftype, fname))
        else:
            return os.path.join(self.target_dir, '%s.fits' % ftype)

    @handle_create_fits('bias')
    def bias(self, out_hdu, hdus, dims=None, regions=None):
        # TODO: fix headers?

        if regions == None:
            # no overscan region
            if dims != hdus[0].shape:
                raise ValueError('Shapes wrong: %s vs %s' % (repr(dims), repr(hdus[0].shape)))
            region = (0, 0, dims[0], dims[1])
            ccdproc.process_avg_images(out_hdu.data, hdus, ('A', region, None, region, 0))
        else:
            ccdproc.process_avg_images(out_hdu.data, hdus, regions)

    @handle_create_fits('dark')
    def dark(self, out_hdu, hdus, dims=None, regions=None, use_bias=None):
        # TODO: fix headers?

        if use_bias != None:
            bias = fits.open(self.get_filename('bias', use_bias), memmap=True)[0].data
        else:
            bias = None

        if regions == None:
            # no overscan region
            if dims != hdus[0].shape:
                raise ValueError('Shapes wrong: %s vs %s' % (repr(dims), repr(hdus[0].shape)))
            region = (0, 0, dims[0], dims[1])
            ccdproc.process_avg_images(out_hdu.data, hdus, ('A', region, None, region, 0), bias=bias)
        else:
            ccdproc.process_avg_images(out_hdu.data, hdus, regions, bias=bias)

    @handle_create_fits('flat')
    def flat(self, out_hdu, hdus, dims=None, regions=None, use_bias=None, use_dark=None, rescale_dark=True, countlimits=None, countlimits_region=None):
        # TODO: fix headers maybe?

        if use_bias != None:
            bias = fits.open(self.get_filename('bias', use_bias), memmap=True)[0].data
        else:
            bias = None

        if use_dark != None:
            # TODO: support use_dark=[list of darks] and then match EXPTIME of flat with darks
            dark_hdu = fits.open(self.get_filename('dark', use_dark), memmap=True)
            dark = dark_hdu[0].data
            dark_exptime = dark_hdu[0].header['EXPTIME']
            if rescale_dark:
                dark_scales = map(lambda hdu: hdu.header['EXPTIME'] / dark_exptime, hdus)
            else:
                dark_scales = None
        else:
            dark = None
            dark_scales = None

        if countlimits != None:
            if countlimits_region == None:
                countlimits_region = (0, hdus[0].data.shape[0], 0, hdus[0].data.shape[1])
            log.info('Performing filtering based on counts (lower=%d, upper=%d)', countlimits[0], countlimits[1])
            cl_xs, cl_xe, cl_ys, cl_ye = countlimits_region
            cl_size = (cl_xe - cl_xs)*(cl_ye - cl_ys)
            cl_lower, cl_higher = countlimits
            def mean_counts(hdu):
                #log.debug('Shape=%s; input=%s; size=%d', repr(hdu.data.shape), repr(hdu.data), cl_size)
                data = hdu.data[cl_xs:cl_xe, cl_ys:cl_ye]
                mean = numexpr.evaluate('sum(a)', local_dict={'a': data})
                
                if 'BZERO' in hdu.header:
                    bzero, bscale = hdu.header['BZERO'], hdu.header['BSCALE']
                    mean = (bzero*cl_size + bscale*mean)/cl_size
                else:
                    mean = mean / cl_size
                
                log.debug('Got %s', repr(mean))
                return (hdu, mean)
            def match_counts((hdu, mean)):
                if cl_lower < mean < cl_higher:
                    return True
                else:
                    log.info('Rejected.. %f outside of (%f, %f)', mean, cl_lower, cl_higher)
                    return False
            hdus,scales = zip(*filter(match_counts, map(mean_counts, hdus)))
            #scales = map(lambda m: m, scales)
        else:
            scales = None
        if regions == None:
            # no overscan region
            if dims != hdus[0].shape:
                raise ValueError('Shapes wrong: %s vs %s' % (repr(dims), repr(hdus[0].data.shape)))
            region = (0, 0, dims[0], dims[1])
            ccdproc.process_avg_images(out_hdu.data, hdus, ('A', region, None, region, 0), bias=bias, dark=dark, darkscale=dark_scales, scales=scales)
        else:
            ccdproc.process_avg_images(out_hdu.data, hdus, regions, bias=bias, dark=dark, darkscale=dark_scales, scales=scales)
            

class MultiCalRed(CalibReduction):

    def __init__(self, files, opts, target_dir, recipes):
        super(MultiCalRed, self).__init__(files, opts, target_dir)
        self.recipes = recipes

    def group_inputs(self, file_hdu):
        raise NotImplemented()

    def setup_processing(self, file_hdu):
        groups = self.group_inputs(file_hdu)

        ret = []
        for group in groups.keys():
            def doit(files):
                c = self.recipes[group](files, self.opts, self.target_dir)
                c.execute()
            ret.append((doit, map(lambda (fn, _): fn, groups[group])))
        return ret
            


class ImagerCalRed(CalibReduction):
    pass


class MosaicCalRed(ImagerCalRed):
    def __init__(self, files, opts, target_dir):
        super(MosaicCalRed, self).__init__(files, opts, target_dir)
        
        
    def group_inputs(self, file_hdu):
        """
        Keyword arguments:
        file_hdu -- list of (filename, hdulist) tuples
        Returns:
        list of (function, name, *args) tuples
        """
        raise NotImplemented()

class SpectrographCalRed(CalibReduction):
    pass

OverscanInfo = collections.namedtuple('OverscanInfo', 'startcol endcol startrow endrow')

FitOverscan = collections.namedtuple('FitOverscan', 'xstart xstop ystart ystop axis')
TrimImage = collections.namedtuple('TrimImage', 'xstart xstop ystart ystop')
