import astropy.io.fits as fits
import ccdproc
import numpy as np
import os
import logging
import msgpack
import msgpack_numpy
import bottleneck
import fnmatch

import gp3.calibred.grond as grond_calib


log = logging.getLogger('gp3.calibred')

def find_files(files):
    # TODO: improve slightly and move to gp3.utils as find_fit_files
    ret = []
    for f in files:
        if os.path.isdir(f):
            for w_dir, w_dirs, w_files in os.walk(f):
                ret.extend(map(lambda f: os.path.join(w_dir, f), fnmatch.filter(w_files, '*.fit?')))
        else:
            ret.append(f)
    return ret

def guess_instrument(files):
    hdu_list = fits.open(files[0], memmap=True, do_not_scale_image_data=True)
    if 'INSTRUME' in hdu_list[0].header:
        inst = hdu_list[0].header['INSTRUME']
        if inst == 'GROND':
            return 'grond'

def autocalib_grond(files, target_dir):
    cal = grond_calib.GRONDCalRed(files, None, target_dir)
    cal.execute()

def autocalib_inst(files, target_dir):
    files = find_files(files)
    inst = guess_instrument(files)
    if inst == 'grond':
        return autocalib_grond(files, target_dir)

def load_file(path):
    log.debug("Loading %s" % path)
    hdul = fits.open(path, mode='readonly', memmap=True)
    hdu = hdul[0]
    meta = hdu.header
    meta['filename'] = path
    return ccdproc.CCDData(data=hdu.data, meta=meta, unit='electron')

def write_file(data, dest, mef=True, overwrite=True):
    hdu_list = data.to_hdu()
    if mef:
        uncertainty_header = fits.Header()
        #uncertainty_header['bunit'] = u.photon.to_string()
        uncertainty_header['name'] = 'uncertainty'
        uncertainty_hdu = fits.ImageHDU(data=data.uncertainty.array, header=uncertainty_header, name='uncertainty')
        hdu_list.append(uncertainty_hdu)
        
        mask_header = fits.Header()
        mask_header.add_comment('TRUE indicates the data is INVALID')
        # FITS isn't fond of boolean images -- convert it to 16 bit integers, nonzero indicates True
        # TODO: needs more experimentation
        mask_array = np.zeros_like(data.mask, dtype=np.int16)
        mask_array[data.mask] = 1
        mask_hdu = fits.ImageHDU(data=mask_array, header=mask_header, name='mask')
        hdu_list.append(mask_hdu)
    
    hdu_list.writeto(dest, clobber=overwrite)

def write_meta(data, dest):
    with open(dest, 'w') as f:
        f.seek(0)
        msgpack.pack(data, f, default=msgpack_numpy.encode)
        f.truncate()

def extract_header(keyname):
    def func(path):
        hdul = fits.open(path, mode='readonly')
        return hdul[0].header[keyname]
    return func

#def imagetyp(path):
#    hdul = fits.open(path, mode='readonly')
#    return hdul[0].header['imagetyp']
imagetyp=extract_header('imagetyp')

def autocalib(src_list, dest_dir, overwrite, create_master_bias):
    typed = zip(src_list, map(imagetyp, src_list))
    
    log.info("-> Creating master bias")
    bias_files = map(lambda (f,t): f, filter(lambda (f,t): t == 'zero', typed))
    master_bias = create_master_bias(map(load_file, bias_files))
    write_file(master_bias, os.path.join(dest_dir, 'BIAS.fits'), mef=False, overwrite=overwrite)
    #write_file(master_bias, os.path.join(dest_dir, 'BIAS_mef.fits'), mef=True)

def combine_images(img_list, comb_func=np.median):
    shapes = set(map(np.shape, img_list))
    if len(shapes)>1:
        raise ValueError("Incompatible sizes: %s" % repr(shapes))
    
def bn_median(masked_array, axis=None):
    """
    Perform fast median on masked array
    
    Parameters
    ----------
    
    masked_array : `numpy.ma.masked_array`
        Array of which to find the median.
    
    axis : int, optional
        Axis along which to perform the median. Default is to find the median of
        the flattened array.
    """
    data = masked_array.filled(fill_value=np.NaN)
    med = bottleneck.nanmedian(data, axis=axis)
    # construct a masked array result, setting the mask from any NaN entries
    return np.ma.array(med, mask=np.isnan(med))
