import os
import logging

import gp3.wrappers.fitsh as fitsh

log = logging.getLogger(__name__)

def produce_master_flat(calib_func, input_files, dest_file, dest_scatter_file=None, master_bias=None, master_dark=None, mask_objects=False, workdir=None):
    # TODO: handle non-unique filenames properly
    # TODO: implement object masking
    # TODO: having to supply all destination file paths seems rather meh..?
    
    log.info('Start master flat production')
    
    proc_files = map(lambda f: os.path.join(workdir, os.path.basename(f)), input_files)
    # calibrate all frames
    calib_func(proc_files, input_files, master_bias=master_bias,
               master_dark=master_dark, post_scale=30000)
    
    fitsh.ficombine(dest_file, proc_files, mode='rejmed')

    if dest_scatter_file is not None:
        fitsh.ficombine(dest_scatter_file, proc_files, mode='scatter')
