import numpy as np
import george
from george import kernels
import scipy.optimize as optimize



def plot_fancy_lc(df, ax, include_bands=[], plot_residual=False, distance_modulus=None, explosion_mjd=None, show_data=True, show_gp=True, spec_times=[], combine_instruments=False):
    if combine_instruments:
        selector = df.groupby(level=2, sort=True)
    else:
        selector = df.groupby(level=(0,1,2), sort=True)
    for group_key, df_sel in selector:
#    for band in df.index.levels[df.index.names.index('filter')]:
        if combine_instruments:
            telescope, instrument = None, None
            band = group_key
        else:
            (telescope, instrument, band) = group_key
        if band not in include_bands:
            continue
        #df_sel = df.xs(band, level='filter')
        mjd = df_sel.index.get_level_values('MJD')
        mag = df_sel['MAG']
        mag_err = df_sel['MAG_ERR']
        #
        
        #kernel = 10 * kernels.ExpSquaredKernel(80)
        kernel = 3 * kernels.ExpSquaredKernel(60)
        gp = george.GP(kernel, mean=19.27, fit_mean=True)
        gp.compute(mjd, mag_err)
        
        # Define the objective function (negative log-likelihood in this case).
        def nll(p):
            gp.set_parameter_vector(p)
            ll = gp.log_likelihood(mag, quiet=True)
            return -ll if np.isfinite(ll) else 1e25

        # And the gradient of the objective function.
        def grad_nll(p):
            gp.set_parameter_vector(p)
            return -gp.grad_log_likelihood(mag, quiet=True)
        
        p0 = gp.get_parameter_vector()
        results = optimize.minimize(nll, p0, jac=grad_nll, method="L-BFGS-B")

        # Update the kernel and print the final log-likelihood.
        gp.set_parameter_vector(results.x)
        print(gp.get_parameter_vector())
        
        
        peak_epoch = None
        if not plot_residual:
            mjd_pred = np.linspace(mjd.min(), mjd.max(), 500)
            pred, pred_var = gp.predict(mag, mjd_pred, return_var=True)
            peak_epoch = mjd_pred[np.argmin(pred)]
            if not combine_instruments:
                label = '%s-%s' % (instrument, band)
            else:
                label = band
            if show_gp:
                ax.fill_between(mjd_pred, pred - 3*np.sqrt(pred_var), pred + 3*np.sqrt(pred_var), alpha=0.4, label=label)
                #ax.plot(mjd_pred, pred, lw=1.5, label='%s-%s' % (instrument, band))
                if show_data:
                    ax.errorbar(mjd, mag, yerr=mag_err, alpha=0.2, color='grey', fmt='.')
            else:
                ax.errorbar(mjd, mag, yerr=mag_err, fmt='.', label=label)
        else:
            if not combine_instruments:
                label = '%s-%s' % (instrument, band)
            else:
                label = band
            mjd_pred = np.array(mjd, copy=True)
            pred, pred_var = gp.predict(mag, mjd_pred, return_var=True)
            ax.errorbar(mjd, mag-pred, yerr=mag_err, fmt='.')
            ax.fill_between(mjd, 3*np.sqrt(pred_var), -3*np.sqrt(pred_var), alpha=0.4, label=label)

    if distance_modulus is not None and not plot_residual:
        def ap2abs(x):
            return x - distance_modulus

        def abs2ap(x):
            return x + distance_modulus
        
        sec_ax = ax.secondary_yaxis('right', functions=(ap2abs, abs2ap))
        sec_ax.set_ylabel('Absolute Magnitude')

    if peak_epoch is not None:
        def abs2rel(x):
            return x - peak_epoch

        def rel2abs(x):
            return x + peak_epoch
        
        sec_ax = ax.secondary_xaxis('top', functions=(abs2rel, rel2abs))
        sec_ax.set_xlabel('Phase [d]')

    for stime in spec_times:
        ax.axvline(x=stime, alpha=0.4, c='red')
            
    ax.set_ylabel('Apparent Magnitude')
    ax.set_xlabel('MJD [d]')
    ax.legend()
    ax.invert_yaxis()
