from bokeh.models import TickFormatter
from bokeh.core.properties import Dict, Int, String

JS_CODE = """

_ = require "underscore"
Model = require "model"
p = require "core/properties"
TickFormatter = require "models/formatters/tick_formatter"

class FixedTickFormatter extends TickFormatter.Model
  type: 'FixedTickFormatter'
  
  @define {
    labels: [ p.Any ]
  }

  doFormat: (ticks) ->
    labels = @get("labels")
    return (labels[tick] ? "{ #tick }" for tick in ticks)

module.exports =
  Model: FixedTickFormatter

"""

class FixedTickFormatter(TickFormatter):

    labels = Dict(Int, String, help="""
    A mapping of integer ticks values to their labels.
    """)

    __implementation__ = JS_CODE

# stolen from http://stackoverflow.com/questions/29166353/how-do-you-add-error-bars-to-bokeh-plots-in-python
def errorbar(fig, x, y, xerr=None, yerr=None, color='red', 
             point_kwargs={}, error_kwargs={}):

  fig.circle(x, y, color=color, **point_kwargs)

  if xerr:
      x_err_x = []
      x_err_y = []
      for px, py, err in zip(x, y, xerr):
          x_err_x.append((px - err, px + err))
          x_err_y.append((py, py))
      fig.multi_line(x_err_x, x_err_y, color=color, **error_kwargs)

  if yerr is not None:
      y_err_x = []
      y_err_y = []
      for px, py, err in zip(x, y, yerr):
          y_err_x.append((px, px))
          y_err_y.append((py - err, py + err))
      fig.multi_line(y_err_x, y_err_y, color=color, **error_kwargs)
