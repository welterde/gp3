import pandas as pd
import numpy as np




# Example csv:
# From http://skipper.caltech.edu:8080/cgi-bin/growth/print_lc.cgi?name=ZTF19abqwtfu on 2019-10-09
# date,jdobs,filter,absmag,magpsf,sigmamagpsf,limmag,instrument,programid,reducedby,refsys,issub,isdiffpos
# "2019 Jul 22",2458686.9448,"i", 99.0, 99.0,99.0, 19.24,"P48+ZTF","2","None","None",True,True

GROWTH_TEL_MAP = {
    'P48+ZTF': 'P48',
    'P60+SEDM': 'P60',
    # ugly hack because of csv shit
    '"P48+ZTF"': 'P48',
    '"P60+SEDM"': 'P60'
}

GROWTH_INS_MAP = {
    'P48+ZTF': 'ZTF',
    'P60+SEDM': 'SEDM',
    # ugly hack because of csv shit
    '"P48+ZTF"': 'ZTF',
    '"P60+SEDM"': 'SEDM'
}

def ingest_growth_marshall_data(fname):
    df = pd.read_csv(fname)
    df['mjd'] = df['jdobs'] - 2400000.5

    df['telescope'] = df['instrument'].map(GROWTH_TEL_MAP)
    df['instrument'] = df['instrument'].map(GROWTH_INS_MAP)
    # image subtraction + PSF
    df['phot_type'] = 'IS+PSF'

    # mask upper limits
    df.at[df['magpsf'] > 30.0, 'magpsf'] = np.nan
    df.at[df['magpsf'] > 30.0, 'sigmamagpsf'] = np.nan
    df.at[df['absmag'] > 30.0, 'absmag'] = np.nan
    df.at[df['limmag'] > 30.0, 'limmag'] = np.nan

    df.rename(columns={
        'magpsf': 'mag',
        'sigmamagpsf': 'magerr',
        'absmag': 'mag_abs',
        'limmag': 'mag_limiting'
    }, inplace=True)

    # TODO: delete the extra columns? (maybe including abs mag)
    
    df2 = df.set_index(['telescope', 'instrument', 'filter', 'mjd', 'phot_type'])

    return df2



FORCE_STR_COLS = [
    'filter',
    'scifilename',
    'diffilename',
    'reffilename',
    'obsdate',
    'refcreated'
]

FORCE_INT_COLS = [
    'index',
    'field',
    'ccdid',
    'pid',
    'infobitssci',
    'ncalmatches',
    'programid',
    'procstatus'
]
    
def parse_f_line(line, hdr):
    parts = line.strip().split(' ')
    x = {}
    for k,v in zip(hdr, parts):
        if k in FORCE_STR_COLS:
            x[k] = v
        elif k in FORCE_INT_COLS:
            x[k] = int(v)
        else:
            x[k] = float(v)
    return x

def ingest_force_photometry_data(fname):
    f = open(fname, 'r')

    # line describing the columns
    header = None
    data = []
    for line in f:
        # ignore commented lines
        if line.strip()[0] == '#':
            continue
        # ignore empty lines
        if line.strip() == '':
            continue

        # read the header
        if header is None:
            header = list(map(str.strip, line.strip().split(',')))
            continue

        # parse line
        data.append(parse_f_line(line, hdr=header))

    f.close()

    df = pd.DataFrame(data)
    df2 = df.set_index('jd')

    # TODO: reformat to some common format
    
    return df2
