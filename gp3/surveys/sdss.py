from astropy.table import Table
import astropy.units as u
from astropy.coordinates import SkyCoord


# TODO: automatically download the datasweep index file
# https://data.sdss.org/sas/dr14/eboss/sweeps/dr13_final/datasweep-index-star.fits


def match_sdss_fields(coords, sdss_sweep_idx_fname):
    """
    Match fields that should lie within the SDSS footprint
    """
    idx_t = Table.read(sdss_sweep_idx_fname)
    idx_c = SkyCoord(idx_t['RA']*u.deg, idx_t['DEC']*u.deg)
    
    # perform the crossmatch
    sdss_idx, dist, _dist3 = coords.match_to_catalog_sky(idx_c)
    
    # the datasweep fields are 0.36deg in diameter..
    # TODO: double-check that..
    return dist.deg < 0.36
