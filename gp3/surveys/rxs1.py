import sys
import os
import numpy as np
from astropy.table import Table, Column
from astropy.coordinates import SkyCoord
import astropy.units as u




def load_target_cat(cat_file, invert_agn_cut=False, best_only=True):
    cat = Table.read(cat_file)
    
    # select the best candidate
    if best_only:
        idx = cat['match_flag'] == 1
    else:
        idx = np.ones(len(cat), dtype=np.bool)
    
    # cut away stars based on xray-IR cutoff
    if not invert_agn_cut:
        idx = np.logical_and(idx, cat['ALLW_w1mpro'] + 1.625*np.log10(0.585*cat['2RXS_SRC_FLUX']) + 8.8 > 0)
    else:
        idx = np.logical_and(idx, np.logical_not(cat['ALLW_w1mpro'] + 1.625*np.log10(0.585*cat['2RXS_SRC_FLUX']) + 8.8 > 0))
    
    # cut to declination strip
    idx = np.logical_and(idx, np.logical_and(cat['2RXS_DEC'] <= -35, cat['2RXS_DEC'] >= -40))
    
    # cut away objects with known proper motion
    idx = np.logical_and(idx, np.logical_or(np.isnan(cat['pmra']), np.logical_and(np.abs(cat['pmra']) < 5*cat['pmra_error'], np.abs(cat['pmdec']) < 5*cat['pmdec_error'])))
    
    return cat[idx]
