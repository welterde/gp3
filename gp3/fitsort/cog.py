import glob
import itertools
import os
import logging
import pyfits
import collections
import re
import fnmatch
import shutil

import astropy.io.fits as fits
import astropy.time as time

import gp3.fitsort.control as control



log = logging.getLogger('gp3.fitsort.cog')

INTERESTING_FILES = ['.fit', '.fits', '.fit.gz', '.fits.gz']

CALIB_IMAGETYPS = ['dark', 'zero', 'flat']

KNOWN_INSTRUMENTS = ['gowi', 'dados', 'baches']

INVALID_OBJECTS = ['object'] + map(lambda s: s+'_', KNOWN_INSTRUMENTS) + KNOWN_INSTRUMENTS


ImgInfo = collections.namedtuple('ImgInfo', 'object instrument imagetyp')


def sort(indir, destdir, process_hook, controlfile=None,
         consider_exts=INTERESTING_FILES, prune_indir=True):
    """
    Target sorting:
    $destdir/$day/
    $destdir/$day/$object/$instrument         where OBJECT set
    $destdir/$day/calib/$instrument           for calib frames that are not target related
    original frames are placed in orig dir below that
    """
    
    if controlfile:
        crules = control.parse(controlfile)
    else:
        crules = None
    
    mfiles = []
    # collect all interesting files..
    # TODO: warn about all those not matched
    for dirpath, dirs, files in os.walk(indir):
        for ext in consider_exts:
            mfiles.extend(map(lambda f: os.path.join(dirpath, f), fnmatch.filter(files, '*' + ext)))
    
    moved_files = 0
    skip_ignore = 0
    skip_fail = 0
    
    for f in mfiles:
        log.info(" ---> Processing %s", f)
        hdul = fits.open(f, mode='readonly', memmap=True)
        
        if crules:
            abort = control.apply(crules, os.path.relpath(f, start=indir), hdul)
            if abort:
                log.info("Skipping (ignored per control file).")
                skip_ignore += 1
                continue
        
        obi = extract_imginfo(os.path.relpath(f, start=indir), hdul[0].header)
        
        if obi == None:
            log.warn("Missing Image information.. skipping.")
            skip_fail += 1
            continue
        else:
            log.info("Extracted: %s", repr(obi))

        if 'DATE-OBS' not in hdul[0].header:
            log.warn("Missing DATE-OBS.. skipping.")
            skip_fail += 1
            continue
        
        # next day doesn't start at 0 UTC, but at 12 UTC
        date = time.Time(hdul[0].header['DATE-OBS'])
        if date.datetime.hour < 12:
            date = date - time.TimeDelta(1, format='jd')
        day = date.datetime.strftime('%Y%m%d')
        
        dest = os.path.join(destdir, day, obi.object, obi.instrument)
        orig_dest = os.path.join(dest, 'orig', os.path.relpath(os.path.dirname(f), start=indir))
        if not os.path.isdir(orig_dest):
            log.debug("Creating %s", orig_dest)
            os.makedirs(orig_dest)
        
        destfn = os.path.join(dest, get_fname(obi, hdul[0].header))
        origfn = os.path.join(orig_dest, os.path.basename(f))
        if os.path.exists(origfn):
            # TODO: compare checksums
            log.warn("Duplicate file %s!", f)
            continue
        
        # process file
        log.debug("Calling process_hook")
        if process_hook(f, destfn, obi, hdul):
            # move original file to orig destination
            shutil.move(f, origfn)
            moved_files += 1
        else:
            log.warn("Process hook failed.. not moving original file")
        
    
    # delete all empty dirs in indir?
    if prune_indir:
        log.info("Pruning input directory of empty dirs...")
        for root, dirs, files in os.walk(indir, topdown=False):
            if len(dirs) == 0 and len(files) == 0:
                os.rmdir(root)
    
    log.info("================================================")
    log.info("RUN SUMMARY")
    log.info("================================================")
    log.info(" Found input files: %-4d" % len(mfiles))
    log.info(" Moved files (to orig folders): %-4d" % moved_files)
    log.info(" Ignore files: %-4d" % skip_ignore)
    log.info(" Skip due to missing information: %-4d" % skip_fail)

def extract_imginfo(fname, hdr):
    if not ('INSTRUME' in hdr and hdr['INSTRUME'].lower() in KNOWN_INSTRUMENTS):
        log.info("Missing or invalid instrument information")
        return None
    instrument = hdr['INSTRUME'].lower()
    
    if 'OBJECT' in hdr and  hdr['OBJECT'].lower() not in INVALID_OBJECTS:
        obj = hdr['OBJECT']
        log.debug('Object=%s based on OBJECT header', object)
    else:
        if hdr['imagetyp'].lower() in CALIB_IMAGETYPS:
            obj = 'calib'
        else:
            log.debug("Missing object information")
            return None
    
    if hdr['IMAGETYP'].lower().startswith('dark'):
        imagetyp = 'dark'
    elif hdr['IMAGETYP'].lower().startswith('light') or hdr['IMAGETYP'].lower() == 'object':
        imagetyp = 'sci'
    elif hdr['IMAGETYP'].lower().startswith('zero'):
        imagetyp = 'zero'
    elif hdr['IMAGETYP'].lower().startswith('flat'):
        imagetyp = 'flat'
    else:
        log.info("Unknown imagetype: %s", hdr['IMAGETYP'])
        return None
    
    return ImgInfo(instrument=instrument, object=obj, imagetyp=imagetyp)

GOWI_FILTER_MAP = {
    'blue': 'B',
    'green': 'G',
    'red': 'R',
    'luminance': 'L',
    'halpha': 'H',
    'b': 'B',
    'g': 'G',
    'r': 'R',
    'l': 'L',
    'h': 'h',
}

def get_fname(obi, hdr):
    """
    Return file names of the form: $unixtime-($filter|flat-$filter|dark-$exptime|bias).fits
    """
    utime = int(time.Time(hdr['DATE-OBS']).unix)
    
    if obi.imagetyp == 'flat':
        if obi.instrument == 'gowi':
            flat_type = 'twilightflat'
            if 'OBJECT' in hdr:
                flat_type = hdr['OBJECT']
            return '%d-flat-%s-%s.fits' % (utime, GOWI_FILTER_MAP[hdr['FILTER'].lower()], flat_type)
        else:
            return '%d-flat.fits' % utime
    elif obi.imagetyp == 'zero':
        return '%d-bias.fits' % utime
    elif obi.imagetyp == 'dark':
        return '%d-dark-%d.fits' % (utime, hdr['EXPTIME'])
    else:
        if 'FILTER' in hdr:
            return '%d-%s.fits' % (utime, GOWI_FILTER_MAP[hdr['FILTER'].lower()])
        else:
            return '%d.fits' % utime



