import itertools
import collections
import shlex
import logging
import fnmatch


log = logging.getLogger('gp3.fitsort.control')

ControlIgnore = collections.namedtuple('ControlIgnore', 'glob')
ControlOverrideHeader = collections.namedtuple('ControlOverrideHeader', 'glob key value')
ControlReplaceHeader = collections.namedtuple('ControlOverrideHeader', 'glob key value_old value_new')

def parse(fname):
    """
    # Example:
    # ignore some directory (relative to incoming/)
    ignore welterde/big_test_data/*
    # Syntax: ignore <pattern>
    
    # override some headers for certain files
    override_hdr welterde/calib_baches/* instrument=baches object=calib observer=welterde
    override_hdr welterde/calib_baches/*
    # Syntax: override_hdr <pattern> Key=Value [K2=V2 [K3=V3 ...]]
    
    #####################################################################
    # not implemented land begins here (ie. ignore what comes after this)
    # replace some headers for certain files
    replace_hdr * object="V404 Cyg"="V404_Cyg"
    """
    control=[]
    with open(fname, 'r') as f:
        for line in itertools.imap(str.strip, f.readlines()):
            # comment
            if line.startswith('#') or line == '':
                continue
            log.debug("Parsing line: %s", line)
            
            cmd, args = line.split(' ', 1)
            args = args.split('#', 1)[0].strip()
            tokens = shlex.split(args, posix=True)
            
            if cmd == 'ignore':
                assert len(tokens) == 1
                control.append(ControlIgnore(glob=tokens[0]))
                
            elif cmd == 'override_hdr':
                assert len(tokens) > 1
                globp = tokens[0]
                control.extend(
                    map(
                        lambda (k,v): ControlOverrideHeader(
                            glob=globp,
                            key=k, value=v),
                        map(lambda s: s.split("="),
                            tokens[1:])
                    ))
                
            elif cmd == 'replace_hdr':
                assert len(tokens) > 1
                globp = tokens[0]
                control.extend(map(lambda (k,v1,v2): ControlReplaceHeader(
                    glob=globp,
                    key=k,
                    value_old=v1,
                    value_new=v2),
                                   map(lambda s: s.split("="), tokens[1:])))
            else:
                log.warn('Unknown command: %s', cmd)
    return control
                
                    
def apply(rules, fname, hdul):
    hdr = hdul[0].header
    
    for rule in rules:
        if not fnmatch.fnmatch(fname, rule.glob):
            continue
        if rule.__class__ == ControlIgnore:
            return True
        elif rule.__class__ == ControlOverrideHeader:
            log.debug("Setting %s to %s", rule.key, rule.value)
            hdr[rule.key] = rule.value
        elif rule.__class__ == ControlReplaceHeader:
            if rule.key in hdr and hdr[rule.key] == rule.value_old:
                log.debug("Replacing %s (%s) with %s", hdr[rule.key], rule.key, rule.value_new)
                hdr[rule.key] = rule.value_new
    return False
