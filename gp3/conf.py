import astropy.config as config


class PipelineConfig(config.ConfigNamespace):
    tmpdir = config.ConfigItem(defaultvalue='~/.gp3/tmp', description='Temporary storage for large computations')

pipeline = PipelineConfig()

