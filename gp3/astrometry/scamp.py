import logging
import tempfile
import shutil
import os
import re
import multiprocessing
import functools

import astropy.table as table
import astropy.io.fits as fits
import numpy as np

import gp3.conf as conf
import gp3.wrappers.scamp as scamp
import gp3.wrappers.sextractor as sextractor

log = logging.getLogger('gp3.astrometry.scamp')

SCAMP_PARAMS = [
    'XWIN_IMAGE',
    'YWIN_IMAGE',
    'ERRAWIN_IMAGE',
    'ERRBWIN_IMAGE',
    'FLUX_AUTO',
    'FLUXERR_AUTO',
    'FLAGS'
]

SCAMPREF_PARAMS = [
    'XWIN_IMAGE',
    'YWIN_IMAGE',
    'X_WORLD',
    'Y_WORLD',
    'ERRA_WORLD',
    'ERRB_WORLD',
    'ERRTHETA_WORLD',
    'ERRAWIN_IMAGE',
    'ERRBWIN_IMAGE',
    'MAG_AUTO',
    'MAGERR_AUTO',
    'FLUX_AUTO',
    'FLUXERR_AUTO',
    'FLAGS'
]



# TODO:
# - mosaic support

FITS_RE = re.compile(r'\.fit(s?)$')


def extract_sextractor(img_cat, workdir, params):
    src_img, dst_cat = img_cat
    abs_img = os.path.abspath(src_img)
    abs_cat = os.path.abspath(os.path.join(workdir, dst_cat))
    log.debug("Running sextractor %s -> %s", src_img, dst_cat)
    sextractor.sextractor(abs_img, template='default.sex', parse_cat=False,
                          params=SCAMP_PARAMS,
                          ini={'CATALOG_NAME': abs_cat, 
                               'CATALOG_TYPE': 'FITS_LDAC',
                               'VERBOSE_TYPE': 'QUIET',
                               'HEADER_SUFFIX': '.ignore_headers'})
    # TODO: extract return code?
    return 0

def astrometry(images, destdir=None, mosaic=False, just_patch_broken=True,
               max_offset=4.0, contrast_threshold=9.0, grond_arcfile_fix=False,
               contrast_fail_callback=None, copy_bad=False, ref_image=None, quiet=False,
               tmpdir=conf.pipeline.tmpdir):
    """
    just_patch_broken: If true only patch up images where astrometry has failed
    If false use best solution and derive all other images from that
    """
    ######
    ### 1. create temp dir
    reddir = tempfile.mkdtemp(prefix='scamp_astro', dir=os.path.expanduser(tmpdir))
    log.debug('Created tempdir %s', reddir)

    if ref_image is not None:
        log.debug('Running sextractor to generate ref cat from %s', ref_image)
        sextractor.sextractor(ref_image, template='default.sex', parse_cat=False,
                              workdir=reddir, preserve_workdir=True,
                              params=SCAMPREF_PARAMS,
                              catalog_name='ref.cat',
                              ini={'CATALOG_TYPE': 'FITS_LDAC'})
    ######
    ### 2. generate the catalogs for scamp
    # check if the files are unique
    if len(images) != len(np.unique(map(lambda a: os.path.basename(a), images))):
        cats = ['input-%03d.cat' % i for i in range(len(images))]
    else:
        cats = [FITS_RE.split(os.path.basename(img_name))[0] + '.cat' for img_name in images]
    
    img2cat = list(zip(images, cats))
    
    # run multiple sextractor instances
    # TODO: nice progress bar or something?
    log.info('Running sextractor pool with 8 workers')
    pool = multiprocessing.Pool(8)
    sex_func = functools.partial(extract_sextractor, workdir=reddir, params=SCAMP_PARAMS)
    retcodes = pool.map(sex_func, img2cat)
    pool.terminate()
    
    # 2.1. generate arcfile -> {$band -> ($img_idx, $img)} mapping if grond_arcfile_fix
    arcfile_mapping = {}
    arcfile_reverse = []
    image_band = []
    if grond_arcfile_fix:
        for idx, img in enumerate(images):
            hdr = fits.getheader(img)
            arcfile = hdr['ARCFILE']
            band = hdr['FILTER']
            if arcfile not in arcfile_mapping:
                arcfile_mapping[arcfile] = {}
            assert band not in arcfile_mapping[arcfile]
            arcfile_mapping[arcfile][band] = (idx, img)
            arcfile_reverse.append(arcfile)
            image_band.append(band)
    else:
        for i in range(len(images)):
            arcfile_reverse.append(None)
    
    grond_arcfile_fix_possible = [0]

    def cb(rdir, c2c):
        log.debug('Running scamp callback')
        # TODO: move this maybe into the wrapper?
        # load scamp run summary statistics..
        input_stats = table.Table.read(os.path.join(reddir, 'scamp.xml'), table_id='Fields')
        fgroups = table.Table.read(os.path.join(reddir, 'scamp.xml'), table_id='FGroups')
        
        success = (input_stats['XY_Contrast'] > contrast_threshold) & (input_stats['AS_Contrast'] > contrast_threshold)
        failure = np.logical_not(success)

        for fgroup in fgroups:
            succ_fg = (input_stats['Group'] == fgroup['Index']) & success
            fail_fg = np.logical_not(succ_fg)

            # make sure there is at least one solution in this field group
            #if np.count_nonzero(succ_fg) < 1:
                # TODO: add knob to keep going if #fgroup > 1
            #    raise ValueError('Astrometry failed for all in Field group')

            # TODO: select best sigma against reference catalog
            
            topatch = fail_fg
        
        possible_reruns = 0
        
        img_ok = np.zeros(len(images), dtype=np.bool)
        
        if contrast_fail_callback is not None:
            for idx, ((img, cat), scamp_stat) in enumerate(zip(img2cat, input_stats)):
                if scamp_stat['XY_Contrast'] < contrast_threshold:
                    contrast_fail_callback(img, final_pass=(not grond_arcfile_fix))
                else:
                    img_ok[idx] = True

        log.debug('%d images are ok', np.count_nonzero(img_ok))
                    
        if np.count_nonzero(~img_ok) > 0 and grond_arcfile_fix:
            log.debug('Running grond-arcfile-fix..')
            for idx in range(len(images)):
                if not img_ok[idx] and arcfile_reverse[idx] is not None:
                    log.debug('Searching for counterpart for %s (idx %d)', images[idx], idx)
                    other_bands = arcfile_mapping[arcfile_reverse[idx]].copy()
                    del other_bands[image_band[idx]]
                    use_band = None
                    max_contrast = 0.0
                    for band in other_bands.keys():
                        c = input_stats['XY_Contrast'][other_bands[band][0]]
                        if c < 3.0:
                            log.debug('Rejecting band %s due to contrast (%f < 3.0)', band, c)
                            continue
                        if c > max_contrast:
                            use_band = band
                    if use_band is not None:
                        log.debug('Using band %s -> copying over header file', use_band)
                        src_idx = other_bands[use_band][0]
                        src_head_fname = os.path.join(rdir, cats[src_idx][:-4]+'.head')
                        ahead_fname = os.path.join(rdir, cats[idx][:-4]+'.ahead')
                        shutil.copy(src_head_fname, ahead_fname)
                        print(repr(grond_arcfile_fix_possible))
                        grond_arcfile_fix_possible[0] += 1
            if grond_arcfile_fix_possible[0] > 0:
                return
                        
        
        # for all success
        for (img, cat), scamp_stat in zip(img2cat, input_stats):
            log.debug('processing entry for file %s', img)
            if (not copy_bad) and scamp_stat['XY_Contrast'] < contrast_threshold:
                log.debug('skipped %s because of low contrast' % img)
                continue
            hdr = os.path.join(rdir, cat[:-4]+'.head')
            idir = os.path.dirname(img)
            if destdir:
                idir = destdir
            dst = os.path.join(idir, os.path.basename(img)[:-5]+'.head')
            log.debug('moving %s to %s', hdr, dst)
            shutil.move(hdr, dst)

    verbose_mode = 'NORMAL'
    if quiet:
        verbose_mode = 'QUIET'
    
    # 3. run scamp on those catalogs
    if ref_image is None:
        scamp.scamp(cats, cb, copy_cats=False, workdir=reddir,
                    ini={'ASTREF_CATALOG': '2MASS',
                         'ASTREF_BAND': 'J',
                         'VERBOSE_TYPE': verbose_mode,
                         'POSITION_MAXERR': str(max_offset)})
    else:
        scamp.scamp(cats, cb, copy_cats=False, workdir=reddir,
                    ini={'ASTREF_CATALOG': 'FILE',
                         'ASTREFCAT_NAME': os.path.join(reddir, 'ref.cat'),
                         'ASTREFMAG_KEY': 'MAG_AUTO',
                         'VERBOSE_TYPE': verbose_mode,
                         'ASTREFMAGERR_KEY': 'MAGERR_AUTO'})
    
    if grond_arcfile_fix_possible[0] > 0:
        # otherwise we might loop^^
        grond_arcfile_fix = False
        scamp.scamp(cats, cb, copy_cats=False, workdir=reddir,
                    ini={'ASTREF_CATALOG': '2MASS',
                         'ASTREF_BAND': 'J',
                         'VERBOSE_TYPE': verbose_mode,
                         'POSITION_MAXERR': str(max_offset)})
