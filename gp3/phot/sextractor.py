
import logging
import math
import tempfile
import os

import astropy.io.fits as fits

import gp3.conf as conf
import gp3.wrappers.sextractor as sextractor
import gp3.wrappers.psfex as psfex

log = logging.getLogger('gp3.phot.sex')


PSFEX_IN_PARAMS = [
    'FLUX_APER',
    'FLUXERR_APER',
    'FLUX_MAX',
    'X_IMAGE',
    'Y_IMAGE',
    'FLAGS',
    'ELONGATION',
    'VIGNET(25,25)',
    'FLUX_RADIUS(1)',
    'SNR_WIN'
]

PHOT_PARAMS = [
    'ALPHAPSF_J2000',
    'DELTAPSF_J2000',
    'MAG_AUTO',
    'MAGERR_AUTO',
    'MAG_PSF',
    'MAGERR_PSF',
    'CHI2_PSF'
]

# TODO: move this information to a per-instrument file
GROND_ZP = {
    'g': 24.99,
    'r': 25.20,
    'i': 24.20,
    'z': 24.00,
    'J': 23.973,
    'H': 23.515,
    'K': 22.703
}

# TODO: move this to a per-site file
LASILLA_EXTINCTION_COEFFS = {
    'u': 0.45, 'U':0.45, 'B':0.22, 'V':0.12, 'R':0.09, 'I':0.05,
    'g': 0.16, 'r':0.09, 'i':0.04, 'z':0.04,
    'Y': 0.08, 'J':0.12, 'H':0.06, 'K':0.07, 'NB1190':0.12
}
    

# TODO: Add support for detection image
# Maybe have usage like gp3-phot -m sextractor --detect img1.fits -o foo.hdf5:/asdf img2.fits?

# TODO: calibration methods?
# maybe --phot-calib SDSS etc. ?

# TODO: maybe make a workdir decorator..
# or better mechanism in general..?
# @workdir('sex_phot') will either use user-supplied or create one..
# and also takes care of delete at or not
# actual function just has workdir param; outside it has that + tmpdir + preserve_workdir

# TODO: mechanism for all those useful reference images, etc.

# TODO: make PSF extraction and photometry more configurable

# TODO: set correct saturation value in sextractor

# TODO: detect_image in first sextractor run?

def phot(images, detect_image=None, band=None, tmpdir=conf.pipeline.tmpdir, astrometric_image_dest=None, extra_options=None):
    """
    Perform PSF photometry using sextractor and psfex

    Three steps:
    1. Generate catalog for use with psfex
    2. Build PSF model using psfex
    3. Run sextractor again using PSF model
    """

    workdir = tempfile.mkdtemp(prefix='sex_phot', dir=os.path.expanduser(tmpdir))
    log.debug('Created tempdir %s', workdir)

    cats = ['input-%03d.cat' % i for i in range(len(images))]
    psfs = ['input-%03d.psf' % i for i in range(len(images))]

    # generate catalogs for psfex..
    for image, cat_dest in zip(images, cats):
        # TODO: support MEF files
        image_hdu = fits.open(image)[0]
        
        # extract some headers
        gain = image_hdu.header['GAIN']
        exptime = image_hdu.header['EXPTIME']
        if image_hdu.header['INSTRUME'] == 'GROND':
            base_zp = GROND_ZP[image_hdu.header['FILTER']]
            zp = base_zp + 2.5 * math.log10(exptime)
        else:
            # TODO: change to non-fatal error and keep going?
            raise NotImplemented('Instrument %s not supported', image_hdu['INSTRUME'])
        
        # TODO: support more instruments
    
        ## 1. Catalog for constructing the PSF
        # TODO: Set SATUR_LEVEL, GAIN, MAG_ZEROPOINT, etc.
        # TODO: maybe configure special convolution mask
        sextractor.sextractor(image,
                            params=PSFEX_IN_PARAMS,
                            parse_cat=False,
                            workdir=workdir,
                            preserve_workdir=True,
                            catalog_name=cat_dest,
                            ini={
                                'CATALOG_TYPE': 'FITS_LDAC',
                                'GAIN': gain,
                                'MAG_ZEROPOINT': zp
                            })

    ## 2. Build PSF model using psfex
    psfex.psfex(cats,
                workdir=workdir,
                preserve_workdir=True,
                copy_cats=False,
                ini={
                    'BASIS_TYPE': 'PIXEL_AUTO'
                })

    ## 3. Run final photometry using that PSF model
    cat_out = []
    for image, cat_dest, psf_fname in zip(images, cats, psfs):
        # TODO: support MEF files
        # TODO: refactor this... don't read those headers twice..?
        image_hdu = fits.open(image)[0]
        
        # extract some headers
        gain = image_hdu.header['GAIN']
        exptime = image_hdu.header['EXPTIME']
        if image_hdu.header['INSTRUME'] == 'GROND':
            base_zp = GROND_ZP[image_hdu.header['FILTER']]
            zp = base_zp + 2.5 * math.log10(exptime)
        else:
            # TODO: change to non-fatal error and keep going?
            raise NotImplemented('Instrument %s not supported', image_hdu['INSTRUME'])
    
        cat = sextractor.sextractor(image,
                                    detect_image=detect_image,
                                    params=PHOT_PARAMS,
                                    parse_cat=True,
                                    workdir=workdir,
                                    preserve_workdir=True,
                                    ini={
                                        'PSF_NAME': psf_fname,
                                        'GAIN': gain,
                                        'MAG_ZEROPOINT': zp
                                    })
        # the coordinate columns should be named like that
        cat.rename_column('ALPHAPSF_J2000', 'RAJ2000')
        cat.rename_column('DELTAPSF_J2000', 'DEJ2000')
        cat_out.append(cat)
    return cat_out, None
