import astropy.io.fits as fits
import shutil

FITS_BLACKLIST = [
    'SIMPLE',
    'BITPIX',
    'NAXIS',
    'NAXIS1',
    'NAXIS2',
    'EXTEND'
]


def write_header(header, outfile, format):
    if format == 'fits':
        hdul = fits.open(outfile, mode='update')
        hdul[0].header.update(filter(lambda k: k not in FITS_BLACKLIST, header.keys()))
        hdul.flush()
        hdul.close()


def perform_photometry(outfile, func, overwrite=True, sort=True):
    cat, extra = func()
    if sort:
        cat.sort(['RAJ2000', 'DEJ2000'])
    if ':' in outfile:
        outfile, format = outfile.split(':', 1)
    else:
        format = None
    if (format is not None) and (format.startswith('hdf5')):
        if 'h5_file' in extra:
            shutil.copy(extra['h5_file'], outfile)
        else:
            if ':' in format:
                format, path = format.split(':', 1)
            else:
                path = 'default'
            cat.write(outfile, path=path, overwrite=overwrite, append=True)
    elif format == 'result':
        if 'result_raw' in extra:
            # TODO: overwrite handling
            with open(outfile, 'w') as f:
                f.write(extra['result_raw'])
        else:
            raise ValueError('Result format not supported by photometry module')
    else:
        cat.write(outfile, overwrite=overwrite, format=format)
    if hasattr(cat, 'header'):
        write_header(cat.header, outfile, format)

def perform_multi(func, inputs, out_suffix, out_type, sort=True, overwrite=True):
    a, b = out_suffix.split(':', 1)
    outputs = map(lambda s: s.replace(a, b), inputs)
    cats, extra = func(inputs)
    for cat, out_name in zip(cats, outputs):
        if sort:
            cat.sort(['RAJ2000', 'DEJ2000'])
        if out_type.startswith('hdf5'):
            if ':' in out_type:
                type, path = out_type.split(':', 1)
            else:
                path = 'default'
            cat.write(out_name, path=path, append=True, overwrite=overwrite)
        else:
            cat.write(out_name, format=out_type, overwrite=overwrite)
