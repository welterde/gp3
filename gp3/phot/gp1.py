import logging
import glob
import os
import shutil

import astropy.table as table
import astropy.io.fits as fits

import gp3.wrappers.gp1 as gp1

log = logging.getLogger('gp3.phot.gp1')


# XXX: copied from gp3-apply-wcs - move to some common location
BASIC_WCS_HEADERS = [
    'CTYPE1',
    'CTYPE2',
    'CUNIT1',
    'CUNIT2',
    'CRVAL1',
    'CRVAL2',
    'CRPIX1',
    'CRPIX2',
    'CD1_1',
    'CD1_2',
    'CD2_1',
    'CD2_2'
]

def parse_result(resultf):
    # FIXME: support solutions with failed astrometry?
    cat = table.Table.read(resultf, format='ascii')
    cat.columns[0].name = 'RAJ2000'
    cat.columns[1].name = 'DEJ2000'
    cat.columns[2].name = 'MAG_PSF'
    #cat.columns[2].unit = 'mag'
    cat.columns[3].name = 'MAGERR_PSF'
    cat.columns[4].name = 'MAG_APP'
    cat.columns[5].name = 'MAGERR_APP'
    cat.columns[6].name = 'MAG_KRON'
    cat.columns[7].name = 'MAGERR_KRON'
    cat.columns[8].name = 'OFFSET_MATCHED'
    cat.columns[9].name = 'OFFSET_MATCHED_ERR'
    cat.columns[10].name = 'MAG_CALIB'
    cat.columns[11].name = 'MAGERR_CALIB'
    cat.columns[12].name = 'ELONGATION'
    cat.columns[13].name = 'R_HALFLIGHT'
    cat.columns[14].name = 'CLASS_STAR'
    return cat


def phot(image, band=None, force_detect=None, extra_options=None, astrometric_image_dest=None):
    hdul = fits.open(image, memmap=True)
    if band == None:
        band = hdul[0].header['FILTER']

    extra_ret = {}
    
    def cb(reddir):
        results = glob.glob(os.path.join(reddir, "*.result"))
        assert len(results) == 1
        
        extra_ret['h5_file'] = glob.glob(os.path.join(reddir, "*.h5"))[0]
        
        if astrometric_image_dest is not None:
            img_file = glob.glob(os.path.join(reddir, "*_ana.fits"))
            if len(img_file) == 1:
                shutil.copy(img_file[0], astrometric_image_dest)

        # just read the result file into memory
        with open(results[0], 'r') as f:
            extra_ret['result_raw'] = f.read()
        return parse_result(results[0])
    
    def ch(src, dst):
        log.debug("Header found.. updating file")
        hdr = fits.Header.fromtextfile(image[:-5]+'.head')
        hdul[0].header.update(filter(lambda (k,_v): k in BASIC_WCS_HEADERS, hdr.iteritems()))
        hdul.writeto(dst)
    
    ini = {
            'task:trustwcs': '1'
        }
    
    if force_detect is not None:
        ini['task:objwcs'] = '%.5f %.5f' % (force_detect.ra.deg, force_detect.dec.deg)
        ini['task:mansource'] = '4'
        ini['task:COG'] = 'True'
    
    if not os.path.isfile(image[:-5]+'.head'):
        del ini['task:trustwcs']
        ch = None

    if extra_options is not None:
        for k,v in map(lambda a: a.split('='), extra_options):
            ini['task:%s' % k] = v
    
    cat = gp1.astrphot(image, band=band, callback=cb, ini_override=ini, copy_hook=ch)
    cat.header = hdul[0].header
    return cat, extra_ret
