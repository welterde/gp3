import pandas as pd
import numpy as np
import h5py



COMMON_COLUMNS = set([
    'telescope',
    'instrument',
    'phot_type',
    'MJD',
    'filter',
    'ID',
    'MAG',
    'MAG_ERR',
    'XCENTER',
    'YCENTER',
    'RA',
    'DEC',
    'CALIB_SOURCE',
    'NUM_CALIB_SOURCES',
    'CALIB_OFFSET',
    'CALIB_STDDEV'
    ])

def ingest_grond_hdf5(fname):
    dataframes = []
    
    fd = h5py.File(fname, 'r')
    for band in 'grizJHK':
        # check if band is in the hdf5 file (might contain multiple?)
        if '/GROND/%s' % band not in fd:
            continue

        # now load the dataframes..
        for phot_type in ['sextractor', 'psf', 'aperture']:
            if '/GROND/%s/phot/%s' % (band, phot_type) not in fd:
                continue

            df = pd.read_hdf(fname, key='/GROND/%s/phot/%s' % (band, phot_type))
            df['phot_type'] = phot_type
            df['MJD'] = fd['/GROND/%s' % band].attrs['mid_time_mjd']
            df['telescope'] = 'LaSilla'
            df['instrument'] = 'GROND'
            df['filter'] = band

            # load in calibration data
            if '/GROND/%s/calib' % band in fd:
                df['CALIB_SOURCE'] = fd['/GROND/%s/calib' % band].attrs['calibration_source']
                df['NUM_CALIB_SOURCES'] = fd['/GROND/%s/calib' % band].attrs['calibration_source_number']
                df['CALIB_OFFSET'] = fd['/GROND/%s/calib' % band].attrs['offset']
                df['CALIB_STDDEV'] = fd['/GROND/%s/calib' % band].attrs['stddev']

            #print(repr(df.columns))

            # XXX: correct JHK to Vega?

            # the NUMBER column from the sextractor catalog is the ID catalog for the IRAF tasks
            if phot_type == 'sextractor':
                df.rename(columns={
                    'NUMBER': 'ID',
                    'MAG_AUTO': 'MAG', 'MAGERR_AUTO': 'MAG_ERR',
                    'XWIN_IMAGE': 'XCENTER',
                    'YWIN_IMAGE': 'YCENTER',
                    'ALPHA_J2000': 'RA',
                    'DELTA_J2000': 'DEC'
                }, inplace=True)
            elif phot_type == 'psf':
                df.rename(columns={'MERR': 'MAG_ERR'}, inplace=True)
            elif phot_type == 'aperture':
                df.rename(columns={'MAG1': 'MAG', 'MERR1': 'MAG_ERR'}, inplace=True)

            
            
            # rename the remaining columns to something unique
            new_col_names = dict(map(lambda x: (x, '%s_%s' % (phot_type, x)),
                                     filter(lambda x: x not in COMMON_COLUMNS,
                                            df.columns.to_list()
                                     )))
            df.rename(columns=new_col_names, inplace=True)
            #print(repr(df.columns))
            df.set_index(['telescope', 'instrument', 'filter', 'MJD', 'ID', 'phot_type'], inplace=True)
            dataframes.append(df)

    # should be a GROND Dataframe, so should contain at least one DataFrame
    assert len(dataframes) > 0
            
    # now combine all collected dataframes
    df = pd.concat(dataframes, sort=True)

    return df
