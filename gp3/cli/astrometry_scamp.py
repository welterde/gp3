import argparse

import gp3.astrometry.scamp as astrometry

def setup_parser(parser):
    parser = argparse.ArgumentParser(description='Perform astrometry on CCD frames')
    parser.add_argument('file', nargs='+')
    parser.add_argument('-d', '--destdir')

def main(args):
    # perform calibration
    astrometry.astrometry(args.file, destdir=args.destdir)
