import argparse
import logging
import astropy.table


def setup_parser():
    parser = argparse.ArgumentParser(add_help=False)
    #parser.add_argument('--in-format', choices=['ascii', 'hdf5', 'fits'])
    parser.add_argument('--ra-key', type=str, help='Column name of Right Acension in Degree in J2000')
    parser.add_argument('--dec-key', type=str, help='Column name of Declination in Degree in J2000')
    parser.add_argument('--ra-err-key', type=str, help='Column name of RA uncertainty in Degree')
    parser.add_argument('--dec-err-key', type=str, help='Column name of DEC uncertainty in Degree')
    
    return parser


    
def check_key(cat, key, name):
    if key not in cat.colnames:
        raise KeyError('Key %s for %s not found' % (key, name))
    
def load_catalog(args, fd):

    # TODO: implement proper format handling
    cat = table.Table.read(fd)

    ra_key = args.ra_key
    dec_key = args.dec_key
    ra_err_key = args.ra_err_key
    dec_err_key = args.dec_err_key

    # TODO: add flag for catalogs without WCS..
    
    # TODO: implement proper detection algorithm
    if ra_key is None:
        ra_key = 'RAJ2000'
    if dec_key is None:
        dec_key = 'DEJ2000'
    if ra_err_key is None:
        ra_err_key = 'RAJ2000_ERR'
    if dec_err_key is None:
        dec_err_key = 'DEJ2000_ERR'

    # check that these columns actually exist
    check_key(cat, ra_key, 'RA')
    check_key(cat, dec_key, 'DEC')
    check_key(cat, ra_err_key, 'RA_ERR')
    check_key(cat, dec_err_key, 'DEC_ERR')

    cat.ra_key = ra_key
    cat.dec_key = dec_key
    cat.ra_err_key = ra_err_key
    cat.dec_err_key = dec_err_key

    return cat


