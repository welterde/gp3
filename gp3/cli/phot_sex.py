import sys
import argparse
import importlib
import logging
import functools

import gp3.phot.sextractor as phot
import gp3.phot as phot_util


def setup_parser(parser):
    parser.add_argument('file', nargs='+')
    parser.add_argument('--detect-image')
    parser.add_argument('--out-suffix', default='.fits:.cat')
    parser.add_argument('--out-type', default='fits')
    

def main():
    logging.basicConfig(level=logging.DEBUG,
                        format='[%(asctime)-15s %(levelname)-5s] %(message)s')
    parser = argparse.ArgumentParser(description='Perform photometry on CCD frames')
    setup_parser(parser)
    args = parser.parse_args()

    photfun = functools.partial(phot.phot,
                                detect_image=args.detect_image)

    phot_util.perform_multi(photfun,
                            inputs=args.file,
                            out_suffix=args.out_suffix,
                            out_type=args.out_type)
                            
