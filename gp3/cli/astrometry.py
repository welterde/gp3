import importlib
import logging
import functools
import click
try:
    import coloredlogs
except ImportError:
    coloredlogs = False

import gp3.astrometry.scamp as astrometry
    
# TODO:
# - astrometrynet+scamp method support
# - mosaic support (GROND)

@click.command()
@click.option('--debug/--no-debug', default=False)
@click.option('-d', '--destdir')
@click.option('--max-offset', type=float, default=4.0)
@click.option('--contrast-threshold', type=float, default=5.0)
@click.option('--rejected-output-file', type=click.Path(exists=False))
@click.option('--grond-arcfile-fix', is_flag=True)
@click.option('--logfile', type=click.Path(dir_okay=False, writable=True))
@click.option('--copy-bad-solutions', is_flag=True)
@click.option('--ref-image')
@click.option('--quiet', is_flag=True)
@click.argument('image', nargs=-1, type=click.Path(exists=True, dir_okay=False), required=True)
def main(image,debug,max_offset, destdir, contrast_threshold, rejected_output_file, grond_arcfile_fix, logfile, copy_bad_solutions, quiet, ref_image):
    log_level = 'INFO'
    if debug:
        log_level = 'DEBUG'
    if coloredlogs:
        coloredlogs.install(level=log_level, fmt='%(asctime)s %(name)17s %(levelname)s %(message)s')
    else:
        logging.basicConfig(level=log_level, fmt='%(asctime)s %(name)17s %(levelname)s %(message)s')
    if logfile:
        rootLogger = logging.getLogger('')
        file_handler = logging.FileHandler(logfile)
        formatter = logging.Formatter('%(asctime)s %(levelname)8s %(name)s | %(message)s')
        file_handler.setFormatter(formatter)
        rootLogger.addHandler(file_handler)
    
    def rejected_cb(fname, final_pass=False):
        if not final_pass:
            return
        if rejected_output_file is not None:
            with open(rejected_output_file, 'a') as f:
                f.write('%s\n' % fname)
        
    # perform calibration
    astrometry.astrometry(image, destdir=destdir, max_offset=max_offset, contrast_threshold=contrast_threshold, contrast_fail_callback=rejected_cb, grond_arcfile_fix=grond_arcfile_fix, copy_bad=copy_bad_solutions, quiet=quiet, ref_image=ref_image)
