import argparse
import importlib
import logging
import functools

import gp3.redcomb as redcomb
import gp3.astrometry as astro


def setup_parser():
    parser = argparse.ArgumentParser(description='Perform astrometry on CCD frames')
    parser.add_argument('file')
    parser.add_argument('-i', '--instrument', default='grond',
                        choices=['gowi', 'grond', 'grond+gp1'])
    parser.add_argument('-o', metavar='outfile', dest='outfile',
                        help='File redcombed file will be written to (%%b will be replaced by filter band)')
    parser.add_argument('--overwrite', action='store_true')
    
    parser.add_argument('-b', '--band', metavar='BAND',
                        help='Band to reduce')
    return parser

def main():
    logging.basicConfig(level=logging.DEBUG,
                        format='[%(asctime)-15s %(levelname)-5s] %(message)s')
    
    parser = setup_parser()
    args = parser.parse_args()
    
    # load correct instrument module
    imod = importlib.import_module('gp3.phot.%s' % redcomb.INSTRUMENT_MAPPING[args.instrument])
    
    # perform calibration
    phot.perform_photometry(outfile=args.outfile, overwrite=args.overwrite,
                            func=functools.partial(imod.phot,
                                                   band=args.band,
                                                   image=args.file))

if __name__ == '__main__':
    main()

