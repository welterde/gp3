import os
import sys
import click
import logging
import yaml
import shlex
import jinja2
import importlib
import threading
import json
import concurrent.futures as futures
import datetime
import astropy.table as table
import signal
try:
    import coloredlogs
except ImportError:
    coloredlogs = False

from .._version import __version__
from gp3.util.log import AddMissingFields
from gp3.util.time import verify_date
from gp3.util.coords import verify_coords

def search_config(max_level=5):
    log = logging.getLogger('cli.cfg')
    curdir = os.getcwd()
    lvl = 0
    
    log.debug('Searching for project config (startdir=%s, maxlevel=%d)..', curdir, max_level)
    
    # FIXME: dont crash if run into some unaccessable directory
    while lvl < max_level:
        fname = os.path.join(curdir, 'gp3proj.yml')
        if os.path.isfile(fname):
            log.debug('Found config at %s', fname)
            return fname
        lvl += 1
        curdir = os.path.abspath(os.path.join(curdir, os.pardir))
    log.debug('No config found')
    return None


def make_products_obj(suc_reds, failed_reds):
    ret = []
    for ob, products in suc_reds:
        ret.append({
            'stage': products.stage,
            'utag': products.utag,
            'ob': {
                'name': ob.name,
                'band': ob.band
            },
            'files': [],
            'success': True
        })
        for prod in products.products:
            ret[-1]['files'].append(prod.fname)
    for ob, products in failed_reds:
        ret.append({
            'stage': products.stage,
            'utag': products.utag,
            'ob': {
                'name': ob.name,
                'band': ob.band
            },
            'success': False
        })
    return ret

class Project(object):
    
    def __init__(self, rootdir):
        self.rootdir = rootdir
        self.log = logging.getLogger('proj')
        self.cfg_fname = os.path.join(self.rootdir, 'gp3proj.yml')
        
        self.log.debug('Loading config file %s', self.cfg_fname)
        with open(self.cfg_fname, 'r') as f:
            self.raw_cfg = yaml.load(f)
            self.log.debug('Loaded config: %s', self.raw_cfg)
        # TODO: create proper config object
        
        self.raw_directory = os.path.join(rootdir, 'raw')
        self.red_directory = os.path.join(rootdir, 'red')
        self.results_directory = os.path.join(rootdir, 'results')
        
        # produced during run
        self.products = []
        self.products_lock = threading.Lock()
    
    def raw_file(self, fname, tag=None):
        return os.path.join(self.raw_directory, fname)
    
    def red_file(self, redfile):
        subdir = os.path.join(self.red_directory, '%s_%s' % (redfile.stage, redfile.utag))
        if not os.path.isdir(subdir):
            os.makedirs(subdir)
        return os.path.join(subdir, redfile.fname)
    
    def register_products(self, ob, products):
        with self.products_lock:
            self.products.append((ob, products))
    
    def create_results_dir(self, run_id):
        result_dir = os.path.join(self.results_directory, run_id)
        if os.path.exists(result_dir):
            raise ValueError('Run-id already used!')
        
        os.makedirs(result_dir)
        
        success_jobs = []
        failed_jobs = []
        # create links from result dir to the red directory
        for ob, products in self.products:
            stage_dir = os.path.join(result_dir, products.stage)
            if not os.path.exists(stage_dir):
                os.makedirs(stage_dir)
            
            src_fnames = list(map(self.red_file, products.products))
            if len(filter(lambda a: not a, map(os.path.isfile, src_fnames))):
                self.log.warning('Reduction stage %s for OB %s not complete (missing file(s))', products.stage, ob.name)
                failed_jobs.append((ob, products))
                continue
            for src_fname, product in zip(src_fnames, products.products):
                dst_fname = os.path.join(stage_dir, product.fname)
                if os.path.exists(dst_fname):
                    self.log.error('Encountered duplicated product filename %s (%s)', product.fname, product)
                    continue
                os.symlink(src_fname, dst_fname)
            success_jobs.append((ob, products))
        
        # and dump the product information into a json file
        product_fname = os.path.join(result_dir, 'products.json')
        product_data = make_products_obj(success_jobs, failed_jobs)
        with open(product_fname, 'w') as f:
            json.dump(product_data, f)
    
    @staticmethod
    def create(destdir, template):
        assert isinstance(template, ProjectTemplate)
        
        template.generate(destdir)
    
    def update(self):
        if 'pipeline' not in self.raw_cfg:
            self.log.critical('Pipeline not specified in gp3proj.yml!')
            sys.exit(1)
        
        pipe_mod = importlib.import_module(self.raw_cfg['pipeline'])
        executor = futures.ThreadPoolExecutor(max_workers=12)
        self.pipeline = pipe_mod.Pipe(job_engine=executor)
        self.pipeline.setup(self)
        self.pipeline.run()
        
        # create result directory
        run_id = datetime.datetime.now().isoformat()
        self.create_results_dir(run_id)
    
    def list_products(self, run_id):
        if run_id == 'latest':
            self.log.debug('Searching for latest run id')
            entries = sorted(os.listdir(self.results_directory))
            if len(entries) == 0:
                self.log.error('There are no runs in the results directory! That means the pipeline never finished successfully.. Maybe run gp3-proj update first?')
                sys.exit(1)
            run_id = entries[-1]
        prod_fname = os.path.join(self.results_directory, run_id, 'products.json')
        if not os.path.exists(prod_fname):
            self.log.error('Invalid run-id specified! Does not contain a products.json')
            sys.exit(2)
        with open(prod_fname, 'r') as f:
            prod_data = json.load(f)
        rows = []
        for row in prod_data:
            fname = os.path.relpath(os.path.join(self.results_directory, run_id, row['stage'], row['fname']), start=self.rootdir)
            rows.append((row['ob']['name'], row['stage'], row['utag'], fname))
        tab = table.Table(rows=rows, names=('OB Name', 'Stage', 'Input Hash', 'File Name'))
        tab.sort(['OB Name', 'Stage'])
        tab.pprint(max_lines=-1, align='<')
        
    # TODO: refactor this.. a lot of duplicate stuff with list_products
    def list_obs(self, run_id):
        if run_id == 'latest':
            self.log.debug('Searching for latest run id')
            entries = sorted(os.listdir(self.results_directory))
            if len(entries) == 0:
                self.log.error('There are no runs in the results directory! That means the pipeline never finished successfully.. Maybe run gp3-proj update first?')
                sys.exit(1)
            run_id = entries[-1]
        prod_fname = os.path.join(self.results_directory, run_id, 'products.json')
        if not os.path.exists(prod_fname):
            self.log.error('Invalid run-id specified! Does not contain a products.json')
            sys.exit(2)
        with open(prod_fname, 'r') as f:
            prod_data = json.load(f)
        
        obs = {}
        stages = set()
        for row in prod_data:
            stage = row['stage']
            stages.add(stage)
            name = row['ob']['name']
            if name not in obs:
                obs[name] = {}
            if stage not in obs[name]:
                obs[name][stage] = 0
            prod_fname = os.path.join(self.results_directory, run_id, stage, row['fname'])
            if os.path.exists(prod_fname):
                obs[name][stage] += 1
        rows = []
        stages = sorted(stages)
        for ob_name, stage_counts in obs.items():
            row = [ob_name]
            for stage in stages:
                row.append(stage_counts.get(stage, 0))
            rows.append(row)
        names = ['OB Name']+stages
        tab = table.Table(rows=rows, names=names)
        tab.sort('OB Name')
        tab.pprint(max_lines=-1, align='<')
        

class ProjectTemplate(object):
    
    def __init__(self):
        self.env = jinja2.Environment(loader=jinja2.PackageLoader('gp3', 'templates'))
    
    def generate(self, destdir):
        pass

    def render(self, template_name, **kwargs):
        template = self.env.get_template(template_name)
        return template.render(**kwargs)

class LCTemplate(ProjectTemplate):
    
    def __init__(self, target_names, target_coords, start_date):
        self.target_names = target_names
        self.target_coords = target_coords
        self.start_date = start_date
        ProjectTemplate.__init__(self)

    def generate(self, destdir):
        for subdir_name in ['raw', 'red', 'results', 'tmp']:
            os.makedirs(os.path.join(destdir, subdir_name))
        
        cfg_filename = os.path.join(destdir, 'gp3proj.yml')
        with open(cfg_filename, 'w') as f:
            f.write(self.render('proj/cfg_lc.yml.j2', target_names=self.target_names, target_coords=self.target_coords, start_date=self.start_date.isoformat()))



def ensure_proj(proj, cmdname):
    if proj is None:
        logging.critical('The %s command must be run inside a GP3 project directory!', cmdname)
        sys.exit(1)


@click.group()
@click.version_option(version=__version__)
@click.option('--debug/--no-debug', default=False)
@click.pass_context
def cli(ctx,debug):
    ctx.ensure_object(dict)
    
    # Setup logging with maybe coloredlogs and maybe debug output
    log_level = 'INFO'
    if debug:
        log_level = 'DEBUG'
    if coloredlogs:
        coloredlogs.install(level=log_level, fmt='%(asctime)s %(name)17s %(obname)-17s %(levelname)s %(message)s')
    else:
        logging.basicConfig(level=log_level, fmt='%(asctime)s %(name)17s %(obname)-17s %(levelname)s %(message)s')
    logging.getLogger().handlers[0].addFilter(AddMissingFields(['obname']))
    log = logging.getLogger('cli')
    
    # look for gp3 project configuration
    ctx.obj = None
    cfg_file = search_config()
    if cfg_file:
        rootdir = os.path.dirname(cfg_file)
        ctx.obj = Project(rootdir)



@cli.command('create-lc', short_help='Create new project with lightcurve template')
@click.argument('projdir', type=click.Path(exists=False, file_okay=False, dir_okay=False))
@click.option('--target-names', prompt='Target Name(s)', help='List of target names separated by comma')
@click.option('--start-date', callback=verify_date, default='2018-01-01')
@click.option('--target-coords', prompt='Target Coordinates', help='Target coordinates to use in lightcurve generation', callback=verify_coords)
@click.pass_obj
def create_lc(proj, projdir, target_names, start_date, target_coords):
    if proj is not None:
        logging.critical('Trying to create a GP3 project inside another GP3 project!')
        sys.exit(1)
    
    # handle spaces, commas, etc.
    target_names = shlex.split(target_names.replace(',', ' '))
    
    print('Projdir: %s; Target: %s; Start Date: %s' % (projdir, target_names, start_date))
    
    
    Project.create(projdir, LCTemplate(target_names=target_names, target_coords=target_coords, start_date=start_date))

def signal_handler(signal, frame):
    sys.exit("CTRL+C detected, stopping execution")

@cli.command('update', short_help='Update current project')
@click.pass_obj
def update(proj):
    # TODO: maybe that a decorator?
    ensure_proj(proj, 'update')
    signal.signal(signal.SIGINT, signal_handler)
    proj.update()

@cli.command('list-products', short_help='List products of a pipeline run')
@click.option('--run-id', default='latest')
@click.pass_obj
def list_products(proj, run_id):
    # TODO: maybe that a decorator?
    ensure_proj(proj, 'list-products')
    
    proj.list_products(run_id)

@cli.command('list-obs', short_help='List OBs present in a pipeline run')
@click.option('--run-id', default='latest')
@click.pass_obj
def list_obs(proj, run_id):
    # TODO: maybe that a decorator?
    ensure_proj(proj, 'list-obs')
    
    proj.list_obs(run_id)

def main():
    cli(obj={})
