import argparse
import importlib
import logging

import gp3.redcomb as redcomb

# GROND usage:
# gp3-redcomb -i grond+gp1 -b g -o OB1_2-g.fits /data/SMC42/OB1_2
# gp3-redcomb -i grond -b all -o 'OB1_2-%b.fits' /data/SMC42/OB1_2

def setup_parser():
    parser = argparse.ArgumentParser(description='Reduce and combine CCD frames')
    parser.add_argument('files', nargs='+')
    parser.add_argument('-i', '--instrument', default='grond',
                        choices=['gowi', 'grond', 'grond+gp1'])
    parser.add_argument('-o', metavar='outfile', dest='outfile',
                        help='File redcombed file will be written to (%%b will be replaced by filter band)')
    parser.add_argument('--overwrite', action='store_true')
    
    parser.add_argument('-b', '--bands', metavar='BANDS', default='all',
                        help='Bands to reduce')
    
    parser.add_argument('-c', '--calibdir', metavar='DIR', dest='calibdir',
                        help='')
    
    parser.add_argument('-t', '--do-tdp', action='store_false', dest='do_tdp',
                        help="Don't stack the individual TDPs")
    parser.add_argument('--extra-args')
    return parser

def main():
    logging.basicConfig(level=logging.DEBUG,
                        format='[%(asctime)-15s %(levelname)-5s] %(message)s')
    
    parser = setup_parser()
    args = parser.parse_args()
    
    # load correct instrument module
    imod = importlib.import_module('gp3.redcomb.%s' % redcomb.INSTRUMENT_MAPPING[args.instrument])

    extra_args = {}
    # TODO: syntax error handling
    if args.extra_args:
        for p in args.extra_args.split(','):
            k,v = p.split('=')
            extra_args[k] = v
    
    # perform calibration
    imod.redcomb(src_list=args.files, outfile=args.outfile, bands=args.bands,
                 overwrite=args.overwrite, calibdir=args.calibdir,
                 tdp_stack=args.do_tdp, extra_args=extra_args)
