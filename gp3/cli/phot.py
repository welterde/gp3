import sys
import argparse
import importlib
import logging
import functools

import gp3.redcomb as redcomb
import gp3.phot as phot

import astropy.coordinates as coord
import astropy.units as u



def setup_parser():
    parser = argparse.ArgumentParser(description='Perform photometry on CCD frames')
    parser.add_argument('file')
    parser.add_argument('-m', '--method', default='gp1',
                        choices=['gp1', 'sextractor', 'sex'],
                        help='Method for photometry to use')
    parser.add_argument('-o', metavar='outfile', dest='outfile',
                        help='File redcombed file will be written to (%%b will be replaced by filter band)')
    parser.add_argument('--overwrite', action='store_true',
                        help='Overwrite existing output files')
    
    parser.add_argument('-b', '--band', metavar='BAND',
                        help='Band to reduce')
    parser.add_argument('-f', '--force-detect', metavar=('RA', 'DEC'), nargs=2, help='Do a forced detection on a position', default=None)
    parser.add_argument('--extra-opts', metavar=('K=V'), action='append', help='Pass extra options to backend', default=[])
    parser.add_argument('--save-astrometric-image', default=None, help='Store astrometric image somewhere')
    return parser

def main():
    logging.basicConfig(level=logging.DEBUG,
                        format='[%(asctime)-15s %(levelname)-5s] %(message)s')
    
    parser = setup_parser()
    args = parser.parse_args()
    
    # load correct method module
    if args.method.startswith('sex'):
        method = 'sextractor'
    elif args.method in ['gp1']:
        method = args.method
    else:
        sys.stderr.write('Unknown method %s!', args.method)
        sys.exit(1)
        
    force_detect = None
    if args.force_detect:
        force_detect = coord.SkyCoord(args.force_detect[0], args.force_detect[1], unit=(u.deg, u.deg))

    imod = importlib.import_module('gp3.phot.%s' % method)
    func = functools.partial(imod.phot,
                             band=args.band,
                             image=args.file,
                             force_detect=force_detect,
                             extra_options=args.extra_opts,
                             astrometric_image_dest=args.save_astrometric_image)
    
    phot.perform_photometry(outfile=args.outfile, overwrite=args.overwrite,
                            func=func)
