import numpy as np



def convert_mosfit(df, target_name, mag_column='MAG', magerr_column='MAGERR', upper_lim_mag_column='LIMIT', mag_system='AB'):
    # construct photometry table
    photometry = []

    # get rid of multi-index and convert them to columns
    df = df.reset_index()
    for idx, row in df.iterrows():
        p = {
            'time': row['MJD'],
            'band': row['filter'],
            'instrument': row['instrument'],
            'telescope': row['telescope'],
            'magnitude': row[mag_column],
            'e_magnitude': row[magerr_column],
            'u_time': 'MJD',
            'source': '1',
            'system': mag_system
        }
        if np.isnan(row[mag_column]):
            if upper_lim_mag_column is None:
                continue
            
            p['magnitude'] = row[upper_lim_mag_column]
            del p['e_magnitude']
            p['upperlimit'] = True
            
        
        photometry.append(p)
        

    evt = {
        'name': target_name,
        'sources': [
            {'name': 'gp3', 'alias': '1'}
        ],
        'alias': [
            {'name': target_name, 'source': '1'}
        ],
        'photometry': photometry
    }

    ret = {
        target_name: evt
    }
    return ret

