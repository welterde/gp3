import operator

import numpy as np

import gp3.imageproc.backend as backend
import gp3.imageproc.inst as instructions


class NumpyBackend(backend.Backend):
    def __init__(self, instruction, buffer_dim=(50,50)):
        self.inst = instruction
        self.buffer_dim = buffer_dim

    def eval(self, inst):
        if isinstance(inst, instructions.Load):
            return self.eval_load(inst.name, inst.mode, inst.data)
        elif isinstance(inst, instructions.BinOp):
            return self.eval_binop(inst.op, inst.in1, inst.in2)
        elif isinstance(inst, instructions.Average):
            return self.eval_average(inst.inputs, inst.reject)
        else:
            raise ValueError()
    
    def eval_binop(self, op, in1, in2):
        return op(self.eval(in1), self.eval(in2))

    def eval_load(self, name, mode, data):
        if data == None:
            data = self.inputs[name]
        
        if len(self.buffer_len) == 1:
            if mode == 'image':
                return data[self.state[0], self.state[1]:(self.state[1]+self.buffer_len)]
            elif mode == 'column':
                return data[self.state[1]:(self.state[1]+self.buffer_len)]
            elif mode == 'row':
                return data[self.state[0]]
            elif mode == 'const':
                return data
            else:
                raise ValueError()
        else:
            if mode == 'image':
                return data[self.state[0]:(self.state[0]+self.buffer_len[0]),
                            self.state[1]:(self.state[1]+self.buffer_len[1])]
            elif mode == 'column':
                buf = data[self.state[1]:(self.state[1]+self.buffer_len[1])]
                return buf.reshape((1, self.buffer_len[1]))
            elif mode == 'row':
                buf = data[self.state[0]:(self.state[0]+self.buffer_len[0])]
                return buf.reshape((self.buffer_len[0], 0))
            elif mode == 'const':
                return data
            else:
                raise ValueError()

    def eval_average(self, input_insts, reject):
        # create temporary buffer
        buf = np.empty([len(input_insts)] + self.buffer_len, dtype=np.float32)

        # now eval all those instructions
        for i,idx in zip(input_insts, range(len(input_insts))):
            buf[idx] = self.eval(i)

        # apply rejection
        if reject == None or len(reject) < 1:
            return np.median(buf, axis=0)
        elif reject[0] == 'minmax':
            nl = reject[1]
            nh = reject[2]
            n = max(nl, nh)
            if n > len(input_insts):
                raise ValueError()
            bufs = np.sort(buf, axis=0)
            return np.median(bufs[nl:-nh], axis=0)
        else:
            raise ValueError()
    
    def execute(self, out, inputs):
        self.inputs = inputs
        self.state = (0,0)

        if len(self.buffer_dim) > 1:
            dims = self.buffer_dim
            self.buffer_len = [0,0]
        else:
            dims = (1, self.buffer_dim[0])
            self.buffer_len = [0]
        
        for x in range(out.shape[0] / dims[0] + 1):
            for y in range(out.shape[1] / dims[1] + 1):
                if len(self.buffer_dim) > 1:
                    self.buffer_len[0] = min(dims[0], abs(x*dims[0] - out.shape[0]))
                    self.buffer_len[1] = min(dims[1], abs(y*dims[1] - out.shape[1]))
                else:
                    self.buffer_len[0] = min(dims[1], abs(y*dims[1] - out.shape[1]))
                
                self.state = (x*dims[0], y*dims[1])
                print self.state, self.buffer_len
                if len(self.buffer_dim) > 1:
                    out[self.state[0]:(self.state[0]+self.buffer_len[0]),
                        self.state[1]:(self.state[1]+self.buffer_len[1])] = self.eval(self.inst)
                else:
                    out[self.state[0],
                        self.state[1]:(self.state[1]+self.buffer_len)] = self.eval(self.inst)
