

import astropy.io.fits as fits

import gp3.imageproc.inst as inst


class CCDData(object):

    def __init__(self, hdu, filename=None):
        self.hdu = hdu
        self.filename = filename
        self.instructions = inst.Load(self.hdu.data)

        # ok.. we need to do the BZERO + BSCALE*fits transformation
        if 'BZERO' in hdu.header:
            bzero = hdu.header['BZERO']
            bscale = hdu.header['BSCALE']

            if bscale != 1.0:
                self.transformation = inst.BinOp()
