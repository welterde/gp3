

class Instruction(object):

    def __init__(self, children):
        self.nodes = children

class BinOp(Instruction):

    def __init__(self, op, in1, in2):
        self.op = op
        self.in1 = in1
        self.in2 = in2
        super(BinOp, self).__init__(self, [in1, in2])

class Load(Instruction):

    def __init__(self, data=None, name=None, mode='image'):
        if name == None and data == None:
            raise ValueError()
        self.name = name
        self.data = data
        self.mode = mode
        super(Load, self).__init__(self, [])

class Average(Instruction):

    def __init__(self, inputs, reject=None):
        self.inputs = inputs
        self.reject = reject
        super(Average, self).__init__(self, inputs)

