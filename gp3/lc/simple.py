import logging
import numpy as np
try:
    from numba import njit
except ImportError:
    # TODO: log missing numba
    njit = lambda a: a

from scipy.spatial import cKDTree
from astropy.coordinates import SkyCoord
import astropy.units as u

"""
Simple LC Processing module.

Just uses a sigma clipped median filter on reference objects.
"""



# Without numba this will be really slow for more than 1M objects..
@njit
def select_first(arr):
    for i in range(len(arr)):
        if arr[i]:
            return i
    return -1


@njit
def update_offsets(offsets, groupings, include_group, ref_obs_idx, times_inv, data):
    
    for row_idx in range(len(groupings)):
        group_idx = groupings[row_idx]
        if not include_group[group_idx]:
            #print('[%02d] SKIP GRP %d' % (row_idx, group_idx))
            continue
        mag1 = data[ref_obs_idx[group_idx]]
        mag2 = data[row_idx]
        offset = mag2 - mag1
        time_idx = times_inv[row_idx]
        #print('[%02d] group=%d m1=%f m2=%f time=%d' % (row_idx, group_idx, mag1, mag2, time_idx))
        offsets[time_idx, group_idx] = offset
        

@njit
def compute_lc(groupings, group_idx, mags, err_mag, obs_count, offsets, row_time, row_caloffset):
    ret = np.empty(obs_count, dtype=np.float64)
    ret_err = np.empty(obs_count, dtype=np.float64)
    mjd = np.empty(obs_count, dtype=np.float64)
    valid = np.zeros(obs_count, dtype=np.bool_)
    caloffset = np.zeros(obs_count, dtype=np.float64)*np.nan
    raw_mags = np.empty(obs_count, dtype=np.float64)
    
    m = groupings == group_idx
    for i,row_idx in enumerate(m.nonzero()[0]):
        raw_mags[i] = mags[row_idx]
        ret[i] = mags[row_idx] + offsets[i]
        ret_err[i] = err_mag[row_idx]
        mjd[i] = row_time[row_idx]
        valid[i] = True
        caloffset[i] = row_caloffset[row_idx]
    
    return ret, ret_err, mjd, valid, raw_mags, caloffset

class SimpleLC(object):
    
    def __init__(self, xmatch_radius):
        self.raw_obs = []
        self.reference_epoch = None
        self.xmatch_radius = xmatch_radius
        self.xmatch_radians = xmatch_radius.to(u.rad).value
        self.log = logging.getLogger('simple_lc')
        self.log.debug('Created with xmatch radius %s', repr(xmatch_radius))
    
    def add_observation(self, time_mjd, obs, calib_offset, reference_epoch=False):
        self.log.debug('Adding observation for time-mjd %f with %d objects', time_mjd, len(obs))
        # obs struct: {ra,dec,mag,err_mag}
        self.raw_obs.append((time_mjd, obs, calib_offset))
        # FIXME: check that time_mjd does not exist yet.. duplicate!
        if reference_epoch:
            if self.reference_epoch:
                # XXX: or throw exception here?
                self.log.error('Reference epoch set twice (old=%s, new=%s)!', self.reference_epoch, time_mjd)
            self.log.debug('Settings reference epoch to %s', time_mjd)
            self.reference_epoch = time_mjd
            
    
    def process(self):
        self.log.info('Computing light curve with %d observations', len(self.raw_obs))
    
        # Sort it by mjd
        raw_obs = sorted(self.raw_obs, key=lambda a: a[0])
        
        time_idx = np.array([t_mjd for t_mjd,_data in raw_obs])
        self.mjds = time_idx
        
        # generate magnitude and coordinate tables
        n_obs = 0
        for _time, obs in raw_obs:
            n_obs += len(obs)
        
        self.log.debug('Building master table with %d objects', n_obs)
        mags = np.zeros(n_obs, dtype={'names': ('mag', 'err_mag'),
                                      'formats': ('f8', 'f8')})
        coords = np.zeros((n_obs, 2), dtype='f8')
        times = np.zeros(n_obs, dtype='f8')
        cal_offsets = np.zeros(n_obs, dtype='f8')*np.nan
        times_inv = np.zeros(n_obs, dtype=np.int32)
        offset = 0
        for t_mjd, obs, photcal_offset in raw_obs:
            length = len(obs)
            mags['mag'][offset:(offset+length)] = obs['MAG']
            mags['err_mag'][offset:(offset+length)] = obs['MERR']
            coords[offset:(offset+length), 0] = obs['RA']
            coords[offset:(offset+length), 1] = obs['DEC']
            times[offset:(offset+length)] = t_mjd
            cal_offsets[offset:(offset+length)] = photcal_offset
            times_inv[offset:(offset+length)] = (time_idx == t_mjd).nonzero()[0][0]
            offset += length
            
        coords_obj = SkyCoord(coords[:,0]*u.deg, coords[:,1]*u.deg)
        
        # perform cross-matching into groups (which are each considered one group)
        #self.log.debug('Building KD tree for xmatch..')
        #tree = cKDTree(coords)
        unmatched = np.ones(n_obs, dtype=np.bool)
        
        self.log.debug('Performing xmatch with search radius %e', self.xmatch_radius)
        groupings = np.zeros(n_obs, dtype=np.int32)
        group_ctr = 0
        # TODO: or maybe we can do this more easily with the query_ball_tree function?
        while np.count_nonzero(unmatched) > 0:
            # select the first unmatched object
            sel_idx = select_first(unmatched)
            sel_coord = SkyCoord(coords[sel_idx,0]*u.deg, coords[sel_idx, 1]*u.deg)
            # p-2 = euclidian norm
            #_kd_dist, kd_idx = tree.query(x=coords[sel_idx], k=1, p=2)
            dist = sel_coord.separation(coords_obj)
            match_idxs = dist < self.xmatch_radius
            groupings[match_idxs] = group_ctr
            unmatched[match_idxs] = False
            
            group_ctr += 1
        
        self.log.debug('%d objects crossmatched', group_ctr)
        
        offsets = np.empty((len(raw_obs), group_ctr), dtype=np.float32)*np.nan
        offsets_err = np.empty((len(raw_obs), group_ctr), dtype=np.float32)*np.nan
        
        ref_obs_rows = times == self.reference_epoch
        
        ref_obs_idx = np.empty(group_ctr, dtype=np.int32)
        
        # TODO: now compute diagnostics for these groups
        included_groups = np.zeros(group_ctr, dtype=np.bool)
        for i in range(group_ctr):
            m = groupings == i
            utimes = np.unique(times[m])
            if np.count_nonzero(m) > len(utimes):
                self.log.warning('Object %04d has multiple detections in one epoch.. Should be filtered! RA=%f DEC=%f', i, coords[m][0,0], coords[m][0,1])
                continue
            m = np.logical_and(m, ref_obs_rows)
            if np.count_nonzero(m) != 1:
                self.log.debug('Ignoring object %04d.. not one observation matched (%d matched)', i, np.count_nonzero(m))
                continue
            ref_obs_idx[i] = m.nonzero()[0][0]
            included_groups[i] = True
        
        self.log.debug('Computing the offsets..')
        update_offsets(offsets, groupings, included_groups, ref_obs_idx, times_inv, mags['mag'])
        #update_offsets(offsets_err, groupings, included_groups, ref_obs_idx, times_inv, mags['err_mag'])
        self.log.debug('Matrix: \n%s', offsets)
        t_offsets = np.nanmedian(offsets, axis=1)
        self.log.debug('Got: %s', t_offsets)
        
        self.log.debug('Computing lightcurves')
        self.lc = []
        self.lc_coords = []
        for i in range(group_ctr):
            self.lc.append(compute_lc(groupings, i, mags['mag'], mags['err_mag'], len(self.raw_obs), t_offsets, times, cal_offsets))
            m = groupings == i
            
            self.lc_coords.append((np.mean(coords[m,0]), np.mean(coords[m,1])))

        
