from astropy.table import Table, Column
import numpy as np
import astropy.units as u

class LineDB(object):
    
    def __init__(self):
        self.lines = Table(names=('LineID', 'Name', 'Center', 'Width', 'Group', 'Sky', 'Color'), dtype=(np.int16, np.dtype('S42'), np.float, np.float, np.dtype('S21'), np.int8, np.dtype('S16')))
        self.lines['Center'].unit = u.Angstrom
        self.lines['Width'].unit = u.Angstrom
        self.line_ctr = 0
    
    def identify(self, center, width):
        dist = np.abs(self.lines['Center'] - center)
        width = np.ones(len(self.lines))*width
        m = dist < np.max([width, self.lines['Width']], axis=0)
        return self.lines[m]

    def load_lines(self, fname, color='blue', bands=False, group=None, sky=0, format='ascii.commented_header', width=None):
        l = Table.read(fname, format=format)
        if bands:
            l.add_column(Column(data=(l['Start'] + l['Stop'])/2.0, name='Center'))
            l.add_column(Column(data=(l['Stop'] - l['Start']), name='Width'))
        elif 'Width' not in l.colnames and width:
            l.add_column(Column(data=np.ones(len(l))*width, name='Width'))
        l.add_column(Column(data=np.zeros(len(l)), name='LineID'))
        for i in range(len(l)):
            l['LineID'][i] = self.line_ctr
            self.line_ctr += 1
        for line in l:
            if 'Comment' in l.colnames:
                name = line['Comment']
            else:
                name = None
            self.lines.add_row([line['LineID'],
                                name,
                                line['Center'],
                                line['Width'],
                                group,
                                sky,
                                color])
