import numpy as np
import numba
from specutils import Spectrum1D
from gp3.pipe import Recipe, Requirement, Product, Parameter


@numba.jit(nopython=True)
def clip_lines_kernel(data, uncertainty, sigma, mask):
    # TODO: hmm.. and to support lower resolution (worse than 3px PSF) just need to change 2.0 fudge constant?
    # TODO: write some tests and replace with a variant that performs better
    for i in range(1, data.size - 1):
        if np.isnan(data[i]) or np.isnan(data[i-1]) or np.isnan(data[i+1]):
            continue
        mask[i] = (2.0*(data[i-1] + sigma*uncertainty[i-1]) < data[i] - sigma*uncertainty[i]) or (2.0*(data[i+1] + sigma*uncertainty[i+1]) < data[i] - sigma*uncertainty[i])
    
    return mask

def clip_lines(spec, sigma):
    data = np.array(spec.data, dtype='f4', copy=True)
    uncertainty = np.array(spec.uncertainty.array, dtype='f4')
    
    mask = clip_lines_kernel(data, uncertainty, sigma, np.zeros(len(spec.data), dtype=np.bool))
    data[mask] = np.nan
    return Spectrum1D(flux=data, wcs=spec.wcs, uncertainty=spec.uncertainty, meta=spec.meta, unit=spec.unit)
    
    

class ClipUnphysicalLines(Recipe):
    input_spec = Requirement(Spectrum1D)
    output_spec = Product(Spectrum1D)
    #min_pix_size = Parameter(int, default=2)
    sigma = Parameter(int, default=5)
    
    def run(self):
        spec = self.input_spec.value
        sigma = self.sigma.value
        
        self.output_spec.value = clip_lines(spec, sigma)
