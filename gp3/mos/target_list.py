import astropy.io.fits as fits
from astropy.time import Time
import specutils.io.read_fits as read_fits

from .target import Target

class TargetList(object):
    
    def __init__(self):
        self.targets = {}
    
    def add_target(self, name):
        if name in self.targets:
            return
        self.targets[name] = Target(name)
        
    def add_spec(self, target, spectrum, stype, src_file, extra={}):
        self.add_target(target)
        self.targets[target].add_spec(spectrum, stype, src_file)

    def load_aao(self, fname):
        hdu = fits.open(fname)
        time = Time(hdu[0].header['UTMJD'], format='mjd')
        specs = read_fits.read_fits_spectrum1d(fname)
        specs_noskysub = read_fits.read_fits_spectrum1d(fname, hdu='RWSS')
        # only keep program objects (no sky fiber or guide fiber)
        specs_obj = filter(lambda a: a.meta['TYPE'] == 'P', specs)
        specs2_obj = filter(lambda a: a.meta['TYPE'] == 'P', specs_noskysub)
        # now add all spectra to the database
        for spec,spec2 in zip(specs_obj, specs2_obj):
            spec.meta['TIME'] = time
            spec2.meta['TIME'] = time
            # TODO: guess arm
            self.add_spec(spec.meta['NAME'], spec, 'normal', fname)
            self.add_spec(spec.meta['NAME'], spec2, 'RWSS', fname)
