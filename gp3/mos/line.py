class Line(object):
    
    def __init__(self, name, wavelength, sky=False, group=None):
        self.name = name
        self.wavelength = wavelength
        self.sky = sky
        self.group = group
