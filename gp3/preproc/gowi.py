import os
import logging
import socket
import ephem

import astropy.io.fits as fits
from astropy.coordinates import name_resolve, SkyCoord
import astropy.time as time
import astropy.units as u

import ephem

import gp3.util.cog as cog

import msumastro.header_processing.patchers as patchers



log=logging.getLogger('gp3.preproc.gowi')

def set_site(name):
    """
    Override the feder observatory site by the COG/LS site
    """
    name = name.lower()
    if name == 'cog':
        patchers.feder = cog.COG()
    else:
        raise ValueError('Unknown observatory site: %s' % name)


# TODO: move to generic
def add_obj_pos(header):
    # no need
    if 'RA' in header or 'OBJCTRA' in header:
        return
    
    object_name = header['OBJECT']
    if object_name == 'zilla_monster':
        return False
    if object_name.capitalize() in dir(ephem):
        body = getattr(ephem, object_name.capitalize())(cog.cog_site)
        #cog.cog_site.date = time.Time(header['DATE-OBS']).jd
        body.compute(time.Time(header['DATE-OBS']).iso)
        object_coords = SkyCoord('%s %s' % (body.a_ra, body.a_dec), frame="icrs", unit=(u.hourangle, u.deg))
    else:
        try:
            object_coords = SkyCoord.from_name(object_name)
        except (name_resolve.NameResolveError, socket.timeout) as e:
            log.warning('Unable to lookup position for %s', object_name)
            log.warning(e)
            object_coords = False
    
    if not object_coords:
        return False
    
    common_format_keywords = {'sep': ':',
                              'precision': 2,
                              'pad': True}
    header['RA'] = object_coords.ra.to_string(unit=u.hour,
                                              **common_format_keywords)
    header['DEC'] = object_coords.dec.to_string(unit=u.degree,
                                                **common_format_keywords)
    log.info("Added Coordinates %s based on object name %s", repr(object_coords), object_name)
    return True
    


def preproc(hdu):
    header = hdu.header
    
    set_site('cog')
    
    try:
        if header['OBJECT'] != 'zilla_monster':
            add_obj_pos(header)
        patchers.get_software_name(header)
        patchers.purge_bad_keywords(header, history=True)
        patchers.change_imagetype_to_IRAF(header, history=True)
        patchers.add_time_info(header, history=True)
        if header['imagetyp'] == 'LIGHT' and ('RA' in header or 'OBJCTRA' in header):
            patchers.add_object_pos_airmass(header, history=True)
        patchers.add_overscan_header(header, history=True)
    except KeyError:
        log.warn('Frames by unknown software')
