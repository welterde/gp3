############
Installation
############

Requirements
============

The following python packages are required to be installed:

.. list-table::
   :header-rows: 1
   :widths: 1 1
   
   * - Name
     - Version
   
   * - astropy
     - >=1.3
   * - numpy
     - >=1.10
   * - scipy
     - >=1.0.0
   * - click
     - >= 4.0
   * - h5py
     - >= 2.8.0
   * - matplotlib
     - >= 2.0.0

The following packages are required for certain functionality to be available:

.. list-table::
   :header-rows: 1
   :widths: 1 1 3
   
   * - Name
     - Version
     - Function
     
   * - coloredlogs
     - >= 6.1
     - Shows logs in color
   * - jinja2
     - >= 2.9.0
     - MOSview
   * - gp
     - >= 2.2.5.1
     - Reduction and photometry using gp pipeline
   * - sphinx
     - >= 1.8.5
     - Building the documentation
   * - sphinx-click
     - >= 2.2.0
     - Building the documentation

Using Conda
===========

One way to install gp3 is to install all requirements in a `Conda <https://conda.io/>`_ environment.
For instruction on how to install refer to the `Conda installation guide <https://docs.conda.io/projects/conda/en/latest/user-guide/install/>`_.

First create a new conda environemnt in case one doesn't wish to break an existing one:

.. code:: bash
	  
   conda create --name gp3 -c conda-forge python==2.7 numpy==1.14.5 scipy==1.10 matplotlib==2.0.0 h5py==2.8.0 click==4.0

Activate the environment and install the recommended requirements:

.. code:: bash
   
   source activate gp3
   conda install -c forge link to requirements file?
   # to be finished..

TODO: how to upgrade?

Using Debian Packages
=====================

In case you are running a debian based system (debian or ubuntu) then the probably most comfortable way is to globally install as an debian package.

TODO: link to my repository and keys, etc.

.. code:: bash
   
   apt-get update
   apt-get install gp3



