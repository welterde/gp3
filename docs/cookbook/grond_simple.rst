Simple GROND data reduction
===========================

In case of a one-off reduction of some target or in case some further processing between the pipeline stages is desired (or one wishes to integrate this into a different pipeline) it might be benefital to call the individual pipeline stages manually.

Requirements
------------

* Raw frames of one or more observation block in *RAW_FRAMES* directory
* Calibration frames stored in ~/data/calib directory in the usual layout (one directory per day in the format YYYY-MM-DD)

Reduction and Combination
-------------------------

First we have to the basic reduction of the raw frames and combine them to one stacked image per band (the two shown commands are equivalent):

.. code:: bash
   
   gp3-redcomb -i grond -o smc0032_g.fits -b g $RAW_FRAMES/*.fits
   gp3-redcomb --instrument grond -o smc0032_g.fits --bands g RAW_FRAMES/*.fits

The NIR frames in the commandline argument are ignored when processing optical frames (and vice versa).
In case raw frames from multiple observation blocks are specified they are automatically stacked.

The script will automatically search for the closest (in time) usable calibration frames in the default calibration directory.
But a directory containing the necessary calibration files can also be manually specified using the *--calibdir* option:

.. code:: bash
   
   gp3-redcomb -i grond -o smc0032_g.fits -b g --calibdir=$CALIBDIR/2019-01-01 $RAW_FRAMES/*.fits


Astrometry (optional)
---------------------

Next we can attempt to solve the astrometry using SCAMP, which also allows us to fine-tune some parameters if desired:

.. code:: bash
   
   gp3-astrometry smc*.fits

In case of overlapping fields SCAMP will also derive a global astrometric images for all images.

This step will produce header files containing the astrometric solution for each image (smc0032_g.fits will become smc0032_g.head).

Photometry
----------

If we did astrometry using *gp3-astrometry* in the previous step it will be automatically be used by *gp3-phot*.

To obtain the result file from gp1 we can specify the output to be of type result:

.. code:: bash
   
   gp3-phot --method gp1 -o smc0032_g.result:result --band g smc0032_g.fits

In case the resulting catalog is used in further analysis steps in python scripts the HDF5 output format is recommended:

.. code:: bash
   
   gp3-phot --method gp1 -o smc0032_g.h5:hdf5 --band g smc0032_g.fits
