.. gp3 documentation master file, created by
   sphinx-quickstart on Thu Feb 12 13:16:34 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to gp3's documentation!
===============================

Contents:

.. toctree::
   :maxdepth: 1
   
   install
   cookbook
   cli_tools

