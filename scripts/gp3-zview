#!/usr/bin/env python2
import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
from matplotlib.widgets import Slider
import sys
import argparse
import logging
import Tkinter as tk
import ttk
from astropy.time import Time
import specutils
import specutils.io.read_fits as read_fits
import astropy.io.fits as fits
import specutils.wcs.specwcs as specwcs
import matplotlib.transforms as mtransforms
import numpy as np
import matplotlib.animation as animation
from astropy.modeling import models, fitting

from gp3.mos import TargetList, LineDB
from gp3.mos.cosmetic import ClipUnphysicalLines
from gp3.pipe import DAG
import gp3.spec.redshift as calc_redshift

# Info about embedding matplotlib plots into Tk
# https://pythonprogramming.net/how-to-embed-matplotlib-graph-tkinter-gui/
# http://matplotlib.org/examples/user_interfaces/embedding_in_tk.html

# Misc stuff about matplotlib
# http://matplotlib.org/users/recipes.html
    

def fit_emission_line(wavelength, flux, uncertainty):
    # find peak in the region
    idx = np.argmax(flux)
    cont = np.nanmean(flux)
    gg_init = models.Linear1D(slope=0.01, intercept=cont) + models.Gaussian1D(amplitude=(flux[idx] - cont), mean=wavelength[idx], stddev=3.0)
    fitter = fitting.SLSQPLSQFitter()
    gg_fit = fitter(gg_init, wavelength, flux, weights=1.0/uncertainty)
    print(gg_fit)
    return (gg_fit.mean_1.value, gg_fit.stddev_1.value, gg_fit.amplitude_1.value)
                            


class ZView(tk.Tk):
    
    def __init__(self, files, lines):
        tk.Tk.__init__(self)
        tk.Tk.wm_title(self, "gp3-zview")
        self.target_list = TargetList()
        for f in files:
            self.target_list.load_aao(f)
        self.lines = lines
        # TODO: give select view select window title and spectrum view spectrum view
        self.spec_view = SpectrumView(self)
        self.spec_view.tkraise()
        self.sel_view = SelectView(self)
        self.sel_view.tkraise()
        self.line_view = LineView(self)
        self.line_view.tkraise()
        #self.pack(expand=True, fill='both')

    def select_target(self, target_name):
        # find the spectrum
        tgt = self.target_list.targets[target_name]
        s = tgt.specs[filter(lambda (stype, _w): stype == 'normal', tgt.specs.keys())[0]]
        self.line_view.clear_lines()
        self.spec_view.update_spec(s)
        
    def add_line(self, *args, **kwargs):
        self.line_view.add_line(*args, **kwargs)

class SpectrumView(tk.Frame):
    
    def __init__(self, parent):
        tk.Frame.__init__(self, tk.Toplevel(parent))
        self.master = parent
        self.pack(side="top", fill="both", expand = True)
        self.fig = Figure(figsize=(10,5), dpi=100)
        self.ax = self.fig.add_subplot(111)
        #self.fig.subplots_adjust(left=0.25, bottom=0.25)
        
        self.ax.plot([1,2,3,4,5,6,7,8],[5,6,1,3,8,9,3,5])
        
        self.canvas = FigureCanvasTkAgg(self.fig, self)
        self.canvas.show()
        self.canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)
        self.canvas.mpl_connect('pick_event', self.on_pick)
        
        toolbar = NavigationToolbar2TkAgg(self.canvas, self)
        
        self.red_ax = self.fig.add_axes([0.15, 0.03, 0.65, 0.03])
        self.red_slider = Slider(self.red_ax, 'Redshift', 0, 3, valinit=0., valfmt='%f')
        self.red_slider.on_changed(self.update_redshift)
        self.anim = animation.FuncAnimation(self.fig, self.update_wavelen, interval=100)
        toolbar.update()
        self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
    
    def on_pick(self, event):
        line = event.artist
        
        # the closest 10 points of the plot to the click event
        ind = event.ind
        wave = line.get_xdata()[ind]
        flux = line.get_ydata()[ind]
        uncertainty = self.spec.uncertainty.array[ind]
        center, width, amplitude = fit_emission_line(wave, flux, uncertainty)
        self.master.add_line(center, width, amplitude)
        

    def update_spec(self, new_spec):
        dag = DAG()
        clipper = ClipUnphysicalLines(dag=dag)
        clipper.input_spec.value = new_spec
        dag.run()
        self.spec = clipper.output_spec.value
        self.spec_wavelen = self.spec.wavelength
        self.redshift = 0.0
        self.redraw()
    
    def redraw(self):
        self.ax.clear()
        beta = self.redshift + 1.0
        wavelen = self.spec_wavelen/beta
        wavelen = wavelen.value
        self.spec_plot, = self.ax.plot(wavelen, self.spec.data, picker=20)
        trans = mtransforms.blended_transform_factory(self.ax.transData, self.ax.transAxes)
        for line in self.master.lines.lines:
            #print wavelen, line['Center']
            #self.ax.fill_between(wavelen, 0, 1, where=np.abs(wavelen - line['Center']) < line['Width'], alpha=0.5, facecolor=line['Color'], transform=trans)
            self.ax.fill_between([line['Center'] - line['Width']/2.0, line['Center'] + line['Width']/2.0], 0, 1, alpha=0.5, facecolor=line['Color'], transform=trans)
        self.canvas.draw()
        
    def update_redshift(self, new_redshift):
        self.redshift = new_redshift
        #self.redraw()
    
    def update_wavelen(self, _whatever):
        if not hasattr(self, 'redshift'):
            return
        beta = self.redshift + 1.0
        wavelen = self.spec_wavelen/beta
        wavelen = wavelen.value
        self.spec_plot.set_xdata(wavelen)
        self.ax.set_xlim(np.min(wavelen), np.max(wavelen))
        self.spec_plot,

class LineView(tk.Frame):
    
    def __init__(self, parent):
        tk.Frame.__init__(self, tk.Toplevel(parent))
        self.master = parent
        self.lines = {}
        
        self.tree = ttk.Treeview(self)
        self.tree['columns'] = ('consider', 'amplitude', 'rest_id', 'red_id')
        self.tree.column('consider', width=1)
        self.tree.heading('consider', text='C')
        self.tree.column('amplitude', width=40)
        self.tree.heading('amplitude', text='Ampl')
        self.tree.column('rest_id', width=100)
        self.tree.heading('rest_id', text='RestframeID')
        self.tree.column('red_id', width=100)
        self.tree.heading('red_id', text='RedID')
        
        self.tree.bind('<Delete>', self.on_delete)
        self.tree.bind('<Double-1>', self.on_double_click)
        self.tree.pack(expand=True, fill='both', side='top')
        
        self.button = ttk.Button(self, text='Calculate', command=self.on_btn_calc)
        self.button.pack(side='bottom')
        self.pack(side="top", fill="both", expand = True)
    
    def on_delete(self, event):
        item = self.tree.selection()[0]
        del self.lines[int(item)]
        self.tree.delete(item)
    
    def on_double_click(self, event):
        item = int(self.tree.selection()[0])
        self.lines[item]['active'] = not self.lines[item]['active']
        self.update_values(item)
        
    def on_btn_calc(self):
        # active lines
        lines_obs = map(lambda a: a['center'],
                        filter(lambda a: a['active'],
                               self.lines.values()))
        lines_lab = self.master.lines.lines['Center'].data
        print('--------------------------------------')
        print('Calculating based on %d lines' % len(lines_obs))
        print(repr(lines_obs))
        print(repr(lines_lab))
        dist = calc_redshift.calc_line_redshift_dist(lines_obs, lines_lab, line_tol=0.75)
        z = calc_redshift.pick_redshift(dist)
        calc_redshift.print_redshift_dist(dist)
        print('Best redshift: %f' % z)

    def clear_lines(self):
        for line in self.lines.keys():
            self.tree.delete(line)
        self.lines = {}
        self.ctr = 0
    
    def add_line(self, center, width, amplitude):
        i = self.ctr
        self.ctr += 1
        self.lines[i] = {'center': center, 'width': width, 'amplitude': amplitude, 'active': True}
        # TODO: perform identification
        self.tree.insert('', 'end', '%d' % i, text='%f' % center)
        self.update_values(i)
    
    def update_values(self, lid):
        d = self.lines[lid]
        if d['active']:
            act = 'Y'
        else:
            act = 'N'
        rest_ids = self.master.lines.identify(center=d['center'], width=d['width'])
        if len(rest_ids) == 0:
            rest_ids = ''
        else:
            t = ''
            for line in rest_ids:
                t += line['Name']
                t += ' '
            rest_ids = t
        self.tree.item(lid, values=(act, '%d' % int(d['amplitude']), rest_ids, ''))

class SelectView(tk.Frame):
    
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        self.master = parent
        self.tree = ttk.Treeview(self)
        self.tree_scroll = ttk.Scrollbar(self)
        self.tree_scroll.configure(command=self.tree.yview)
        self.tree.configure(yscrollcommand=self.tree_scroll.set)
        self.tree.bind('<Double-1>', self.on_double_click)
        
        # load targets into tree
        for target in self.master.target_list.targets.values():
            self.tree.insert('', 'end', target.name, text=target.name)
            
        self.tree.pack(expand=True, fill='both', side='left')
        self.tree_scroll.pack(fill='both', side='right')
        self.pack(expand=True, fill='both')
    
    def on_double_click(self, event):
        item = self.tree.selection()[0]
        self.master.select_target(item)
        
    def load_file(self, fname):
        self.tree.insert('', 'end', fname, text=fname)
        hdu = fits.open(fname)
        time = Time(hdu[0].header['UTMJD'], format='mjd')
        specs = read_fits.read_fits_spectrum1d(fname)
        specs_noskysub = read_fits.read_fits_spectrum1d(fname, hdu='RWSS')
        # only keep program objects (no sky fiber or guide fiber)
        specs_obj = filter(lambda a: a.meta['TYPE'] == 'P', specs)
        specs2_obj = filter(lambda a: a.meta['TYPE'] == 'P', specs_noskysub)
        # now add all spectra to the database
        for spec,spec2 in zip(specs_obj, specs2_obj):
            spec.meta['TIME'] = time
            spec2.meta['TIME'] = time
            # TODO: guess arm
            self.master.target_list.add_spec(spec.meta['NAME'], spec, 'normal', fname)
            self.master.target_list.add_spec(spec.meta['NAME'], spec2, 'RWSS', fname)
            self.tree.insert(fname, 'end', spec.meta['NAME'], text=spec.meta['NAME'])



def main():
    logging.basicConfig(level=logging.DEBUG)
    
    ldb = LineDB()
    ldb.load_lines('stuff/balmer_lines.txt', group='Balmer', bands=True)
    ldb.load_lines('stuff/sdss_em_lines.txt', group='SDSS', bands=False, width=10)
    
    app = ZView(files=sys.argv[1:], lines=ldb)
    app.mainloop()

if __name__ == '__main__':
    main()



# Local Variables:
# mode: python
# End:
