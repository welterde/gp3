#!/usr/bin/env python2
import argparse
import logging
import os
import fnmatch




INTERESTING_FILES = ['.fit', '.fits', '.fit.gz', '.fits.gz']

def main():
    logging.basicConfig(level=logging.DEBUG, format='[%(asctime)-15s %(levelname)-5s] %(message)s')
    log = logging.getLogger('main')
    parser = argparse.ArgumentParser(description='Preprocess raw CCD frames')
    #parser.add_argument('-i', '--instrument', default='gowi', choices=['gowi'])
    #parser.add_argument('--overwrite', default=False, action='store_const', const=True)
    parser.add_argument('--newdir', default='/data/raw/incoming/new',
                        help='Source dir containing images to be processed')
    parser.add_argument('--procdir', default='/data/raw/incoming/preproc',
                        help='Destination dir to place preprocessed files')
    parser.add_argument('--origdir', default='/data/raw/incoming/orig',
                        help='Destination dir to move original files to')
    args = parser.parse_args()
    
    mfiles = []
    # collect all interesting files..
    # TODO: warn about all those not matched
    for dirpath, dirs, files in os.walk(args.newdir):
        for ext in INTERESTING_FILES:
            mfiles.extend(map(lambda f: os.path.join(dirpath, f), fnmatch.filter(files, '*' + ext)))
    
    # TODO: process files
    
    # move original files to new location
    for root, dirs, files in os.walk(args.newdir, topdown=False):
        mf = []
        for ext in INTERESTING_FILES:
            mfiles.extend(fnmatch.filter(files, '*' + ext))
        orig_path = os.path.join(args.origdir, os.relpath(root, args.newdir))
        if not os.path.isdir(orig_path):
            os.makedirs(orig_path)
        for f in mf:
            src=os.path.join(root, f)
            dst=os.path.join(orig_path, f)
            log.info("Moving %s to %s" % (src,dst))
            os.move(src, dst)
    
    # delete all empty dirs
    for root, dirs, files in os.walk(args.newdir, topdown=False):
        if len(dirs) == 0 and len(files) == 0:
            os.rmdir(root)

if __name__ == '__main__':
    main()


# Local Variables:
# mode: python
# End:
