#!/usr/bin/env python2
import argparse
import logging
import sys
import os
import jinja2
import flask
from flask import request
import numpy as np
import itertools
from math import pi

import matplotlib
matplotlib.use('Agg')

import specutils
import specutils.io.read_fits as read_fits
import specutils.wcs.specwcs as specwcs
from bokeh.plotting import figure
from bokeh.resources import CDN
from bokeh.embed import file_html, components
from bokeh.models import BoxAnnotation, Range1d, LinearAxis, FixedTicker, FuncTickFormatter, TickFormatter
from bokeh.resources import INLINE
from astropy.table import Table, Column
import astropy.convolution as conv
import astropy.units as u
from astropy.time import Time
from scipy import interpolate, signal
from astropy.nddata.nduncertainty import StdDevUncertainty
import wtforms
import gp3.util.yamldb as yamldb
from gp3.plot.bokeh_plot import FixedTickFormatter
from astropy.coordinates import EarthLocation
from gp3.mosview.velocity import VelocityView
import astropy.io.fits as fits

import gp3.mosview.redshift as redtools
from gp3.spec.linefit import fit_line
from gp3.mos.cosmetic import clip_lines
import functools 

env = jinja2.Environment(loader=jinja2.PackageLoader('gp3.mosview', 'templates'))

app = flask.Flask('gp3.mosview')

targets = {}
# group -> table of lines
lines = {}
line_ctr = [0]
# ID -> line
all_lines = {}

# TODO: make this configurable
site = EarthLocation.of_site('siding spring observatory')

log = logging.getLogger('main')

spec_templates = redtools.load_templates("stuff/sdss_spectral_templates")

class TagTextInput(wtforms.widgets.TextInput):
    def __call__(self, field, **kwargs):
        kwargs['data-role'] = u'tagsinput'
        return super(TagTextInput, self).__call__(field, **kwargs)

class TagListField(wtforms.Field):
    widget = TagTextInput()

    def _value(self):
        if self.data:
            return u', '.join(self.data)
        else:
            return u''

    def process_formdata(self, valuelist):
        #print repr(valuelist)
        if valuelist:
            self.data = [x.strip() for x in valuelist[0].split(',')]
        else:
            self.data = []
        #print repr(self.data)

class TargetForm(wtforms.Form):
    redshift = wtforms.FloatField('redshift', [wtforms.validators.NumberRange(min=0, max=10)], default=0.0)
    tags = TagListField('tags')
    velocity_line = wtforms.SelectField('velocity_line', coerce=int)

class Database(object):
    log = logging.getLogger('db')
    def set_dbdir(self, db_dir):
        self.db = yamldb.YAMLDatabase(db_dir)
    
    def load_target(self, name):
        if name not in self.db:
            return
        log.info('loading %s from database' % name)
        dat = self.db[name]
        if 'tags' in dat:
            log.debug('Adding %d tags' % len(dat['tags']))
            x = set(targets[name]['tags'])
            x.update(dat['tags'])
            targets[name]['tags'] = list(x)
    
    def update_target(self, name):
        log.info('writing %s to database' % name)
        dat = {}
        dat['tags'] = targets[name]['tags']
        self.db[name] = dat

db = Database()

@app.route('/')
def index_view():
    return flask.render_template('overview.html', targets=targets)

js_resources = INLINE.render_js()
css_resources = INLINE.render_css()

def plot_spec(spec, target, show_stddev=False, show_cont=False, noskysub=None):
    fig = figure(title='Spectrum', responsive=True, plot_width=1200, plot_height=400)
    wavelen = spec.wavelength
    fig.line(wavelen.value, spec.data)
    fig.set(y_range=Range1d(-200, float(np.nanmax(spec.data))))
    if noskysub is not None:
        fig.line(noskysub.wavelength.value, noskysub.data, color='green')
    # mark lines
    ticks = []
    labels = {}
    for groupname, group in lines.items():
        grouplines = group['lines']
        for line in grouplines:
            start = line['Start']*u.Angstrom
            stop = line['Stop']*u.Angstrom
            if 'redshift' in target and group['sky']:
                z = target['redshift']
                start = start/(z+1.0)
                stop = stop/(z+1.0)
            if stop > np.nanmin(wavelen) and start < np.nanmax(wavelen):
                box = BoxAnnotation(left=start.value, right=stop.value,
                                    fill_alpha=0.2, fill_color=group['color'])
                fig.add_layout(box)
                #print repr(line)
                if 'Comment' in line.colnames:
                    x = int(((start+stop)/2.0).value)
                    ticks.append(x)
                    labels[x] = line['Comment'].strip()
    #print ticks, labels
    ticker = FixedTicker(ticks=ticks)
    formatter = FixedTickFormatter(labels=labels)
    fig.add_layout(LinearAxis(ticker=ticker,
                              formatter=formatter,
                              major_label_orientation=3.0*pi/8.0),
                   "above")
    #fig.xaxis.ticker = ticker
    #fig.xaxis.formatter = formatter
                
    
    if show_stddev:
        valid = ~np.isnan(spec.data) & ~np.isnan(spec.uncertainty.array)
        wavelen = np.hstack((wavelen[0], wavelen[valid], wavelen[-1]))
        stddev = np.hstack((0, spec.uncertainty.array[valid], 0))
        fig.patches([wavelen], [stddev], alpha=0.2, fill_color='grey')
        fig.patches([wavelen], [stddev*(-1.0)], alpha=0.2, fill_color='grey')
    
    if show_cont:
        fig.line(show_cont.wavelength.value, show_cont.data*(-1.0), color='red')

    return components(fig)


def z_match(target):
    em_lines = []
    for name in filter(lambda a: a.startswith('lines:'), target.keys()):
        em_lines.extend(map(lambda (a,b): (a.value, b.value),
                            target[name]))
    #print len(em_lines)
    #print repr(em_lines)
    z_lines = filter(lambda (a,b): b['zmatch'], lines.iteritems())
    line_dbs = [y['lines'] for x,y in z_lines]
    
    upd = {}
    for z in np.concatenate((np.arange(0, 1, 0.001), np.arange(1,4,0.01))):
        if z not in upd:
            upd[z] = []
        eml = map(lambda (a,b): (a/(z + 1.0), b), em_lines)
        for center, width in eml:
            for db in line_dbs:
                for line in db:
                    if not 'Center' in line:
                        line_center = (line['Start']+line['Stop'])/2
                    else:
                        line_center = line['Center']
                    #print line
                    #if center >= (line['Start']-8) and center <= (line['Stop']+8):
                    #if np.abs(np.mean([line['Start'], line['Stop']]) - center) <= np.max([width, line['Stop'] - line['Start']]):
                    if np.abs(line_center - center) <= 3.0:
                        upd[z].append({'obs:center': center,
                                           'obs:width': width,
                                           'entry': line})
    return upd

def z_corr(spec):
    out = {}
    for cls in [2,3,4]:
        for fname, template in spec_templates[cls].iteritems():
            corr = redtools.correlate2(spec, template)
            out[(cls, fname)] = corr
    return out

def plot_zcorr(tgt):
    zcorr = tgt['zcorr']
    fig = figure(title='Cross-Correlation', responsive=True, plot_width=400, plot_height=400)
    for (name, (corr,z)), color in itertools.izip(zcorr.iteritems(), ['red', 'blue', 'green', 'cyan', 'black']):
        fig.line(z, corr, color=color, legend='%d %s' % name)
    return components(fig)

def smooth_spec(spec, fwhm=3.0):
    stddev = fwhm/2.0/np.sqrt(2*np.log(2))
    # also interpolates invalid values (NaN) using the kernel
    g = conv.Gaussian1DKernel(stddev=stddev)
    new_data = conv.convolve(spec.data, g)
    # FIXME: also copy mask?
    return specutils.Spectrum1D(flux=new_data, wcs=spec.wcs, meta=spec.meta, uncertainty=spec.uncertainty)

def bin_spec(spec, bin_size=2):
    if len(spec.data) % bin_size != 0:
        assert('Bin size %d does not cleanly divide spectra length %d' % (bin_size, len(spec.data)))
    out = np.zeros(len(spec.data) / bin_size)
    out_uncertain = np.zeros(len(spec.data) / bin_size)
    for i in range(len(spec.data) / bin_size):
        out[i] = np.nanmean(spec.data[i*bin_size:(i+1)*bin_size])
        out_uncertain[i] = np.nanmax(spec.uncertainty.array[i*bin_size:(i+1)*bin_size])
    wcs = spec.wcs.copy()
    wcs.c1.value = wcs.c1.value * float(bin_size)
    return specutils.Spectrum1D(flux=out, wcs=wcs, meta=spec.meta, uncertainty=StdDevUncertainty(out_uncertain))
                            
def redshift_spec(spec, redshift=1.0):
    wcs = specwcs.CompositeWCS()
    wcs.unit = spec.wcs.unit
    wcs.add_WCS(spec.wcs)
    wcs.add_WCS(specwcs.DopplerShift.from_redshift(redshift).inverse)
    #print spec.unit, spec.wcs.unit, wcs.unit
    return specutils.Spectrum1D(flux=spec.data, wcs=wcs, uncertainty=spec.uncertainty, meta=spec.meta)

def extract_continuum(spec, use_grid=False, kernel_size=101):
    #valid = ~np.isnan(spec.data) & ~np.isnan(spec.uncertainty.array)
    #i1 = interpolate.UnivariateSpline(spec.wavelength[valid], spec.data[valid], s=100)
    #i1 = np.interp(spec.wavelength, xp=spec.wavelength[valid], fp=spec.data[valid])
    g = conv.Gaussian1DKernel(stddev=150)
    continuum = conv.convolve(spec.data, g)
    #continuum = interpolate.UnivariateSpline(spec.wavelength, i2, s=100)
    if use_grid:
        continuum = np.interp(use_grid, xp=spec.wavelength, fp=continuum)
    #    new_data = continuum(use_grid)
    #else:
    #    new_data = continuum(spec.wavelength)
    return specutils.Spectrum1D(flux=continuum, wcs=spec.wcs, meta=spec.meta)

def max_filter(data, win_len=3):
    out = np.zeros(data.shape)
    for i in range(win_len, len(data) - win_len):
        out[i] = np.nanmax(data[(i-win_len):(i+win_len)])
    return out

def extract_lines(wavelength, data, uncertainty, em=True, sigma=2.0):
    if em:
        consider = data > sigma*uncertainty
    else:
        consider = data < -sigma*uncertainty
    idx = np.arange(len(data))
    groups = [list(v) for k,v in itertools.ifilter(lambda (a,b): a, itertools.groupby(idx, lambda i: consider[i]))]
    # reject everything that's not at least two pixels above the threshold
    groups_real = filter(lambda a: len(a) > 1, groups)
    def dostuff(group):
        #print consider[group]
        #center = np.mean(wavelength[group])
        if em:
            center = wavelength[group][np.argmax(data[group])]
        else:
            center = wavelength[group][np.argmin(data[group])]
        size = np.max(wavelength[group]) - np.min(wavelength[group])
        #print '\tCenter=%f \pm %f' % (center, size)
        return (center, size)
    foo = map(dostuff, groups_real)
    print 'Found Lines:', repr(foo)
    return foo

@app.route('/target/<target_name>', methods=['GET', 'POST'])
def target_view(target_name):
    target = targets[target_name]
    if 'redshift' not in target:
        target['redshift'] = 0.0
    form = TargetForm(request.form, tags=target['tags'], redshift=target['redshift'])
    form.tags.default = target['tags']
    form.velocity_line.choices = map(lambda (i,l): (i, '%s[%.2f]' % (l['Comment'], l['Center'])), all_lines.iteritems())
    form.velocity_line.default = min(all_lines.keys())
    form.redshift.default = target['redshift']
    #print 'Tags:', target['tags']
    if request.method == 'POST' and form.validate():
        target['redshift'] = form.redshift.data
        target['tags'] = form.tags.data
        print 'New Tags: ', target['tags']
        db.update_target(target_name)
    
    # if we haven't measured all the lines yet do it now
    if ('linesfit:%f' % target['redshift']) not in target:
        # line -> (results)
        line_m = {}
        for line_group in lines.keys():
            if lines[line_group]['sky']:
                continue
            for line in lines[line_group]['lines']:
                line_center = line['Center']
                # now find the correct spectrum...
                for fname, spec in target['specs'].iteritems():
                    if fname.endswith('RWSS') or fname.startswith('spliced'):
                        continue
                    if line_center > np.max(spec.wavelength.value) or line_center < np.min(spec.wavelength.value):
                        continue
                    line_m[line_center] = fit_line(spec, line_center) + (line['Comment'],)
        target['linesfit:%f' % target['redshift']] = line_m

    plots={}
    all_specs = []
    for fname, spec in target['specs'].iteritems():
        if fname.endswith('RWSS') or fname.startswith('spliced'):
            continue
        print repr(spec.wavelength)
        if np.nanmin(spec.wavelength) < 4000*u.Angstrom:
            arm = 'blue'
        else:
            arm = 'red'
        spec2 = target['specs']['%s:RWSS' % fname]
        if 'redshift' in target:
            log.debug('Redshifting spectrum to %f' % float(target['redshift']))
            new_spec = redshift_spec(spec, redshift=float(target['redshift']))
            new_spec2 = redshift_spec(spec2, redshift=float(target['redshift']))
        else:
            new_spec = spec
            new_spec2 = spec2
        plots[arm] = plot_spec(new_spec, target, noskysub=new_spec2)
        # TODO: make smoothing kernel configurable via web page
        smoothed = smooth_spec(spec)
        #smoothed = bin_spec(spec, bin_size=4)
        if 'redshift' in target:
            smoothed = redshift_spec(smoothed, redshift=float(target['redshift']))
        plots['%s:smooth' % arm] = plot_spec(smoothed, target=target)
        continuum = extract_continuum(smoothed)
        #continuum = extract_continuum(smoothed, kernel_size=403)
        plots['%s:continuum' % arm] = plot_spec(continuum, target=target)
        sub_flux = smoothed.data - continuum.data
        #sub_uncertain = max_filter(spec.uncertainty.array)
        sub_uncertain = max_filter(smoothed.uncertainty.array)
        subtracted = specutils.Spectrum1D(flux=sub_flux, wcs=smoothed.wcs, meta=new_spec.meta, uncertainty=StdDevUncertainty(sub_uncertain))
        all_specs.append(subtracted)
        plots['%s:sub' % arm] = plot_spec(subtracted, show_stddev=True, show_cont=continuum,target=target)
        
        # extract lines
        if 'lines:%s' % arm not in target:
            #c = extract_continuum(spec, kernel_size=301)
            #sub_flux2 = spec.data - c.data
            #sub_uncertain2 = max_filter(spec.uncertainty.array)
            #subtracted2 = specutils.Spectrum1D(flux=sub_flux2, wcs=spec.wcs, meta=spec.meta, uncertainty=StdDevUncertainty(sub_uncertain2))
            target['lines:%s' % arm] = extract_lines(subtracted.wavelength, subtracted.data,
                                                     subtracted.uncertainty.array,
                                                     em=True, sigma=2.0)
        
        # generate velocity profile if possible
        if form.velocity_line.data is None:
            form.velocity_line.data = form.velocity_line.default
        selected_line = all_lines[form.velocity_line.data]
        selected_wave = selected_line['Center']*u.Angstrom
        if np.nanmin(new_spec.wavelength) <= selected_wave and np.nanmax(new_spec.wavelength) >= selected_wave:
            velocity = VelocityView(new_spec, line_wavelength=selected_line['Center']*u.Angstrom, site=site, time=spec.meta['TIME'])
            if selected_line['Center'] in target['linesfit:%f' % target['redshift']]:
                velocity.fit_obj = target['linesfit:%f' % target['redshift']][selected_line['Center']][5]
            plots['velocity'] = velocity.plot_bokeh()
        
        # if 'zmatch' not in target:
        #     target['zmatch'] = {}
        # if 'zmatch:%s' % arm not in target:
        #     #smoothed = bin_spec(spec, bin_size=2)
        #     continuum = extract_continuum(spec)
        #     sub_flux = spec.data - continuum.data
        #     sub_uncertain = max_filter(spec.uncertainty.array)
        #     subtracted = specutils.Spectrum1D(flux=sub_flux, wcs=spec.wcs, meta=spec.meta, uncertainty=StdDevUncertainty(sub_uncertain))
        #     z_match(subtracted, upd=target['zmatch'])
        #     target['zmatch:%s' % arm] = True
    # any arm should be fine
    
    if 'zmatch' not in target:
        target['zmatch'] = z_match(target)
    if 'zcorr' not in target:
        spliced = target['specs']['spliced']
        continuum = extract_continuum(spliced)
        sub_flux = spliced.data - continuum.data
        #sub_uncertain = max_filter(spec.uncertainty.array)
        sub_uncertain = max_filter(smoothed.uncertainty.array)
        subtracted = specutils.Spectrum1D(flux=sub_flux, wcs=spliced.wcs, meta=spliced.meta, uncertainty=StdDevUncertainty(sub_uncertain))
        target['zcorr'] = z_corr(subtracted)
    plots['zcorr'] = plot_zcorr(target)
    spec_meta = target['specs'].items()[0][1].meta
    return flask.render_template('target.html', target=target, name=target_name, plots=plots, meta=spec_meta, js=js_resources, css=css_resources, zmatch=target['zmatch'], form=form, linedb=lines, line_measurements=target['linesfit:%f' % target['redshift']])

def add_target(name):
    if name in targets:
        return
    targets[name] = {'specs': {}, 'tags': []}
    # load stuff from database
    if db is not None:
        db.load_target(name)

def add_spec(target, spectrum, src_file, extra={}):
    add_target(target)
    targets[target]['specs'][src_file] = spectrum
    
def load_file(name, fspec):
    parts = fspec.split(",")
    ftype = parts[0]
    parts.pop(0)
    
    if ftype == 'aaospecs':
        log.info('Loading AAOmega spectra from %s' % name)
        hdu = fits.open(name)
        time = Time(hdu[0].header['UTMJD'], format='mjd')
        specs = read_fits.read_fits_spectrum1d(name)
        specs_noskysub = read_fits.read_fits_spectrum1d(name, hdu='RWSS')
        
        # exclude all unphysical lines from further analysis
        specs = map(functools.partial(clip_lines, sigma=5.0), specs)
        #specs_noskysub = map(functools.partial(clip_lines, sigma=7.0), specs_noskysub)
        
        # only keep program objects (no sky fiber or guide fiber)
        specs_obj = filter(lambda a: a.meta['TYPE'] == 'P', specs)
        specs2_obj = filter(lambda a: a.meta['TYPE'] == 'P', specs_noskysub)
        # now add all spectra to the database
        for spec,spec2 in zip(specs_obj, specs2_obj):
            spec.meta['TIME'] = time
            spec2.meta['TIME'] = time
            if 'spliced' in ftype:
                add_spec(spec.meta['NAME'], spec, 'spliced')
                add_spec(spec.meta['NAME'], spec2, '%s:RWSS' % 'spliced')
            else:
                add_spec(spec.meta['NAME'], spec, name)
                add_spec(spec.meta['NAME'], spec2, '%s:RWSS' % name)
    elif ftype.startswith('lines'):
        parts = ftype.split(',')
        # remove first element (which is lines)
        parts.pop(0)
        def parse(a):
            if '=' in a:
                return a.split('=')
            else:
                return a, True
        cfg = dict(map(lambda a: tuple(parse(a)), parts))
        
        group = cfg['group']
        # TODO: use dict.update({..}) instead here
        if 'format' not in cfg:
            cfg['format'] = 'ascii.commented_header'
        if 'color' not in cfg:
            cfg['color'] = 'red'
        log.info('Loading %s-lines from %s' % (group, name))
        if 'bands' in cfg:
            l = Table.read(name, format=cfg['format'])
            l.add_column(Column(data=(l['Start']+l['Stop'])/2.0, name='Center'))
        else:
            l = Table.read(name, format=cfg['format'])
            l.add_column(Column(data=l['Center'] - 2.0, name='Start'))
            l.add_column(Column(data=l['Center'] + 2.0, name='Stop'))
        l.add_column(Column(data=np.zeros(len(l)), name='LineID'))
        for i in range(len(l)):
            l['LineID'][i] = line_ctr[0]
            line_ctr[0] += 1
            if 'sky' not in cfg and 'nolabel' not in cfg:
                all_lines[int(l['LineID'][i])] = l[i]
        if 'nolabel' in cfg and 'Comment' in l.colnames:
            l.remove_column('Comment')
        lines[group] = {
            'lines': l,
            'color': cfg['color'],
            'zmatch': 'zmatch' in cfg,
            'sky': 'sky' in cfg
        }
    elif ftype.startswith('db'):
        log.info('Using %s as database directory' % name)
        db.set_dbdir(name)
    

def main():
    logging.basicConfig(level=logging.DEBUG)

    sys.argv.pop(0)
    if len(sys.argv) % 2 != 0:
        print 'Usage: gp3-mosview <type> <file> [<type> <file> [...]]'
        sys.exit(1)
    for ftype, fname in zip(sys.argv[::2], sys.argv[1::2]):
        load_file(fname, ftype=ftype)
    while len(sys.argv) > 0:
        sys.argv.pop()
    
    #app.debug = True
    app.run(host='0.0.0.0')

if __name__ == '__main__':
    main()

# Local Variables:
# mode: python
# End:
