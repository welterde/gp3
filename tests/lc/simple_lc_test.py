from gp3.lc.simple import SimpleLC
import numpy as np





def gen_testcase(num_refobj=100, num_epochs=10, fraction_exclude=0.01):
    refobj_coords_ra = np.random.uniform(10, 10.1, size=num_refobj)
    refobj_coords_dec = np.random.uniform(10, 10.1, size=num_refobj)
    
    refobj_base_flux = np.random.exponential(5000, size=num_refobj)
    
    cur_mjd = 58563
    for epoch_idx in range(num_epochs):
        cur_coords_ra = refobj_coords_ra + np.random.normal(0, 0.0001, size=num_refobj)
        cur_coords_dec = refobj_coords_dec + np.random.normal(0, 0.0001, size=num_refobj)
        
        cur_background = np.random.uniform(100, 1000)
        
        cur_obj_flux = np.random.poisson(refobj_base_flux)
        cur_bkg_flux = np.random.poisson(cur_background, size=num_refobj)
        
        cur_mjd += 2.0*np.random.random()
        
