import pytest

import numpy as np
from gp3.mos.line_db import LineDB

from gp3.spec.redshift import measure_line_redshift

@pytest.fixture(scope="session")
def emission_line_db():
    ldb = LineDB()
    ldb.load_lines('stuff/balmer_lines.txt', group='Balmer', bands=True)
    ldb.load_lines('stuff/sdss_em_lines.txt', group='SDSS', bands=False, width=10)
    return ldb

class TestCalcRedshift:
    @pytest.mark.randomize(redshift=float, min_num=0.0, max_num=6.0, ncalls=25)
    def test_basic(self, emission_line_db, redshift):
        obs = np.array([6562.793, 4932.603, 5008.24])*(1+redshift)
        ret = measure_line_redshift(obs, emission_line_db.lines['Center'].data)
        assert round(ret, 3) == round(redshift, 3)
    
    @pytest.mark.randomize(redshift=float, min_num=0.0, max_num=6.0, ncalls=25)
    def test_contamination(self, emission_line_db, redshift):
        obs = np.array([6562.793, 4932.603, 5008.24, 4932.603/(1.0+redshift)])*(1+redshift)
        ret = measure_line_redshift(obs, emission_line_db.lines['Center'].data)
        assert round(ret, 3) == round(redshift, 3)
    
    @pytest.mark.randomize(redshift=float, min_num=0.0, max_num=6.0, ncalls=25)
    def test_contamination_meas(self, emission_line_db, redshift):
        obs = np.array([6562.793, 4932.603, 5008.24, 4932.603/(1.0+redshift)])*(1+redshift)
        offsets = np.random.ranf(len(obs))/8 - 0.0625
        obs += offsets
        ret = measure_line_redshift(obs, emission_line_db.lines['Center'].data)
        assert round(ret, 1) == round(redshift, 1)
