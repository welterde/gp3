import tempfile
#import benchmark
import gp3.util.coorddb as cdb
import numpy as np
import leveldb
import struct
import pytest
import astropy.units as u
import astropy.coordinates as coordinates



def insert_stuff(db, ras, decs):
    idx = np.random.randint(5000)
    #db.find((ras[idx], decs[idx]))
    data = db.get((ras[idx], decs[idx]))
    data[(np.random.random(), 'r')] = np.random.random()
    db.update((ras[idx], decs[idx]), data)

def test_speed(benchmark):
    d = tempfile.mkdtemp()
    
    db = cdb.CoordDB(d)
    ras = np.random.standard_normal(5000)*0.08
    decs= np.random.standard_normal(5000)*0.08
    lens= np.random.randint(15, size=5000)
    for i in range(5000):
        data = dict([((np.random.random(), 'g'), np.random.random()) for _j in range(lens[i])])
        #if db.find((ras[i], decs[i]))[0]:
        #    ras[i] += 2.0/3600
        db.put((ras[i], decs[i]), data)
    benchmark(insert_stuff, db, ras, decs)


def query_stuff_lvl(db, prefix):
    return list(db.RangeIter(key_from=struct.pack('>l', prefix), key_to=struct.pack('>l', prefix+1), include_value=False))
    
def test_leveldb_query(benchmark):
    dbdir = tempfile.mkdtemp()
    db = leveldb.LevelDB(dbdir)
    rand = open('/dev/urandom', 'rb')
    
    prefix = 58540067
    for i in range(5000):
        key = struct.pack('>ldd', prefix + np.random.randint(5), np.random.random(), np.random.random())
        db.Put(key, rand.read(1024))
    
    benchmark(query_stuff_lvl, db, prefix)

def calc_sepa(ras, decs, num, center_ra, center_dec):
    centerc = coordinates.SkyCoord(ra=center_ra, dec=center_dec, unit=(u.deg, u.deg))
    coords = coordinates.SkyCoord(ras[0:num], decs[0:num], unit=(u.deg, u.deg))
    #return coords.separation(centerc).deg
    return centerc.separation(coords).deg

@pytest.mark.parametrize("count", [1,2,4,8,16,32,64])
def test_separation_full(benchmark, count):
    ras = np.random.standard_normal(128)*0.08
    decs= np.random.standard_normal(128)*0.08
    benchmark(calc_sepa, ras, decs, count, 0.0, 0.0)

def calc_sepa_create(ras, decs, num, center_ra, center_dec):
    centerc = coordinates.SkyCoord(ra=center_ra, dec=center_dec, unit=(u.deg, u.deg))
    coords = coordinates.SkyCoord(ras[0:num], decs[0:num], unit=(u.deg, u.deg))
    #return coords.separation(centerc).deg
    #return centerc.separation(coords).deg

@pytest.mark.parametrize("count", [1,2,4,8,16,32,64])
def test_separation_create(benchmark, count):
    ras = np.random.standard_normal(128)*0.08
    decs= np.random.standard_normal(128)*0.08
    benchmark(calc_sepa_create, ras, decs, count, 0.0, 0.0)
