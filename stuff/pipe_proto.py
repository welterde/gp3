

class QueryESOArchive(Recipe):
    
    instrument = Property(string)
    target = Property(string)
    
    frames = Output(DataSet(ImageID))

class FetchImages(Recipe):
    
    image_ids = Input(DataSet(ImageID))
    images = Output(DataSet(Image))
    
    def __call__(self):
        for iid in self.image_ids:
            img = fetch_image(iid)
            self.images.add(img)
        self.images.finish()

class ProcImg(Recipe):
    
    in_img = Input(Image)
    out_img = Output(Image)
    
    

def build_pipeline():
    p = Pipeline()
    a = QueryESOArchive(dag=p)
    a.instrument.value = 'GROND'
    a.target.value = 'OJ287'
    
    b = FetchImages(image_ids=a.frames, dag=p)
    
    c = Map(dag=p, input=b.images)
    c1 = ProcImg(dag=c, input=c.inner)
    c.collect(c1.output)
    
    p.run()
    
